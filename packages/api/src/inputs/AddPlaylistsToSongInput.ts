import { InputType, Field, ID } from 'type-graphql';
import { Input } from '../interfaces';

@InputType()
export class AddPlaylistsToSongInput implements Input {

  @Field({ nullable: true })
  clientMutationId!: string;

  @Field(type => ID, {
    description: 'The song',
  })
  song!: string;

  @Field(type => [ID], {
    description: 'The song playlists',
  })
  songPlaylists!: string[];

}