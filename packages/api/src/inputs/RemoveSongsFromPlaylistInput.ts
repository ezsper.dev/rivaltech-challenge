import { InputType, Field, ID } from 'type-graphql';
import { Input } from '../interfaces';

@InputType()
export class RemoveSongsFromPlaylistInput implements Input {

  @Field(type => String, { nullable: true })
  clientMutationId?: string;

  @Field(type => ID, {
    description: 'The song playlist id',
  })
  songPlaylist!: string;

  @Field(type => [ID], {
    description: 'The items to compose playlist',
  })
  songs!: string[];

}