import { InputType, Field, ID, Int } from 'type-graphql';
import { Input } from '../interfaces';

@InputType()
export class ReorderSongsOfPlaylistInput implements Input {

  @Field({ nullable: true })
  clientMutationId!: string;

  @Field(type => ID, {
    description: 'The song playlist id',
  })
  songPlaylist!: string;

  @Field(type => [ID], {
    description: 'The song items to remove from playlist',
  })
  songItem!: string;

  @Field(type => Int, {
    description: 'The new sort order',
  })
  sortOrder!: number;
}