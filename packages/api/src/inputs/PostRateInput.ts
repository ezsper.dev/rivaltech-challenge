import { InputType, Field, ID, Int } from 'type-graphql';
import { Input } from '../interfaces';
import { Min, Max } from 'class-validator';

@InputType()
export class PostRateInput implements Input {

  @Field({ nullable: true })
  clientMutationId!: string;

  @Field(type => ID)
  postId!: string;

  @Field(type => Int, { nullable: true })
  @Min(1)
  @Max(10)
  ratingScore!: number;

}