import { InputType, Field } from 'type-graphql';
import { Input } from '../interfaces';

@InputType()
export class PostCategoryInput implements Input {

  @Field({ nullable: true })
  clientMutationId!: string;

  @Field({
    description: 'The category title',
  })
  title!: string;

  @Field({
    description: '',
    nullable: true,
  })
  description!: string;

}