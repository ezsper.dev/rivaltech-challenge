import { InputType, Field, ID } from 'type-graphql';
import { Input } from '../interfaces';
import { SongPlaylistPrivacyType } from '../enums';

@InputType()
export class SongPlaylistInput implements Input {

  @Field({ nullable: true })
  clientMutationId!: string;

  @Field({
    description: 'The song playlist title',
  })
  title!: string;

  @Field(type => [ID], {
    description: 'The items to compose playlist',
  })
  songs!: string[];


  @Field({
    description: 'If the playlist is public',
  })
  privacy!: SongPlaylistPrivacyType;

}