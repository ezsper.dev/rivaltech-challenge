import { InputType, Field, ID } from 'type-graphql';
import { Input } from '../interfaces';

@InputType()
export class PostInput implements Input {

  @Field({ nullable: true })
  clientMutationId!: string;

  @Field({
    description: 'The post title',
  })
  title!: string;

  @Field({ nullable: true })
  subheading!: string;

  @Field({
    description: '',
  })
  content!: string;

  @Field({ nullable: true })
  excerpt!: string;

  @Field(type => ID)
  categoryId!: string;

}