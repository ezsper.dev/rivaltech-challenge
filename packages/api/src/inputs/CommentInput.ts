import { InputType, Field } from 'type-graphql';
import { Input } from '../interfaces';

@InputType()
export class CommentInput implements Input {

  @Field({ nullable: true })
  clientMutationId!: string;

  @Field({
    description: '',
  })
  content!: string;

}