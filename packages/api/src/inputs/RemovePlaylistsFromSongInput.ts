import { InputType, Field, ID } from 'type-graphql';
import { Input } from '../interfaces';

@InputType()
export class RemovePlaylistsFromSongInput implements Input {

  @Field({ nullable: true })
  clientMutationId!: string;

  @Field(type => ID, {
    description: 'The song',
  })
  song!: string;

  @Field(type => [ID], {
    description: 'The song playlists',
  })
  songPlaylists!: string[];

}