const frontUri = 'http://localhost:3000';

export const config = {
  permalinks: {

    auth: {
      failureRedirectUri: `${frontUri}/login/?failure`,
      tokenRedirectUri: `${frontUri}/auth/?token=%token%`,
    },
    post: {
      link: `${frontUri}/post/%id%/%name%/`,
    },
  },
};