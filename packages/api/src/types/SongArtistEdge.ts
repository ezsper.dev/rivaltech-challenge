import { ID, ObjectType, Field } from 'type-graphql';
import { SongArtist } from './SongArtist';

@ObjectType()
export class SongArtistEdge {

  @Field(type => ID)
  cursor!: string;

  @Field()
  node!: SongArtist;

}