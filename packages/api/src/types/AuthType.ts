import { registerEnumType } from 'type-graphql';

export enum AuthType {
  ANONYMOUS,
  USER,
}

registerEnumType(AuthType, {
  name: 'AuthType',
});