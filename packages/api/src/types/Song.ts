import { ObjectType, ID, Field, Int } from 'type-graphql';
import { Entity, PrimaryColumn, Column, ManyToOne, JoinColumn, OneToMany, Index } from 'typeorm';
import { Factory, Properties, Property } from '@type-properties/core';
import { Node, NodeIdentifierProps } from '../interfaces';
import { SongArtistAlbum } from './SongArtistAlbum';
import { SongArtist } from './SongArtist';
import { SongPlaylistItem } from './SongPlaylistItem';

@Properties()
export class SongIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    init => 'Song',
  );

  _id = Property[1](
    type => String,
  );

}

export class SongIdentifier extends Factory(SongIdentifierProps) {}

@ObjectType({
  implements: [Node],
})
@Entity()
export class Song {

  static Id = SongIdentifier;

  __typename = 'Song';

  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return Song.Id.stringify(this);
  }

  @Field()
  @Column()
  title!: string;

  @Field(type => ID)
  @Index()
  @Column()
  _artistId!: string;

  @Field(type => ID)
  get artistId() {
    return SongArtist.Id.stringify({ _id: this._artistId });
  }

  @Field(type => SongArtist)
  @ManyToOne(type => SongArtist, artist => artist.songs)
  @JoinColumn({ name: '_artistId' })
  artist?: SongArtist;

  @Field(type => ID, { nullable: true })
  @Index()
  @Column('string', { nullable: true })
  _albumId?: string | null;

  @Field(type => ID)
  get albumId() {
    if (!this._albumId) {
      return null;
    }

    return SongArtistAlbum.Id.stringify({ _id: this._albumId });
  }

  @Field(type => SongArtistAlbum)
  @ManyToOne(type => SongArtistAlbum, album => album.songs)
  @JoinColumn({ name: '_albumId' })
  album?: SongArtistAlbum | null;

  @Column()
  albumTrack?: number;

  @OneToMany(type => SongPlaylistItem, item => item.song)
  playlistItems?: SongPlaylistItem[];

  @Field()
  @Column()
  sourceUrl!: string;

  @Field(type => Int)
  @Column()
  duration!: number;

}