import { Entity, Column, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Properties, Property, Factory } from '@type-properties/core';
import { Node, NodeIdentifierProps } from '../interfaces';
import { Post } from './Post';
import { User } from './User';

@Properties()
export class PostRatingIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    def => 'PostRating',
  );

  _viewerType = Property[1](
    type => String,
  );

  _viewerId = Property[2](
    type => String,
  );

  _postId = Property[3](
    type => String,
  );

}

export class PostRatingIdentifier extends Factory(PostRatingIdentifierProps) {}

@Entity()
export class PostRating implements Node {

  static Id = PostRatingIdentifier;

  @PrimaryColumn()
  _viewerType!: string;

  @PrimaryColumn()
  _viewerId!: string;

  @PrimaryColumn()
  _postId!: string;

  get id() {
    return PostRating.Id.stringify(this);
  }

  get viewerId() {
    switch (this._viewerType) {
      case 'User':
        return User.Id.stringify({ _id: this._viewerId });
      default:
        throw new Error(`Not a valid type`);
    }
  }

  get postId() {
    return Post.Id.stringify({ _id: this._postId });
  }

  @ManyToOne(type => Post, post => post.ratings)
  @JoinColumn({ name: '_postId' })
  post!: Post;

  @Column('tinyint')
  ratingScore!: number;

  @Column()
  createdAt!: Date;

  @Column()
  updatedAt!: Date;

}