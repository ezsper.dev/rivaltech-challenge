import { ObjectType, Field, Int } from 'type-graphql';
import { PostCategoryEdge } from './PostCategoryEdge';
import { PostCategory } from './PostCategory';
import { PageInfo } from './PageInfo';


@ObjectType()
export class PostCategoryConnection {

  @Field(type => [PostCategoryEdge])
  edges!: PostCategoryEdge[];

  @Field(type => [PostCategory])
  nodes!: PostCategory[];

  @Field(type => Int)
  totalCount!: number;

  @Field()
  info!: PageInfo;

}