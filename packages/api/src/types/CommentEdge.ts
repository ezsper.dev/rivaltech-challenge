import { ID, ObjectType, Field } from 'type-graphql';
import { Comment } from './Comment';

@ObjectType()
export class CommentEdge {

  @Field(type => ID)
  cursor!: string;

  @Field()
  node!: Comment;

}