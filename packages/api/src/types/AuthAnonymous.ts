import { Field, ObjectType, ID } from 'type-graphql';
import { Factory, Properties, Property } from '@type-properties/core';
import { NodeIdentifierProps, Auth, Node } from '../interfaces';
import { AuthType } from '../types';

@Properties()
export class AuthAnonymousIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    def => 'AuthAnonymous',
  );

}

export class AuthAnonymousIdentifier extends Factory(AuthAnonymousIdentifierProps) {}


@ObjectType({
  implements: [Node, Auth],
})
export class AuthAnonymous implements Auth, Node {

  static Id = AuthAnonymousIdentifier;

  @Field(type => ID)
  get id() {
    return AuthAnonymous.Id.stringify({});
  }

  @Field(type => AuthType)
  type = AuthType.ANONYMOUS;

}