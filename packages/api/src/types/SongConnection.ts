import { ObjectType, Field } from 'type-graphql';
import { SongEdge } from './SongEdge';
import { Song } from './Song';
import { PageInfo } from './PageInfo';

@ObjectType()
export class SongConnection {

  @Field(type => [SongEdge])
  edges!: SongEdge[];

  @Field(type => [Song])
  nodes!: Song[];

  @Field()
  info!: PageInfo;

}