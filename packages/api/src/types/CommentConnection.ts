import { ObjectType, Field, Int } from 'type-graphql';
import { CommentEdge } from './CommentEdge';
import { Comment } from './Comment';
import { PageInfo } from './PageInfo';


@ObjectType()
export class CommentConnection {

  @Field(type => [CommentEdge])
  edges!: CommentEdge[];

  @Field(type => [Comment])
  nodes!: Comment[];

  @Field(type => Int)
  totalCount!: number;

  @Field()
  info!: PageInfo;

}