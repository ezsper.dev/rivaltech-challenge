import { ID, ObjectType, Field } from 'type-graphql';
import { Post } from './Post';

@ObjectType()
export class PostEdge {

  @Field(type => ID)
  cursor!: string;

  @Field()
  node!: Post;

}