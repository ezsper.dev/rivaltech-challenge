import { ObjectType, ID, Field } from 'type-graphql';
import { Entity, PrimaryColumn, Column, ManyToOne, JoinColumn, OneToMany, Index } from 'typeorm';
import { Factory, Properties, Property } from '@type-properties/core';
import { Node, NodeIdentifierProps } from '../interfaces';
import { SongArtist } from './SongArtist';
import { Song } from './Song';

@Properties()
export class SongArtistAlbumIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    init => 'SongArtistAlbum',
  );

  _id = Property[1](
    type => String,
  );

}

export class SongArtistAlbumIdentifier extends Factory(SongArtistAlbumIdentifierProps) {}

@ObjectType({
  implements: [Node],
})
@Entity()
export class SongArtistAlbum {

  static Id = SongArtistAlbumIdentifier;

  __typename = 'SongArtistAlbum';

  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return SongArtistAlbum.Id.stringify(this);
  }


  @Field()
  @Column()
  title!: string;


  @Field()
  @Column()
  bio!: string;

  @Field(type => ID)
  @Index()
  @Column()
  _artistId!: string;

  @Field(type => ID)
  get artistId() {
    return SongArtist.Id.stringify({ _id: this._artistId });
  }

  @Field(type => SongArtist)
  @ManyToOne(type => SongArtist, artist => artist.albums)
  @JoinColumn({ name: '_artistId' })
  artist?: SongArtist;

  @Field(type => Song)
  @OneToMany(type => Song, song => song.album)
  songs?: Song[];

  @Field({ nullable: true })
  @Column({ nullable: true })
  coverImageSrc!: string;

}