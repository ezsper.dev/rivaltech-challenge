import { ObjectType, ID, Field } from 'type-graphql';
import { Entity, PrimaryColumn, OneToMany, Column } from 'typeorm';
import { Factory, Properties, Property } from '@type-properties/core';
import { Node, NodeIdentifierProps } from '../interfaces';
import { SongArtistAlbum } from './SongArtistAlbum';
import { Song } from './Song';

@Properties()
export class SongArtistIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    init => 'SongArtist',
  );

  _id = Property[1](
    type => String,
  );

}

export class SongArtistIdentifier extends Factory(SongArtistIdentifierProps) {}

@ObjectType({
  implements: [Node],
})
@Entity()
export class SongArtist {

  static Id = SongArtistIdentifier;

  __typename = 'SongArtist';

  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return SongArtist.Id.stringify(this);
  }

  @Field()
  @Column()
  title!: string;

  @Field()
  @Column()
  bio!: string;

  @Field(type => [SongArtistAlbum])
  @OneToMany(type => SongArtistAlbum, songArtistAlbum => songArtistAlbum.artist)
  albums!: SongArtistAlbum[];

  @Field(type => [Song])
  @OneToMany(type => Song, song => song.artist)
  songs!: Song[];

  @Field({ nullable: true })
  @Column({ nullable: true })
  pictureSrc!: string;


}