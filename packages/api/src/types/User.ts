import { ID, ObjectType, Field } from 'type-graphql';
import { Entity, PrimaryColumn, Column, Index } from 'typeorm';
import { Factory, Properties, Property } from '@type-properties/core';
import { NodeIdentifierProps, Node, PostOwner, SongPlaylistOwner, Viewer } from '../interfaces';

@Properties()
export class UserIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    def => 'User',
  );

  _id = Property[1](
    type => String,
  );

}

export class UserIdentifier extends Factory(UserIdentifierProps) {}

@ObjectType({
  implements: [Node, Viewer, PostOwner, SongPlaylistOwner]
})
@Entity()
export class User implements PostOwner, Viewer {

  static Id = UserIdentifier;

  @Field()
  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return User.Id.stringify(this);
  }

  @Field()
  @Column()
  displayName!: string;

  @Field()
  initials!: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  avatarSrc!: string;

  @Column({ nullable: true })
  @Index()
  _githubId!: string;

}