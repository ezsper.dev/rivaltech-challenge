import { ObjectType, Field } from 'type-graphql';
import { SongArtistAlbumEdge } from './SongArtistAlbumEdge';
import { SongArtistAlbum } from './SongArtistAlbum';
import { PageInfo } from './PageInfo';

@ObjectType()
export class SongArtistAlbumConnection {

  @Field(type => [SongArtistAlbumEdge])
  edges!: SongArtistAlbumEdge[];

  @Field(type => [SongArtistAlbum])
  nodes!: SongArtistAlbum[];

  @Field()
  info!: PageInfo;

}