import { ID, ObjectType, Field } from 'type-graphql';
import { Entity, Column, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Properties, Property, Factory } from '@type-properties/core';
import { PostOwner } from '../interfaces';
import { Post } from './Post';

@Properties()
export class CommentIdentifierProps {

  __typename = Property[0](
    type => String,
    def => 'Comment',
  );

  _id = Property[1](
    type => String,
  );

}

export class CommentIdentifier extends Factory(CommentIdentifierProps) {}

@ObjectType()
@Entity()
export class Comment {

  static Id = CommentIdentifier;

  @Field()
  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return Comment.Id.stringify(this);
  }

  @Field()
  @Column()
  content!: string;

  @Field(type => ID)
  @Column()
  authorId!: string;

  @Field()
  author?: PostOwner;

  @Field()
  @Column()
  _postId!: string;

  @Field(type => ID)
  get postId() {
    return Post.Id.stringify({ _id: this._postId });
  }

  @Field(type => Post)
  @ManyToOne(type => Post, post => post.comments)
  @JoinColumn({ name: '_postId' })
  post?: Post;

  @Field()
  @Column()
  createdAt!: Date;

  @Field()
  @Column()
  publishedAt!: Date;

  @Field()
  @Column()
  updatedAt!: Date;

}