import { ObjectType, Field, Int } from 'type-graphql';
import { PostEdge } from './PostEdge';
import { Post } from './Post';
import { PageInfo } from './PageInfo';


@ObjectType()
export class PostConnection {

  @Field(type => [PostEdge])
  edges!: PostEdge[];

  @Field(type => [Post])
  nodes!: Post[];

  @Field(type => Int)
  totalCount!: number;

  @Field()
  info!: PageInfo;

}