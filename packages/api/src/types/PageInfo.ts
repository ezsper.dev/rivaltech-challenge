import { ID, ObjectType, Field } from 'type-graphql';

@ObjectType()
export class PageInfo {

  @Field(type => ID, { nullable: true })
  endCursor!: string;

  @Field(type => ID, { nullable: true })
  startCursor!: string;

  @Field(type => Boolean)
  hasPrevPage = false;

  @Field(type => Boolean)
  hasNextPage = false;

}