import { ID, ObjectType, Field } from 'type-graphql';
import { SongPlaylist } from './SongPlaylist';

@ObjectType()
export class SongPlaylistEdge {

  @Field(type => ID)
  cursor!: string;

  @Field()
  node!: SongPlaylist;

}