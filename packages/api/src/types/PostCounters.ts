import { ObjectType, Field, ID, Int } from 'type-graphql';
import { Entity, Column, PrimaryColumn } from 'typeorm';
import { Factory, Properties, Property } from '@type-properties/core';
import { NodeIdentifierProps } from '../interfaces';

@Properties()
export class PostCountersIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    def => 'PostCounters',
  );

  _id = Property[1](
    type => String,
  );

}

export class PostCountersIdentifier extends Factory(PostCountersIdentifierProps) {}

@ObjectType()
@Entity()
export class PostCounters {

  static Id = PostCountersIdentifier;

  @Field()
  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return PostCounters.Id.stringify(this);
  }

  @Field(type => Int)
  @Column({ default: 0 })
  comments!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  raters!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore1!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore2!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore3!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore4!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore5!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore6!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore7!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore8!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore9!: number;

  @Field(type => Int)
  @Column({ default: 0 })
  ratersOnScore10!: number;

}