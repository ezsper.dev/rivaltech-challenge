import { ObjectType, Field } from 'type-graphql';
import { SongArtist } from './SongArtist';
import { SongArtistAlbum } from './SongArtistAlbum';
import { Song } from './Song';

@ObjectType()
export class SongSearchResult {

  @Field(type => [SongArtist])
  artistResults!: SongArtist[];

  @Field(type => [SongArtistAlbum])
  albumResults!: SongArtistAlbum[];

  @Field(type => [Song])
  songs!: Song[];

}