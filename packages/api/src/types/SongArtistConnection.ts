import { ObjectType, Field } from 'type-graphql';
import { SongArtistEdge } from './SongArtistEdge';
import { SongArtist } from './SongArtist';
import { PageInfo } from './PageInfo';

@ObjectType()
export class SongArtistConnection {

  @Field(type => [SongArtistEdge])
  edges!: SongArtistEdge[];

  @Field(type => [SongArtist])
  nodes!: SongArtist[];

  @Field()
  info!: PageInfo;

}