import { ID, ObjectType, Field } from 'type-graphql';
import { Entity, Column, PrimaryColumn, Unique, OneToMany } from 'typeorm';
import { Properties, Property, Factory } from '@type-properties/core';
import { Node, NodeIdentifierProps } from '../interfaces';
import { Post } from './Post';
import { PostCategoryCounters } from './PostCategoryCounters';

@Properties()
export class PostCategoryIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    def => 'PostCategory',
  );

  _id = Property[1](
    type => String,
  );

}

export class PostCategoryIdentifier extends Factory(PostCategoryIdentifierProps) {}

@ObjectType({
  implements: [Node],
})
@Entity()
@Unique('name_idx', ['name'])
export class PostCategory implements Node {

  static Id = PostCategoryIdentifier;

  @Field()
  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return PostCategory.Id.stringify(this);
  }

  @Field()
  @Column()
  name!: string;

  @Field()
  @Column()
  title!: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  description!: string;

  @OneToMany(type => Post, post => post.category)
  posts!: Post[];

  counters!: PostCategoryCounters;

}