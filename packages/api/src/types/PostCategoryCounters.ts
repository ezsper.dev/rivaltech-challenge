import { ObjectType, Field, ID, Int } from 'type-graphql';
import { Entity, Column, PrimaryColumn } from 'typeorm';
import { Factory, Properties, Property } from '@type-properties/core';
import { NodeIdentifierProps } from '../interfaces';

@Properties()
export class TagCountersIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    def => 'PostCounters',
  );

  _id = Property[1](
    type => String,
  );

}

export class TagCountersIdentifier extends Factory(TagCountersIdentifierProps) {}

@ObjectType()
@Entity()
export class PostCategoryCounters {

  static Id = TagCountersIdentifier;

  @Field()
  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return PostCategoryCounters.Id.stringify(this);
  }

  @Field(type => Int)
  @Column()
  posts?: number;

}