import { ID, Field } from 'type-graphql';
import { Entity, PrimaryColumn, Column } from 'typeorm';
import { Factory, Properties, Property } from '@type-properties/core';
import { NodeIdentifierProps } from '../interfaces';

@Properties()
export class AuthCertIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
  );

  _id = Property[1](
    type => String,
  );

}

export class AuthCertIdentifier extends Factory(AuthCertIdentifierProps) {}

@Entity()
export class AuthCert {

  static Id = AuthCertIdentifier;

  @PrimaryColumn()
  @Column()
  __typename!: string;

  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return AuthCert.Id.stringify(this);
  }

  @Column()
  algorithm!: string;

  @Column()
  passphrase!: string;

  @Column()
  privateKey!: string;

  @Column()
  publicKey!: string;

}