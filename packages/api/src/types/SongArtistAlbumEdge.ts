import { ID, ObjectType, Field } from 'type-graphql';
import { SongArtistAlbum } from './SongArtistAlbum';

@ObjectType()
export class SongArtistAlbumEdge {

  @Field(type => ID)
  cursor!: string;

  @Field()
  node!: SongArtistAlbum;

}