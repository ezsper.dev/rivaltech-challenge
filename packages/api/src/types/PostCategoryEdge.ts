import { ID, ObjectType, Field } from 'type-graphql';
import { PostCategory } from './PostCategory';

@ObjectType()
export class PostCategoryEdge {

  @Field(type => ID)
  cursor!: string;

  @Field(type => ID)
  node!: PostCategory;

}