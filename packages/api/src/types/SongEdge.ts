import { ID, ObjectType, Field } from 'type-graphql';
import { Song } from './Song';

@ObjectType()
export class SongEdge {

  @Field(type => ID)
  cursor!: string;

  @Field()
  node!: Song;

}