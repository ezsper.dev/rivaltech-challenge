import { ObjectType, ID, Field } from 'type-graphql';
import { Entity, PrimaryColumn, Column, ManyToOne, JoinColumn, Index } from 'typeorm';
import { Factory, Properties, Property } from '@type-properties/core';
import { Node, NodeIdentifierProps } from '../interfaces';
import { Song } from './Song';
import { SongPlaylist } from './SongPlaylist';

@Properties()
export class SongPlaylistItemIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    init => 'SongPlaylistItem',
  );

  _playlistId = Property[1](
    type => String,
  );

  _songId = Property[2](
    type => String,
  );

}

export class SongPlaylistItemIdentifier extends Factory(SongPlaylistItemIdentifierProps) {}

@ObjectType({
  implements: [Node],
})
@Entity()
export class SongPlaylistItem {

  static Id = SongPlaylistItemIdentifier;

  __typename = 'SongPlaylistItem';

  @Field(type => ID)
  @Index()
  @PrimaryColumn()
  _songId!: string;

  @Field(type => ID)
  get id() {
    return SongPlaylistItem.Id.stringify(this);
  }

  @Field(type => ID)
  @Index()
  @PrimaryColumn()
  _playlistId!: string;

  @Field(type => ID)
  get playlistId() {
    return SongPlaylist.Id.stringify({ _id: this._playlistId });
  }

  @Field(type => SongPlaylist)
  @ManyToOne(type => SongPlaylist, playlist => playlist.items)
  @JoinColumn({ name: '_playlistId' })
  playlist!: SongPlaylist;

  @Field()
  @Index()
  @Column()
  sortOrder!: number;

  @Field(type => ID)
  get songId() {
    return Song.Id.stringify({ _id: this._songId });
  }

  @Field(type => Song)
  @ManyToOne(type => Song, song => song.playlistItems)
  @JoinColumn({ name: '_songId' })
  song!: Song;

  @Column()
  authorId!: string;

  @Field()
  @Column()
  createdAt!: Date;

  @Field()
  @Column()
  updatedAt!: Date;

}