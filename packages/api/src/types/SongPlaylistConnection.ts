import { ObjectType, Field } from 'type-graphql';
import { SongPlaylistEdge } from './SongPlaylistEdge';
import { SongPlaylist } from './SongPlaylist';
import { PageInfo } from './PageInfo';

@ObjectType()
export class SongPlaylistConnection {

  @Field(type => [SongPlaylistEdge])
  edges!: SongPlaylistEdge[];

  @Field(type => [SongPlaylist])
  nodes!: SongPlaylist[];

  @Field()
  info!: PageInfo;

}