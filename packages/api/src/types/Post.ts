import { ID, ObjectType, Field, Int } from 'type-graphql';
import { Entity, Column, PrimaryColumn, OneToMany, ManyToOne, Index, JoinColumn } from 'typeorm';
import { Properties, Property, Factory } from '@type-properties/core';
import { Node, NodeIdentifierProps, PostOwner } from '../interfaces';
import { PostCounters } from './PostCounters';
import { Comment } from './Comment';
import { PostCategory } from './PostCategory';
import { PostRating } from './PostRating';

@Properties()
export class PostIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    def => 'Post',
  );

  _id = Property[1](
    type => String,
  );

}

export class PostIdentifier extends Factory(PostIdentifierProps) {}

@ObjectType({
  implements: [Node],
})
@Entity({})
export class Post implements Node {

  static Id = PostIdentifier;

  @Field()
  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return Post.Id.stringify(this);
  }

  @Field()
  @Index({ fulltext: true })
  @Column()
  title!: string;

  @Field()
  @Column()
  name!: string;

  @Field()
  link!: string;

  @Field({ nullable: true })
  @Index({ fulltext: true })
  @Column({ nullable: true })
  subheading!: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  thumbnailSrc!: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  posterSrc!: string;

  @Field()
  @Index({ fulltext: true })
  @Column()
  content!: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  excerpt!: string;

  @Field(type => ID)
  @Column()
  authorId!: string;

  @Field()
  author!: PostOwner;

  @Field()
  counters!: PostCounters;

  @Field(type => Int)
  ratingScore!: number;

  @Field(type => Int, { nullable: true })
  viewerRatingScore!: number | null;

  @OneToMany(type => Comment, comment => comment.post)
  comments!: Comment[];

  @Field(type => ID)
  @Index()
  @Column()
  _categoryId!: string;

  @Field(type => ID)
  get categoryId() {
    return PostCategory.Id.stringify({ _id: this._categoryId });
  }

  @Field()
  @Column()
  createdAt!: Date;

  @Field()
  @Column()
  updatedAt!: Date;

  @Field()
  @Column()
  publishedAt!: Date;

  @ManyToOne(type => PostCategory, category => category.posts)
  @JoinColumn({ name: '_categoryId' })
  @Field(type => PostCategory)
  category!: PostCategory;

  @OneToMany(type => PostRating, rating => rating.post)
  ratings!: PostRating[];

}