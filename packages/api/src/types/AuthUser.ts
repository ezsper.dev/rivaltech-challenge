import { Field, ObjectType, ID } from 'type-graphql';
import { Factory, Properties, Property } from '@type-properties/core';
import { NodeIdentifierProps, Auth, Node } from '../interfaces';
import { AuthType, User } from '../types';

@Properties()
export class AuthUserIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    def => 'AuthUser',
  );

  _id = Property[1](
    type => String,
  );

}

export class AuthUserIdentifier extends Factory(AuthUserIdentifierProps) {}

@ObjectType({
  implements: [Auth, Node],
})
export class AuthUser implements Auth, Node {

  static Id = AuthUserIdentifier;

  @Field(type => ID)
  get id() {
    return AuthUser.Id.stringify({ _id: this._userId });
  }

  @Field(type => AuthType)
  type = AuthType.USER;

  @Field(type => ID)
  get viewerId() {
    return this.userId;
  }

  @Field(type => User)
  get viewer() {
    return this.user;
  }

  @Field()
  _userId!: string;

  @Field(type => ID)
  userId!: string;

  @Field()
  user!: User;

}