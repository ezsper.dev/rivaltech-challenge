import { ObjectType, ID, Field } from 'type-graphql';
import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm';
import { Factory, Properties, Property } from '@type-properties/core';
import { Node, NodeIdentifierProps, SongPlaylistOwner } from '../interfaces';
import { SongPlaylistItem } from './SongPlaylistItem';
import { SongPlaylistPrivacyType } from '../enums';

@Properties()
export class SongPlaylistIdentifierProps implements NodeIdentifierProps {

  __typename = Property[0](
    type => String,
    init => 'SongPlaylist',
  );

  _id = Property[1](
    type => String,
  );

}

export class SongPlaylistIdentifier extends Factory(SongPlaylistIdentifierProps) {}

@ObjectType({
  implements: [Node],
})
@Entity()
export class SongPlaylist {

  static Id = SongPlaylistIdentifier;

  __typename = 'SongPlaylist';

  @Field()
  @PrimaryColumn()
  _id!: string;

  @Field(type => ID)
  get id() {
    return SongPlaylist.Id.stringify(this);
  }

  @Field()
  @Column()
  title!: string;

  @OneToMany(type => SongPlaylistItem, item => item.playlist)
  @Field(type => [SongPlaylistItem])
  items!: SongPlaylistItem[];

  @Field(type => ID)
  @Column()
  authorId!: string;

  @Field()
  author!: SongPlaylistOwner;

  @Field()
  @Column('text')
  privacy!: SongPlaylistPrivacyType;

  @Field()
  @Column()
  createdAt!: Date;

  @Field()
  @Column()
  updatedAt!: Date;

}