import 'reflect-metadata';
import { resolve as resolvePath } from 'path';
import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { buildSchema } from 'type-graphql';
import { AuthService, PassportService } from './services';
import { createContext, getContextFromRequest } from './helpers';
import { asyncMiddleware } from './helpers/asyncMiddleware';
import { formatError } from './helpers/formatError';
import { formatResponse } from './helpers/formatResponse';
import cors from 'cors';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import { config } from './config';

export const app = express();

asyncMiddleware(app); // enables async middlewares

async function bootstrap() {
  const { authenticator } = PassportService;
  const schema = await buildSchema({
    emitSchemaFile: resolvePath(__dirname, '__snapshots__/schema/schema.graphql'),
    resolvers: [resolvePath(__dirname, './resolvers/**/*Resolver.ts')],
  });

  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));

  if (process.env.NODE_ENV !== 'production') {
    app.use(cookieParser());
    app.use(session({secret: 'development', resave: true, saveUninitialized: true}));
  }

  app.use(async (req, res, next) => {
    const token = req.headers.authorization;
    if (token) {
      const ctx = getContextFromRequest(req);
      await AuthService.loadAuth(ctx, token);
    }
    next();
  });

  app.use((error: Error, req: any, res: any, next: any) => {
    res.json(formatResponse({ data: null, errors: [error] }));
  });

  app.use(authenticator.initialize());

  if (process.env.NODE_ENV !== 'production') {
    app.use(authenticator.session());
  }

  app.get('/auth/github',
    authenticator.authenticate('github'));

  app.get('/auth/github/callback',
    authenticator.authenticate('github', { failureRedirect: config.permalinks.auth.failureRedirectUri }),
    async function(req, res) {
      // Successful authentication, redirect home.
      const ctx = getContextFromRequest(req);
      const token = await AuthService.signAuth(ctx);
      res.redirect(config.permalinks.auth.tokenRedirectUri.replace('%token%', encodeURIComponent(token)));
    });

  app.get('/_healthcheck', (req, res) => {
    const response = {
      data: {
        stat: 'ok',
        time: Date.now(),
      },
    };
    res.json(formatResponse(response));
  });

  const graphqlServer = new ApolloServer({
    schema,
    context: createContext,
    formatError: formatError,
    playground: {
      settings: {
        'editor.theme': 'light',
        ...(process.env.NODE_ENV !== 'production' && {
          'request.credentials': 'include',
        })
      },
      tabs: [
        {
          endpoint: '/',
          query: `query {}`,
        },
      ],
    },
  });

  graphqlServer.applyMiddleware({ app, path: '/' });

  return { graphqlServer };

  // other initialization code, like creating http server
}

bootstrap()
  .then(({ graphqlServer }) => {
    app.listen({ port: 7080 }, () =>
      console.log(`🚀 Server ready at http://localhost:7080${graphqlServer.graphqlPath}`)
    );
  })
  .catch(error => {
    console.error(error);
  });