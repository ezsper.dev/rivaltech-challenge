import { ArgsType, Field } from 'type-graphql';
import { PaginationArgs } from './PaginationArgs';

@ArgsType()
export abstract class QueryMySongPlaylistsArgs extends PaginationArgs {

  @Field(type => String, { nullable: true })
  searchTerm?: string;

}