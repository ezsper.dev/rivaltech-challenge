import { ArgsType, Field, ID } from 'type-graphql';
import { PaginationArgs } from './PaginationArgs';

@ArgsType()
export abstract class QueryCommentsArgs extends PaginationArgs {

  @Field(type => ID)
  postId!: string;

}