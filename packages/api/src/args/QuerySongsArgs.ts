import { ArgsType, Field } from 'type-graphql';
import { PaginationArgs } from './PaginationArgs';

@ArgsType()
export abstract class QuerySongsArgs extends PaginationArgs {

  @Field({ nullable: true })
  searchTerm!: string;

}