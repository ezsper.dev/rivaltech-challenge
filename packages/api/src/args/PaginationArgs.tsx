import { ArgsType, Field, Int, ID } from 'type-graphql';
import { Min, Max } from 'class-validator';

@ArgsType()
export abstract class PaginationArgs {

  @Min(1)
  @Max(500)
  @Field(type => Int, { nullable: true })
  first?: number | null;

  @Min(1)
  @Max(500)
  @Field(type => Int, { nullable: true })
  last?: number | null;

  @Field(type => ID, { nullable: true })
  before?: string;

  @Field(type => ID, { nullable: true })
  after?: string;
}