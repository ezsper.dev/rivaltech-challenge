import { ArgsType, Field, ID } from 'type-graphql';
import { PaginationArgs } from './PaginationArgs';

@ArgsType()
export abstract class QueryPostsArgs extends PaginationArgs {

  @Field(type => ID, { nullable: true })
  categories!: string[];

  @Field({ nullable: true })
  searchTerm!: string;

}