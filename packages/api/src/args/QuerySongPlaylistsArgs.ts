import { ArgsType, Field, ID } from 'type-graphql';
import { PaginationArgs } from './PaginationArgs';

@ArgsType()
export abstract class QuerySongPlaylistsArgs extends PaginationArgs {

  @Field(type => String, { nullable: true })
  searchTerm?: string;

  @Field(type => ID,{ nullable: true })
  authorId?: string;

}