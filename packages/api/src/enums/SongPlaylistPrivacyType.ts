import { registerEnumType } from 'type-graphql';

export enum SongPlaylistPrivacyType {
  PRIVATE = 'PRIVATE',
  UNLISTED = 'UNLISTED',
  PUBLIC = 'PUBLIC',
}

registerEnumType(SongPlaylistPrivacyType, {
  name: 'SongPlaylistPrivacyType',
});