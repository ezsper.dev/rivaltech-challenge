import { InterfaceType, Field } from 'type-graphql';

@InterfaceType()
export class Payload {

  @Field(type => String, { nullable: true })
  clientMutationId?: string;

}