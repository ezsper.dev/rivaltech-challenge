import { InterfaceType, Field, ID } from 'type-graphql';
import { Viewer } from './Viewer';
import { AuthType } from '../types';

@InterfaceType()
export abstract class Auth {

  @Field(type => AuthType)
  type!: AuthType;

  @Field(type => ID, { nullable: true })
  viewerId?: string;

  @Field(type => Viewer, { nullable: true })
  viewer?: Viewer | null;

}