import { ID, InterfaceType, Field } from 'type-graphql';
import { Properties, Property, Factory } from '@type-properties/core';

@Properties()
export class NodeIdentifierProps {

  __typename = Property[0](
    type => String,
  );

}

export class NodeIdentifier extends Factory(NodeIdentifierProps) {}

@InterfaceType()
export abstract class Node {

  static Id = NodeIdentifier;

  @Field(type => ID)
  id!: string;

}