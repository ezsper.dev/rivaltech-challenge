import { ID, InterfaceType, Field } from 'type-graphql';
import { Node } from './Node';

@InterfaceType()
export abstract class Viewer implements Node {

  @Field(type => ID)
  id!: string;

  @Field()
  displayName!: string;

  @Field()
  initials!: string;

  @Field({ nullable: true })
  avatarSrc!: string;

}