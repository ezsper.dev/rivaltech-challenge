import { ID, InterfaceType, Field } from 'type-graphql';
import { Node } from './Node';
import { Viewer } from './Viewer';

@InterfaceType()
export abstract class PostOwner implements Node, Viewer {

  @Field(type => ID)
  id!: string;

  @Field()
  displayName!: string;

  @Field()
  initials!: string;

  @Field({ nullable: true })
  avatarSrc!: string;

}