import { InputType, Field } from 'type-graphql';

@InputType()
export abstract class Input {

  @Field(type => String, { nullable: true })
  clientMutationId?: string;

}