import { ID, ObjectType, Field } from 'type-graphql';
import { Payload } from '../interfaces';
import { SongPlaylist } from '../types';

@ObjectType({
  implements: [Payload]
})
export class SongPlaylistPayload implements Payload {

  @Field(type => String, { nullable: true })
  clientMutationId?: string;

  @Field()
  _songPlaylistId!: string;

  @Field(type => ID)
  get songPlaylistId() {
    return SongPlaylist.Id.stringify({ _id: this._songPlaylistId });
  };

  @Field()
  songPlaylist!: SongPlaylist;

}