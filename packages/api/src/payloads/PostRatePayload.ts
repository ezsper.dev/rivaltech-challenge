import { ID, ObjectType, Field, Float } from 'type-graphql';
import { Payload } from '../interfaces';
import { Post } from '../types';

@ObjectType({
  implements: [Payload]
})
export class PostRatePayload implements Payload {

  @Field(type => String, { nullable: true })
  clientMutationId?: string;

  @Field()
  _postId!: string;

  @Field(type => ID)
  get postId() {
    return Post.Id.stringify({ _id: this._postId });
  };

  @Field(type => Float)
  ratingScore!: number;

}