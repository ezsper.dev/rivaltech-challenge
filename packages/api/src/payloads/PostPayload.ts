import { ID, ObjectType, Field } from 'type-graphql';
import { Payload } from '../interfaces';
import { Post, PostCounters } from '../types';

@ObjectType({
  implements: [Payload]
})
export class PostPayload implements Payload {

  @Field(type => String, { nullable: true })
  clientMutationId?: string;

  @Field()
  _postId!: string;

  @Field(type => ID)
  get postId() {
    return Post.Id.stringify({ _id: this._postId });
  };

  @Field()
  post!: Post;

  @Field()
  postCounters!: PostCounters;

}