import { ID, ObjectType, Field } from 'type-graphql';
import { Payload } from '../interfaces';
import { PostCategory } from '../types';

@ObjectType({
  implements: [Payload]
})
export class PostCategoryPayload implements Payload {

  @Field(type => String, { nullable: true })
  clientMutationId?: string;

  @Field()
  _categoryId!: string;

  @Field(type => ID)
  get categoryId() {
    return PostCategory.Id.stringify({ _id: this._categoryId });
  };

  @Field()
  category!: PostCategory;

}