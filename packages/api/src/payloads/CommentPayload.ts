import { ObjectType, Field, ID } from 'type-graphql';
import { Payload } from '../interfaces';
import { Comment } from '../types';

@ObjectType({
  implements: [Payload]
})
export class CommentPayload implements Payload {

  @Field(type => String, { nullable: true })
  clientMutationId?: string;

  @Field()
  _commentId!: string;

  @Field(type => ID)
  get commentId() {
    return Comment.Id.stringify({ _id: this._commentId });
  };

  @Field()
  comment!: Comment;

}