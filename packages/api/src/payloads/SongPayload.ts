import { ID, ObjectType, Field } from 'type-graphql';
import { Payload } from '../interfaces';
import { Song } from '../types';

@ObjectType({
  implements: [Payload]
})
export class SongPayload implements Payload {

  @Field(type => String, { nullable: true })
  clientMutationId?: string;

  @Field()
  _songId!: string;

  @Field(type => ID)
  get songId() {
    return Song.Id.stringify({ _id: this._songId });
  };

  @Field()
  song!: Song;

}