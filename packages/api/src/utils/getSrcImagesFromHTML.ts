const matchUrlRegExp = /<img[^>]+src="?([^"\s]+)"?[^>]*\/?>/g;

export function getSrcImagesFromHTML(html: string) {
  const urls: string[] = [];
  let m: RegExpMatchArray | null;
  while (m = matchUrlRegExp.exec( html ) ) {
    urls.push(m[1]);
  }
  return urls;
}