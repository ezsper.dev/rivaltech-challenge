import DataLoader from 'dataloader';
import { In, ObjectType } from 'typeorm';
import { memoCtx } from './memoCtx';
import { RepositoryService } from '../services';
import { EntityNotFoundError } from '../errors';

export type Key = string;

export type EntityFields = { id: string };

export type EntityType<Entity extends EntityFields> = ObjectType<Entity> & {
  Id: {
    getPropertyNames(): ['__typename', ...any[]];
    decode(id: string): {
      toPlainObject(): any;
      toValues(): any[];
      stringify(): string;
      toJSONValues(): any;
    },
    stringify(key :any): string
  };
};

/**
 * Creates an entity loader based on Node interfaces
 * @param target
 */
export function createEntityLoader<Entity extends EntityFields>(target: EntityType<Entity>) {
  return memoCtx(ctx => {
    const propertyNames = target.Id.getPropertyNames();
    const lastProperty = propertyNames[propertyNames.length - 1];
    return new DataLoader<Key, Entity>(async (keys) => {
      const client = await RepositoryService.connect(ctx);
      const postRepository = client.getRepository(target);
      const groupMap: Record<string, {
        partitionColumns: any[];
        clusterColumnMap: Record<string, any>;
        clusterColumnIn: any[];
      }> = {};
      const groupKeys: string[] = [];

      keys.forEach(id => {
        let entity: ReturnType<typeof target['Id']['decode']>;
        try {
          entity = target.Id.decode(id);
        } catch (error) {
          return;
        }
        const jsonValues = entity.toJSONValues()
          .map((value: any) => JSON.stringify(value));
        const groupKey = jsonValues.length > 2
          ? jsonValues.slice(1, jsonValues.length - 1).join(':')
          : '';
        const clusterKey = jsonValues[jsonValues.length - 1];
        let clusterValue: any;
        if (!groupMap[groupKey]) {
          const values = Object.entries(entity.toPlainObject());
          groupKeys.push(groupKey);
          groupMap[groupKey] = {
            partitionColumns: Object.fromEntries(values.slice(1, jsonValues.length - 1)) as any,
            clusterColumnMap: {},
            clusterColumnIn: [],
          };
          clusterValue = values[jsonValues.length - 1][1];
        } else {
          clusterValue = entity[lastProperty];
        }

        const { clusterColumnMap, clusterColumnIn } = groupMap[groupKey];
        if (!clusterColumnMap.hasOwnProperty(clusterKey)) {
          clusterColumnMap[clusterKey] = clusterValue;
          clusterColumnIn.push(clusterValue);
        }

        return entity.stringify();
      });

      const index: Record<string, Entity> = {};

      for (const groupKey of groupKeys) {
        const { partitionColumns, clusterColumnIn } = groupMap[groupKey];

        const found = await postRepository.find({
          where: {
            ...partitionColumns,
            [lastProperty]: In(clusterColumnIn),
          },
        });
        for (const entity of found) {
          index[entity.id] = entity;
        }
      }

      return keys.map(key => index[key] || new EntityNotFoundError((target as any).name, key));
    });
  })
}