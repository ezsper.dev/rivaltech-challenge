import { Context } from './Context';

export function memoCtx<T, Args extends any[]>(fn: (ctx: Context, ...args: Args) => T): (ctx: Context, ...args: Args) => T {
  return fn;
}