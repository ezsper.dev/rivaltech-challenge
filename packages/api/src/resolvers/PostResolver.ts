import {
  Resolver,
  Query,
  FieldResolver,
  Ctx,
  Root,
  Arg,
  Args,
  Mutation,
  ResolverInterface,
  ID,
  Int,
} from 'type-graphql';
import { In, Like } from 'typeorm';
import { Properties, Property, Factory } from '@type-properties/core';
import truncate from 'lodash/truncate';
import { generate } from 'shortid';
import striptags from 'striptags';
import slugify from 'slugify';
import { Context } from '../helpers';
import {
  Post,
  PostCounters,
  PostCategoryCounters,
  PostConnection,
  PostEdge,
  PageInfo,
  PostRating,
  PostCategory
} from '../types';
import { PostInput, PostRateInput } from '../inputs';
import { QueryPostsArgs } from '../args';
import { Input, NodeIdentifier, Payload, PostOwner } from '../interfaces';
import { PostPayload, PostRatePayload } from '../payloads';
import { PostLoader, PostRatingLoader, PostCategoryLoader, PostCountersLoader, UserLoader } from '../loaders';
import { RepositoryService, AuthService } from '../services';
import { getSrcImagesFromHTML } from '../utils/getSrcImagesFromHTML';
import { config } from '../config';
import { EntityNotFoundError } from '../errors';

@Properties()
class PostsCursorProps {

  __typename = Property[0](
    type => String,
    def => 'PostsCursor',
  );

  _postId = Property[1](
    type => String,
  );

}

export class PostsCursor extends Factory(PostsCursorProps) {}

@Resolver(of => Post)
export class PostResolver implements ResolverInterface<Post> {

  @FieldResolver(type => PostOwner)
  author(@Root() post: Post, @Ctx() ctx: Context) {
    const { __typename } = NodeIdentifier.decode(post.authorId);
    switch (__typename) {
      case 'User': {
        return UserLoader(ctx).load(post.authorId);
      }
      default:
        throw new Error(`Invalid author id`);
    }
  }

  @FieldResolver(type => String)
  excerpt(
    @Root() post: Post,
    @Ctx() ctx: Context,
    @Arg('resolve', type => Boolean,{ defaultValue: false }) resolve = false,
  ) {
    if (post.excerpt || !resolve) {
      return post.excerpt || '';
    }
    const source = (post.subheading || striptags(post.content));
    if (source) {
      return truncate(source, {
        'length': 100,
        'separator': /,? +/,
      });
    }
    return '';
  }

  @FieldResolver(type => PostCounters)
  counters(@Root() post: Post, @Ctx() ctx: Context) {
    const id = PostCounters.Id.stringify({ _id: post._id });
    return PostCountersLoader(ctx).load(id);
  }

  @FieldResolver(type => String)
  link(@Root() post: Post, @Ctx() ctx: Context) {
    return config.permalinks.post.link
      .replace(/%id%/, post.id)
      .replace(/%name%/, post.name);
  }

  @FieldResolver()
  async ratingScore(@Root() post: Post, @Ctx() ctx: Context) {
    if (post.ratingScore) {
      return post.ratingScore;
    }
    const id = PostCounters.Id.stringify({ _id: post._id });
    const {
      raters,
      ratersOnScore1,
      ratersOnScore2,
      ratersOnScore3,
      ratersOnScore4,
      ratersOnScore5,
      ratersOnScore6,
      ratersOnScore7,
      ratersOnScore8,
      ratersOnScore9,
      ratersOnScore10,
    } = await PostCountersLoader(ctx).load(id);
    const total = ratersOnScore1
      + (ratersOnScore2 * 2)
      + (ratersOnScore3 * 3)
      + (ratersOnScore4 * 4)
      + (ratersOnScore5 * 5)
      + (ratersOnScore6 * 6)
      + (ratersOnScore7 * 7)
      + (ratersOnScore8 * 8)
      + (ratersOnScore9 * 9)
      + (ratersOnScore10 * 10);
    const ratingScore = Math.round(total/raters);// Math.round((total/raters)*2)/2;
    if (isNaN(ratingScore)) {
      return 0;
    }
    return ratingScore;
  }

  @FieldResolver()
  async viewerRatingScore(@Root() post: Post, @Ctx() ctx: Context) {
    return await this.viewerPostRatingScore(ctx, post.id);
  }

  @FieldResolver(type => String)
  category(@Root() post: Post, @Ctx() ctx: Context) {
    if (post.category) {
      return post.category;
    }
    return PostCategoryLoader(ctx).load(post.categoryId);
  }

  @Query(type => Post)
  post(@Ctx() ctx: Context, @Arg('id', type => ID) id: string) {
    return PostLoader(ctx).load(id);
  }

  @Query(type => Post)
  async postByShortId(@Ctx() ctx: Context, @Arg('_id') _id: string) {
    try {
      return await PostLoader(ctx).load(Post.Id.stringify({_id}));
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        error.extensions.key = { _id };
      }
      throw error;
    }
  }

  @Query(type => PostConnection)
  async posts(@Ctx() ctx: Context, @Args() args: QueryPostsArgs) {
    const client = await RepositoryService.connect(ctx);
    const postRepository = client.getRepository(Post);
    const statm = postRepository.createQueryBuilder()
      .select();
    if (args.searchTerm) {
      const searchTerm = args.searchTerm.trim();
      // Not a Full Text Search, but like will do the trick for now
      statm.where([
        { title: Like(`%${searchTerm}%`)},
        { subheading: Like(`%${searchTerm}%`)},
        { content: Like(`%${searchTerm}%`) }
      ]);
    }
    if (args.categories && args.categories.length > 0) {
      statm.where({
        _categoryId: In(args.categories.map(id => PostCategory.Id.decode(id)._id)),
      });
    }
    statm.orderBy('publishedAt', 'DESC');
    const posts = await statm.getMany();
    const connection = new PostConnection();
    connection.nodes = posts;
    const info = new PageInfo();
    info.hasPrevPage = false;
    info.hasNextPage = false;
    connection.info = info;
    connection.edges = posts.map(post => {
      const edge = new PostEdge();
      edge.cursor = PostsCursor.stringify({ _postId: post._id });
      edge.node = post;
      return edge;
    });
    return connection;
  }

  @Mutation(type => PostPayload)
  async createPost(@Ctx() ctx: Context, @Arg('input') input: PostInput) {
    const auth = await AuthService.requireAuth(ctx);

    const post = new Post();
    post._id = generate();
    post.title = input.title;
    post.name = slugify(input.title);
    post.createdAt = new Date();
    post.updatedAt = post.createdAt;
    post.publishedAt = post.createdAt ;
    post.content = input.content;
    post.subheading = input.subheading;
    post.excerpt = input.excerpt;
    const imageUrls = getSrcImagesFromHTML(post.content);
    post.authorId = auth.viewerId;
    const category = await PostCategoryLoader(ctx).load(input.categoryId);
    post._categoryId = category._id;
    post.thumbnailSrc = imageUrls[0];
    post.posterSrc = imageUrls[0];
    const counters = new PostCounters();
    counters._id = post._id;
    counters.comments = 0;
    const client = await RepositoryService.connect(ctx);
    const postRepository = client.getRepository(Post);
    await postRepository.save(post);
    const countersRepository = client.getRepository(PostCounters);
    await countersRepository.save(counters);

    const postCategoryCountersRepository = client.getRepository(PostCategoryCounters);
    await postCategoryCountersRepository.increment(
      { _id: post._categoryId },
      'posts',
      1,
    );

    const payload = new PostPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._postId = post._id;

    return payload;
  }

  @Mutation(type => PostPayload)
  async updatePost(@Ctx() ctx: Context, @Arg('id', type => ID) id: string, @Arg('input') input: PostInput) {
    const auth = await AuthService.requireAuth(ctx);
    const post = await PostLoader(ctx).load(id);

    if (auth.viewerId !== post.authorId) {
      throw new Error('Not allowed');
    }

    let updated = false;

    if (post.title !== input.title) {
      post.title = input.title;
      post.name = slugify(input.title);
      updated = true;
    }

    if (post.subheading !== input.subheading) {
      post.subheading = input.subheading;
      updated = true;
    }

    if (post.content !== input.content) {
      post.content = input.content;
      const imageUrls = getSrcImagesFromHTML(post.content);
      if (imageUrls[0] !== post.posterSrc) {
        post.posterSrc = imageUrls[0];
        post.thumbnailSrc = imageUrls[0];
      }
      updated = true;
    }

    if (post.excerpt !== input.excerpt) {
      post.excerpt = input.excerpt;
      updated = true;
    }

    const category = await PostCategoryLoader(ctx).load(input.categoryId);

    const _lastCategoryId = post._categoryId;
    if (post._categoryId !== category._id) {
      delete post.category;
      post._categoryId = category._id;
      updated = true;
    }


    if (updated) {
      post.updatedAt = new Date();
      const client = await RepositoryService.connect(ctx);
      const postRepository = client.getRepository(Post);
      if (_lastCategoryId !== post._categoryId) {
        const postCategoryCountersRepository = client.getRepository(PostCategoryCounters);
        await postCategoryCountersRepository.decrement(
          { _id: _lastCategoryId },
          'posts',
          1,
        );
        await postCategoryCountersRepository.increment(
          { _id: post._categoryId },
          'posts',
          1,
        );
      }
      await postRepository.save(post);
    }

    const payload = new PostPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._postId = post._id;

    return payload;
  }


  @Mutation(type => Payload)
  async removePost(@Ctx() ctx: Context, @Arg('postId') id: string, @Arg('input', { nullable: true }) input?: Input) {
    const auth = await AuthService.requireAuth(ctx);
    const post = await PostLoader(ctx).load(id);

    if (auth.viewerId !== post.authorId) {
      throw new Error('Not allowed');
    }

    const client = await RepositoryService.connect(ctx);
    const postRepository = client.getRepository(Post);
    await postRepository.remove(post);

    const payload = new Payload();
    if (input) {
      payload.clientMutationId = input.clientMutationId;
    }
    return payload;
  }

  @Mutation(type => PostRatePayload)
  async rate(
    @Ctx() ctx: Context,
    @Arg('input') input: PostRateInput,
  ) {
    const auth = await AuthService.requireAuth(ctx);
    const payload = new PostRatePayload();
    const post = await PostLoader(ctx).load(input.postId);
    payload._postId = post._id;
    const _postId = post._id;
    const _viewerType = 'User';
    const _viewerId = auth._userId;
    const id = PostRating.Id.stringify({ _postId, _viewerId, _viewerType });

    const client = await RepositoryService.connect(ctx);
    const postRatingRepository = client.getRepository(PostRating);
    const postCountersRepository = client.getRepository(PostCounters);
    try {
      const postRating = await PostRatingLoader(ctx).load(id);
      if (postRating.ratingScore === input.ratingScore) {
        return payload;
      }
      const prevRatingScore = postRating.ratingScore;
      if (input.ratingScore == null) {
        await postRatingRepository.remove(postRating);
        await postCountersRepository.decrement(
          { _id: _postId },
          `raters`,
          1,
        );
      } else {
        postRating.ratingScore = input.ratingScore;
        postRating.updatedAt = new Date();
        await postRatingRepository.save(postRating);
      }
      await postCountersRepository.decrement(
        { _id: _postId },
        `ratersOnScore${prevRatingScore}`,
        1,
      );
      if (input.ratingScore != null) {
        await postCountersRepository.increment(
          {_id: _postId},
          `ratersOnScore${input.ratingScore}`,
          1,
        );
      }
    } catch (error) {
      if (!(error instanceof EntityNotFoundError)) {
        throw error;
      }
      const postRating = new PostRating();
      postRating._postId = _postId;
      postRating._viewerId = _viewerId;
      postRating._viewerType = _viewerType;
      postRating.ratingScore = input.ratingScore;
      postRating.createdAt = new Date();
      postRating.updatedAt = postRating.createdAt;
      await postRatingRepository.save(postRating);
      await postCountersRepository.increment(
        { _id: _postId },
        `raters`,
        1,
      );
      await postCountersRepository.increment(
        { _id: _postId },
        `ratersOnScore${input.ratingScore}`,
        1,
      );
    }
    return payload;
  }

  @Query(type => Int, { nullable: true })
  async viewerPostRatingScore(
    @Ctx() ctx: Context,
    @Arg('postId', type => ID) postId: string
  ) {
    const auth = await AuthService.getAuth(ctx);
    if (!('viewer' in auth)) {
      return null;
    }
    const post = await PostLoader(ctx).load(postId);
    const _postId = post._id;
    const _viewerType = 'User';
    const _viewerId = auth._userId;
    const id = PostRating.Id.stringify({ _postId, _viewerId, _viewerType });
    try {
      const postRating = await PostRatingLoader(ctx).load(id);
      return postRating.ratingScore;
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        return null;
      }
      throw error;
    }
  }

}