import {
  Query,
  Ctx,
} from 'type-graphql';
import { Context } from '../helpers';
import { Auth } from '../interfaces';
import { AuthService } from '../services';


export class AuthResolver {

  @Query(type => Auth)
  me(@Ctx() ctx: Context) {
    return AuthService.getAuth(ctx);
  }

}