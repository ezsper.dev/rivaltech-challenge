import {
  Resolver,
  Query,
  Ctx,
  Arg,
  Args,
  FieldResolver,
  Root,
  ResolverInterface,
  ID,
} from 'type-graphql';
import { Like } from 'typeorm';
import { Properties, Property, Factory } from '@type-properties/core';
import {
  PageInfo,
  SongArtistAlbumConnection,
  SongArtistAlbumEdge,
  SongArtistAlbum,
  Song,
} from '../types';
import { Context } from '../helpers';
import { SongArtistAlbumLoader, SongArtistLoader, SongLoader } from '../loaders';
import { QuerySongArtistAlbumsArgs } from '../args';
import { RepositoryService } from '../services';

@Properties()
class SongArtistAlbumsCursorProps {

  __typename = Property[0](
    type => String,
    def => 'SongArtistAlbumsCursor',
  );

  title = Property[1](
    type => String,
  );

  _id = Property[2](
    type => String,
  );

}

export class SongArtistAlbumsCursor extends Factory(SongArtistAlbumsCursorProps) {}

@Resolver(of => SongArtistAlbum)
export class SongArtistAlbumResolver implements ResolverInterface<SongArtistAlbum> {

  @FieldResolver()
  artist(@Root() songArtistAlbum: SongArtistAlbum, @Ctx() ctx: Context) {
    return SongArtistLoader(ctx).load(songArtistAlbum.artistId);
  }

  @FieldResolver()
  async songs(@Root() songArtistAlbum: SongArtistAlbum, @Ctx() ctx: Context) {
    const client = await RepositoryService.connect(ctx);
    const songRepository = client.getRepository(Song);
    const statm = await songRepository.createQueryBuilder()
      .select(['_id'])
      .where({ _albumId: songArtistAlbum._id })
      .orderBy('trackOrder', 'ASC');
    const _songIds = await statm.getMany();
    const results = await SongLoader(ctx).loadMany(_songIds.map(({ _id }) => _id));
    return results.map(result => {
      if (result instanceof Error) {
        throw result;
      }
      return result;
    })
  }

  @Query(type => SongArtistAlbum)
  songArtistAlbum(@Ctx() ctx: Context, @Arg('id', type => ID) id: string) {
    return SongArtistAlbumLoader(ctx).load(id);
  }

  @Query(type => SongArtistAlbumConnection)
  async songArtistAlbums(@Ctx() ctx: Context, @Args() args: QuerySongArtistAlbumsArgs) {
    const client = await RepositoryService.connect(ctx);
    const repository = client.getRepository(SongArtistAlbum);
    const statm = repository.createQueryBuilder()
      .select();
    if (args.searchTerm) {
      const searchTerm = args.searchTerm.trim();
      // Not a Full Text Search, but like will do the trick for now
      statm.where([
        { title: Like(`%${searchTerm}%`)},
      ]);
    }

    if (args.after) {
      statm.andWhere(`(title,_id) > (:title, :_id)`, SongArtistAlbumsCursor.decode(args.after));
    } else if (args.before) {
      statm.andWhere(`(title,_id) < (:title, :_id)`, SongArtistAlbumsCursor.decode(args.before));
    }

    statm.take(args.first || args.last || 100);
    statm.orderBy('title', args.last ? 'DESC' : 'ASC')
      .addOrderBy('_id', args.last ? 'DESC' : 'ASC');

    const songArtistAlbums = await statm.getMany();
    const connection = new SongArtistAlbumConnection();
    connection.nodes = songArtistAlbums;
    const info = new PageInfo();
    info.hasPrevPage = false;
    info.hasNextPage = false;
    connection.info = info;
    connection.edges = songArtistAlbums.map(songArtist => {
      const edge = new SongArtistAlbumEdge();
      edge.cursor = SongArtistAlbumsCursor.stringify({ _id: songArtist._id, title: songArtist.title });
      edge.node = songArtist;
      return edge;
    });
    return connection;
  }

}