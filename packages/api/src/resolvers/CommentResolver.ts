import {
  Resolver,
  Query,
  FieldResolver,
  Ctx,
  Root,
  Arg,
  Args,
  Mutation,
  ResolverInterface,
  ID,
} from 'type-graphql';
import { Properties, Property, Factory } from '@type-properties/core';
import { generate } from 'shortid';
import { Context } from '../helpers';
import { Post, PostCounters, Comment, CommentConnection, CommentEdge, PageInfo } from '../types';
import { CommentInput } from '../inputs';
import { QueryCommentsArgs } from '../args';
import { Input, NodeIdentifier, Payload, PostOwner } from '../interfaces';
import { CommentPayload } from '../payloads';
import { PostLoader, CommentLoader, UserLoader } from '../loaders';
import { RepositoryService, AuthService } from '../services';

@Properties()
class CommentsCursorProps {

  __typename = Property[0](
    type => String,
    def => 'CommentsCursor',
  );

  _commentId = Property[1](
    type => String,
  );

}

export class CommentsCursor extends Factory(CommentsCursorProps) {}

@Resolver(of => Comment)
export class CommentResolver implements ResolverInterface<Comment> {

  @FieldResolver(type => PostOwner)
  author(@Root() comment: Comment, @Ctx() ctx: Context) {
    const buf = Buffer.from(comment.authorId, 'base64');
    const { __typename } = NodeIdentifier.decode(buf);
    switch (__typename) {
      case 'User':
        return UserLoader(ctx).load(comment.authorId);
      default:
        throw new Error(`Invalid author id`);
    }
  }

  @FieldResolver(type => Post)
  post(@Root() comment: Comment, @Ctx() ctx: Context) {
    return PostLoader(ctx).load(comment.postId);
  }

  @Query(type => Post)
  comment(@Ctx() ctx: Context, @Arg('commentId') commentId: string) {
    return CommentLoader(ctx).load(commentId);
  }

  @Query(type => CommentConnection)
  async comments(@Ctx() ctx: Context, @Args() args: QueryCommentsArgs) {
    const client = await RepositoryService.connect(ctx);
    const commentRepository = client.getRepository(Comment);
    const { _id: _postId } = Post.Id.decode(args.postId);
    const comments = await commentRepository.find({ where: { _postId }, order: { 'publishedAt': 'DESC' }  });
    const connection = new CommentConnection();
    connection.nodes = comments;
    const info = new PageInfo();
    info.hasPrevPage = false;
    info.hasNextPage = false;
    connection.info = info;
    connection.edges = comments.map(comment => {
      const edge = new CommentEdge();
      edge.cursor = CommentsCursor.stringify({ _commentId: comment._id });
      edge.node = comment;
      return edge;
    });
    return connection;
  }

  @Mutation(type => CommentPayload)
  async createComment(
    @Ctx() ctx: Context,
    @Arg('postId', type => ID) postId: string,
    @Arg('input') input: CommentInput,
  ) {
    const auth = await AuthService.requireAuth(ctx);
    const comment = new Comment();
    comment._id = generate();
    comment.content = input.content;
    comment.authorId = auth.viewerId;
    const { _id: _postId } = Post.Id.decode(postId);
    comment._postId = _postId;
    comment.createdAt = new Date();
    comment.publishedAt = comment.createdAt;
    comment.updatedAt = comment.createdAt;

    const client = await RepositoryService.connect(ctx);
    const commentRepository = client.getRepository(Comment);
    await commentRepository.save(comment);
    const postCountersRepository = client.getRepository(PostCounters);
    await postCountersRepository.increment(
      { _id: _postId },
      'comments',
      1,
    );

    const payload = new CommentPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._commentId = comment._id;

    return payload;
  }

  @Mutation(type => CommentPayload)
  async updateComment(
    @Ctx() ctx: Context,
    @Arg('commentId', type => ID) commentId: string,
    @Arg('input') input: CommentInput,
  ) {
    const auth = await AuthService.requireAuth(ctx);
    const comment = await CommentLoader(ctx).load(commentId);

    if (auth.viewerId !== comment.authorId) {
      throw new Error('Not allowed');
    }

    let updated = false;

    if (comment.content !== input.content) {
      comment.content = input.content;
      updated = true;
    }

    if (updated) {
      comment.updatedAt = new Date();
      const client = await RepositoryService.connect(ctx);
      const commentRepository = client.getRepository(Comment);
      await commentRepository.save(comment);
    }

    const payload = new CommentPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._commentId = comment._id;

    return payload;
  }


  @Mutation(type => Payload)
  async removeComment(
    @Ctx() ctx: Context,
    @Arg('commentId') commentId: string,
    @Arg('input', { nullable: true }) input?: Input,
  ) {
    const auth = await AuthService.requireAuth(ctx);
    const comment = await CommentLoader(ctx).load(commentId);

    if (auth.viewerId !== comment.authorId) {
      throw new Error('Not allowed');
    }

    const client = await RepositoryService.connect(ctx);
    const commentRepository = client.getRepository(Comment);
    await commentRepository.remove(comment);
    const postCountersRepository = client.getRepository(PostCounters);
    await postCountersRepository.decrement(
      { _id: comment._postId },
      'comments',
      1,
    );

    const payload = new Payload();
    if (input) {
      payload.clientMutationId = input.clientMutationId;
    }
    return payload;
  }

}