import {
  Resolver,
  Query,
  Mutation,
  Ctx,
  Arg,
  Args,
  FieldResolver,
  Root,
  ResolverInterface,
  ID,
} from 'type-graphql';
import { ApolloError } from 'apollo-server-errors';
import { Like, In } from 'typeorm';
import shortid from 'shortid';
import { Properties, Property, Factory } from '@type-properties/core';
import {
  PageInfo,
  SongPlaylistConnection,
  SongPlaylistEdge,
  SongPlaylist,
  SongPlaylistItem,
  Song,
} from '../types';
import { Context } from '../helpers';
import { EntityNotFoundError } from '../errors';
import { SongPlaylistLoader, UserLoader, SongPlaylistItemLoader, SongLoader } from '../loaders';
import { QuerySongPlaylistsArgs, QueryMySongPlaylistsArgs } from '../args';
import { AuthService, RepositoryService } from '../services';
import {
  SongPlaylistInput,
  AddSongsToPlaylistInput,
  RemoveSongsFromPlaylistInput,
  ReorderSongsOfPlaylistInput,
} from '../inputs';
import { SongPlaylistPayload } from '../payloads';

@Properties()
class SongPlaylistsCursorProps {

  __typename = Property[0](
    type => String,
    def => 'SongPlaylistsCursor',
  );

  title = Property[1](
    type => String,
  );

  _id = Property[2](
    type => String,
  );

}

export class SongPlaylistsCursor extends Factory(SongPlaylistsCursorProps) {}

@Resolver(of => SongPlaylist)
export class SongPlaylistResolver implements ResolverInterface<SongPlaylist> {

  @FieldResolver()
  author(@Root() songPlaylist: SongPlaylist, @Ctx() ctx: Context) {
    return UserLoader(ctx).load(songPlaylist.authorId);
  }

  @FieldResolver()
  async items(@Root() songPlaylist: SongPlaylist, @Ctx() ctx: Context) {
    const client = await RepositoryService.connect(ctx);
    const repo = client.getRepository(SongPlaylistItem);
    const statm = await repo.createQueryBuilder()
      .select(['_songId'])
      .where({
        _playlistId: songPlaylist._id,
      })
      .addOrderBy('sortOrder', 'ASC')
      .addOrderBy('_songId', 'ASC');
    const result = await statm.getRawMany();
    const nodes = await SongPlaylistItemLoader(ctx).loadMany(
      result.map(({ _songId }) => SongPlaylistItem.Id.stringify({ _songId, _playlistId: songPlaylist._id })),
    );
    return nodes.map(node => {
      if (node instanceof Error) {
        throw node;
      }
      return node;
    });
  }

  @Query(type => SongPlaylist)
  songPlaylist(@Ctx() ctx: Context, @Arg('id', type => ID) id: string) {
    return SongPlaylistLoader(ctx).load(id);
  }

  @Query(type => SongPlaylist)
  songPlaylistByShortId(@Ctx() ctx: Context, @Arg('_id') _id: string) {
    return SongPlaylistLoader(ctx).load(SongPlaylist.Id.stringify({ _id }));
  }

  @Query(type => SongPlaylistConnection)
  async songPlaylists(@Ctx() ctx: Context, @Args() args: QuerySongPlaylistsArgs) {
    const auth = await AuthService.getAuth(ctx);
    const client = await RepositoryService.connect(ctx);
    const songPlaylistRespository = client.getRepository(SongPlaylist);
    const statm = songPlaylistRespository.createQueryBuilder()
      .select();
    if (args.searchTerm) {
      const searchTerm = args.searchTerm.trim();
      // Not a Full Text Search, but like will do the trick for now
      statm.where([
        { title: Like(`%${searchTerm}%`)},
      ]);
    }

    if (args.authorId) {
      statm.andWhere(`authorId = :authorId`, args);
      if ('userId' in auth && auth.userId !== args.authorId) {
        statm.andWhere(`privacy = 'PUBLIC'`);
      }
    } else if ('userId' in auth) {
      statm.andWhere(`privacy = 'PUBLIC' OR (authorId = :authorId)`, { authorId: auth.userId });
    }

    if (args.after) {
      statm.andWhere(`(title,_id) > (:title, :_id)`, SongPlaylistsCursor.decode(args.after));
    } else if (args.before) {
      statm.andWhere(`(title,_id) < (:title, :_id)`, SongPlaylistsCursor.decode(args.before));
    }

    statm.take(args.first || args.last || 100);
    statm.orderBy('title', args.last ? 'DESC' : 'ASC')
      .addOrderBy('_id', args.last ? 'DESC' : 'ASC');

    const songPlaylists = await statm.getMany();
    const connection = new SongPlaylistConnection();
    connection.nodes = songPlaylists;
    const info = new PageInfo();
    info.hasPrevPage = false;
    info.hasNextPage = false;
    connection.info = info;
    connection.edges = songPlaylists.map(songPlaylist => {
      const edge = new SongPlaylistEdge();
      edge.cursor = SongPlaylistsCursor.stringify({
        title: songPlaylist.title,
        _id: songPlaylist._id,
      });
      edge.node = songPlaylist;
      return edge;
    });
    return connection;
  }

  @Query(type => SongPlaylistConnection)
  async mySongPlaylists(@Ctx() ctx: Context, @Args() args: QueryMySongPlaylistsArgs) {
    const { userId } = await AuthService.requireAuth(ctx);
    return this.songPlaylists(ctx, { ...args, authorId: userId });
  }

  @Mutation(type => SongPlaylistPayload)
  async createSongPlaylist(@Ctx() ctx: Context, @Arg('input') input: SongPlaylistInput) {
    const auth = await AuthService.requireAuth(ctx);
    const client = await RepositoryService.connect(ctx);
    const songPlaylistRepository = client.getRepository(SongPlaylist);
    const songPlaylistItemRepository = client.getRepository(SongPlaylistItem);
    const songPlaylist = new SongPlaylist();
    const createdAt = new Date();
    songPlaylist._id = shortid();
    songPlaylist.title = input.title;
    songPlaylist.privacy = input.privacy;
    songPlaylist.createdAt = createdAt;
    songPlaylist.updatedAt = createdAt;
    songPlaylist.authorId = auth.userId;
    await songPlaylistRepository.save(songPlaylist);
    const songs = await SongLoader(ctx).loadMany(input.songs);
    for (const [index, song] of songs.entries()) {
      if (song instanceof Error) {
        throw song;
      }
      const item = new SongPlaylistItem();
      item._playlistId = songPlaylist._id;
      item._songId = song._id;
      item.sortOrder = index;
      item.createdAt = createdAt;
      item.updatedAt = createdAt;
      item.authorId = auth.userId;
      await songPlaylistItemRepository.save(item);
    }
    const payload = new SongPlaylistPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._songPlaylistId = songPlaylist._id;
    return payload;
  }

  @Mutation(type => SongPlaylistPayload)
  async addSongsToPlaylist(@Ctx() ctx: Context, @Arg('input') input: AddSongsToPlaylistInput) {
    const auth = await AuthService.requireAuth(ctx);
    const client = await RepositoryService.connect(ctx);
    const songPlaylist = await SongPlaylistLoader(ctx).load(input.songPlaylist);
    if (auth.userId !== songPlaylist.authorId) {
      throw new ApolloError(`Song ${input.songPlaylist} cannot be edited by you`);
    }

    const songPlaylistRepository = client.getRepository(SongPlaylist);
    const songPlaylistItemRepository = client.getRepository(SongPlaylistItem);
    let songs = await SongLoader(ctx).loadMany(input.songs) as Song[];

    for (const song of songs) {
      if (song instanceof Error) {
        throw song;
      }
    }

    let { maxSortOrder } = await songPlaylistItemRepository.createQueryBuilder()
      .select("COALESCE(MAX(sortOrder), 0)", "maxSortOrder")
      .where({ _playlistId: songPlaylist._id })
      .getRawOne();

    const songItems = await SongPlaylistItemLoader(ctx).loadMany(
      input.songs.map(id => SongPlaylistItem.Id.stringify({
        _songId: Song.Id.decode(id)._id,
        _playlistId: songPlaylist._id,
      })),
    );

    for (const [i, item] of songItems.entries()) {
      if (item instanceof Error) {
        if (item instanceof EntityNotFoundError) {
          continue;
        } else {
          throw item;
        }
      }
      throw new ApolloError(`Song ${input.songs[i]} already include`);
    }

    const updatedAt = new Date();
    await songPlaylistItemRepository.save(
      songs.map(song => {
        const songItem = new SongPlaylistItem();
        songItem.sortOrder = ++maxSortOrder;
        songItem._songId = song._id;
        songItem._playlistId = songPlaylist._id;
        songItem.createdAt = updatedAt;
        songItem.updatedAt = updatedAt;
        songItem.authorId = auth.userId;
        return songItem;
      }),
    );

    await songPlaylistRepository.update({ updatedAt }, { _id: songPlaylist._id });
    SongPlaylistLoader(ctx).clear(songPlaylist.id);
    const payload = new SongPlaylistPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._songPlaylistId = songPlaylist._id;
    return payload;
  }

  @Mutation(type => SongPlaylistPayload)
  async removeSongsFromPlaylist(@Ctx() ctx: Context, @Arg('input') input: RemoveSongsFromPlaylistInput) {
    const auth = await AuthService.requireAuth(ctx);
    const client = await RepositoryService.connect(ctx);
    const songPlaylist = await SongPlaylistLoader(ctx).load(input.songPlaylist);
    if (auth.userId !== songPlaylist.authorId) {
      throw new ApolloError(`Song ${input.songPlaylist} cannot be edited by you`);
    }

    const songPlaylistRepository = client.getRepository(SongPlaylist);
    const songPlaylistItemRepository = client.getRepository(SongPlaylistItem);
    let songs = await SongLoader(ctx).loadMany(input.songs) as Song[];

    for (const song of songs) {
      if (song instanceof Error) {
        throw song;
      }
    }

    const songItems = await SongPlaylistItemLoader(ctx).loadMany(
      input.songs.map(id => SongPlaylistItem.Id.stringify({
        _songId: Song.Id.decode(id)._id,
        _playlistId: songPlaylist._id,
      })),
    ) as SongPlaylistItem[];

    for (const item of songItems) {
      if (item instanceof Error) {
        throw item;
      }
    }

    const updatedAt = new Date();
    await songPlaylistItemRepository.delete({ _songId: In(songItems.map(item => item._songId)) });
    await songPlaylistRepository.update({ updatedAt }, { _id: songPlaylist._id });
    SongPlaylistLoader(ctx).clear(songPlaylist.id);
    const payload = new SongPlaylistPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._songPlaylistId = songPlaylist._id;
    return payload;
  }


  @Mutation(type => SongPlaylistPayload)
  async reorderSongsOfPlaylist(@Ctx() ctx: Context, @Arg('input') input: ReorderSongsOfPlaylistInput) {
    const auth = await AuthService.requireAuth(ctx);
    const client = await RepositoryService.connect(ctx);
    const songPlaylist = await SongPlaylistLoader(ctx).load(input.songPlaylist);
    if (auth.id !== songPlaylist.authorId) {
      throw new ApolloError(`Song ${input.songPlaylist} cannot be edited by you`);
    }

    const songPlaylistRepository = client.getRepository(SongPlaylist);
    const songPlaylistItemRepository = client.getRepository(SongPlaylistItem);

    let songItem = await SongPlaylistItemLoader(ctx).load(input.songItem);

    if (songItem.playlistId !== input.songPlaylist) {
      throw new ApolloError(`The item ${songItem.id} does not belong to specified playlist`);
    }

    const allSongsItems = await songPlaylistItemRepository.find({
      select: ['_songId'],
      where: { _playlistId: songPlaylist._id },
      order: { sortOrder: 'ASC' },
    });


    const sourceIndex = allSongsItems.findIndex(item => item._songId === songItem._songId);
    const newSongItems = allSongsItems.slice();

    if (sourceIndex >= 0) {
      newSongItems.splice(sourceIndex, 1);
    }

    newSongItems.splice(input.sortOrder, 0, songItem);

    await songPlaylistItemRepository.createQueryBuilder()
      .update()
      .set({ sortOrder: () => `sortOrder + 1` })
      .where(`sortOrder >= :sortOrder AND _playlistId = :playlistId`, {
        sortOrder: input.sortOrder,
        _playlistId: songPlaylist._id,
      });

    await songPlaylistItemRepository.update({ sortOrder: input.sortOrder }, { _songId: songItem._songId });
    const updatedAt = new Date();
    await songPlaylistRepository.update({ updatedAt }, { _id: songPlaylist._id });
    SongPlaylistLoader(ctx).clear(songPlaylist.id);
    const payload = new SongPlaylistPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._songPlaylistId = songPlaylist._id;
    return payload;
  }




}