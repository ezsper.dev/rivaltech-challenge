import {
  Resolver,
  FieldResolver,
  Ctx,
  Root,
  ResolverInterface,
} from 'type-graphql';
import { Context } from '../helpers';
import { PostPayload } from '../payloads';
import { PostLoader, PostCountersLoader } from '../loaders';

@Resolver(of => PostPayload)
export class PostPayloadResolver implements ResolverInterface<PostPayload> {

  @FieldResolver()
  post(@Root() payload: PostPayload, @Ctx() ctx: Context) {
    return PostLoader(ctx).load(payload.postId);
  }

  @FieldResolver()
  postCounters(@Root() payload: PostPayload, @Ctx() ctx: Context) {
    return PostCountersLoader(ctx).load(payload.postId);
  }

}