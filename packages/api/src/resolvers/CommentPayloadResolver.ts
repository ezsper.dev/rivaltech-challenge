import {
  Resolver,
  FieldResolver,
  Ctx,
  Root,
  ResolverInterface,
} from 'type-graphql';
import { Context } from '../helpers';
import { CommentPayload } from '../payloads';
import { CommentLoader } from '../loaders';

@Resolver(of => CommentPayload)
export class CommentPayloadResolver implements ResolverInterface<CommentPayload> {

  @FieldResolver()
  comment(@Root() payload: CommentPayload, @Ctx() ctx: Context) {
    return CommentLoader(ctx).load(payload.commentId);
  }

}