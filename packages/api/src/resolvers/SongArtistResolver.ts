import {
  Resolver,
  Query,
  Ctx,
  Arg,
  Args,
  Root,
  ID,
  FieldResolver,
} from 'type-graphql';
import { Like } from 'typeorm';
import { Properties, Property, Factory } from '@type-properties/core';
import {
  PageInfo,
  SongArtistConnection,
  SongArtistEdge,
  SongArtist,
  SongArtistAlbum,
} from '../types';
import { Context } from '../helpers';
import { SongArtistLoader, SongArtistAlbumLoader } from '../loaders';
import { QuerySongArtistsArgs } from '../args';
import { RepositoryService } from '../services';

@Properties()
class SongArtistsCursorProps {

  __typename = Property[0](
    type => String,
    def => 'SongArtistsCursor',
  );

  title = Property[1](
    type => String,
  );

  _id = Property[2](
    type => String,
  );

}

export class SongArtistsCursor extends Factory(SongArtistsCursorProps) {}

@Resolver(of => SongArtist)
export class SongArtistResolver {

  @Query(type => SongArtist)
  artist(@Ctx() ctx: Context, @Arg('id', type => ID) id: string) {
    return SongArtistLoader(ctx).load(id);
  }

  @FieldResolver()
  async albums(@Root() songArtist: SongArtist, @Ctx() ctx: Context) {
    const client = await RepositoryService.connect(ctx);
    const repo = client.getRepository(SongArtistAlbum);
    const result = await repo.find({
      select: ['_id'],
      where: { _artistId: songArtist._id },
      order: { title: 'ASC', _id: 'ASC' },
    });
    const nodes = await SongArtistAlbumLoader(ctx).loadMany(
      result.map(({ _id }) => SongArtistAlbum.Id.stringify({ _id })),
    );
    return nodes.map(node => {
      if (node instanceof Error) {
        throw node;
      }
      return node;
    });
  }

  @Query(type => SongArtistConnection)
  async artists(@Ctx() ctx: Context, @Args() args: QuerySongArtistsArgs) {
    const client = await RepositoryService.connect(ctx);
    const repository = client.getRepository(SongArtist);
    const statm = repository.createQueryBuilder()
      .select();
    if (args.searchTerm) {
      const searchTerm = args.searchTerm.trim();
      // Not a Full Text Search, but like will do the trick for now
      statm.where([
        { title: Like(`%${searchTerm}%`)},
      ]);
    }

    if (args.after) {
      statm.andWhere(`(title,_id) > (:title, :_id)`, SongArtistsCursor.decode(args.after));
    } else if (args.before) {
      statm.andWhere(`(title,_id) < (:title, :_id)`, SongArtistsCursor.decode(args.before));
    }

    statm.take(args.first || args.last || 100);
    statm.orderBy('title', args.last ? 'DESC' : 'ASC')
      .addOrderBy('_id', args.last ? 'DESC' : 'ASC');

    const songArtists = await statm.getMany();
    const connection = new SongArtistConnection();
    connection.nodes = songArtists;
    const info = new PageInfo();
    info.hasPrevPage = false;
    info.hasNextPage = false;
    connection.info = info;
    connection.edges = songArtists.map(songArtist => {
      const edge = new SongArtistEdge();
      edge.cursor = SongArtistsCursor.stringify({ _id: songArtist._id, title: songArtist.title });
      edge.node = songArtist;
      return edge;
    });
    return connection;
  }

}