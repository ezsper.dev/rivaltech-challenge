import {
  Resolver,
  Query,
  Mutation,
  Ctx,
  Arg,
  Args,
  FieldResolver,
  Root,
  ResolverInterface,
  ID,
} from 'type-graphql';
import { ApolloError } from 'apollo-server-errors';
import { Like } from 'typeorm';
import { Properties, Property, Factory } from '@type-properties/core';
import {
  PageInfo,
  SongConnection,
  SongEdge,
  Song,
  SongPlaylist,
  SongPlaylistConnection,
  SongPlaylistItem,
  SongPlaylistEdge
} from '../types';
import { Context } from '../helpers';
import {
  SongLoader,
  SongArtistAlbumLoader,
  SongPlaylistLoader,
  SongArtistLoader,
} from '../loaders';
import { QuerySongsArgs } from '../args';
import { AuthService, RepositoryService } from '../services';
import { SongPayload } from '../payloads';
import { AddPlaylistsToSongInput, RemovePlaylistsFromSongInput } from '../inputs';
import { SongPlaylistResolver } from '../resolvers/SongPlaylistResolver';

@Properties()
class SongsCursorProps {

  __typename = Property[0](
    type => String,
    def => 'SongsCursor',
  );

  title = Property[1](
    type => String,
  );


  _id = Property[2](
    type => String,
  );

}

export class SongsCursor extends Factory(SongsCursorProps) {}

@Resolver(of => Song)
export class SongResolver implements ResolverInterface<Song> {

  songPlaylistResolver = new SongPlaylistResolver();

  @FieldResolver()
  artist(@Root() song: Song, @Ctx() ctx: Context) {
    return SongArtistLoader(ctx).load(song.artistId);
  }

  @FieldResolver()
  album(@Root() song: Song, @Ctx() ctx: Context) {
    if (!song.albumId) {
      return null;
    }
    return SongArtistAlbumLoader(ctx).load(song.albumId);
  }

  @FieldResolver(type => SongPlaylistConnection)
  async myPlaylists(@Root() song: Song, @Ctx() ctx: Context) {
    const auth = await AuthService.requireAuth(ctx);
    const client = await RepositoryService.connect(ctx);
    const songPlaylistItemRespository = client.getRepository(SongPlaylistItem);
    const statm = songPlaylistItemRespository.createQueryBuilder()
      .select(['_playlistId']);
    statm.where({ _songId: song._id, authorId: auth.userId });
    const results = await statm.getRawMany();
    const nodes = await SongPlaylistLoader(ctx).loadMany(
      results.map(({ _playlistId }) => SongPlaylist.Id.stringify({ _id: _playlistId })),
    ).then(nodes => nodes.map(node => {
      if (node instanceof Error) {
        throw node;
      }
      return node;
    }));
    const connection = new SongPlaylistConnection();
    connection.nodes = nodes;
    const info = new PageInfo();
    info.hasPrevPage = false;
    info.hasNextPage = false;
    connection.info = info;
    connection.edges = nodes.map(songPlaylist => {
      const edge = new SongPlaylistEdge();
      edge.cursor = SongsCursor.stringify({
        title: songPlaylist.title,
        _id: songPlaylist._id,
      });
      edge.node = songPlaylist;
      return edge;
    });
    return connection;
  }

  @Query(type => Song)
  song(@Ctx() ctx: Context, @Arg('id', type => ID) id: string) {
    return SongLoader(ctx).load(id);
  }

  @Query(type => SongConnection)
  async songs(@Ctx() ctx: Context, @Args() args: QuerySongsArgs) {
    const client = await RepositoryService.connect(ctx);
    const songRespository = client.getRepository(Song);
    const statm = songRespository.createQueryBuilder()
      .select();
    if (args.searchTerm) {
      const searchTerm = args.searchTerm.trim();
      // Not a Full Text Search, but like will do the trick for now
      statm.where([
        { title: Like(`%${searchTerm}%`)},
      ]);
    }

    if (args.after) {
      statm.andWhere(`(title,_id) > (:title, :_id)`, SongsCursor.decode(args.after));
    } else if (args.before) {
      statm.andWhere(`(title,_id) < (:title, :_id)`, SongsCursor.decode(args.before));
    }

    statm.take(args.first || args.last || 100);
    statm.orderBy('title', args.last ? 'DESC' : 'ASC')
      .addOrderBy('_id', args.last ? 'DESC' : 'ASC');

    const songs = await statm.getMany();
    const connection = new SongConnection();
    connection.nodes = songs;
    const info = new PageInfo();
    info.hasPrevPage = false;
    info.hasNextPage = false;
    connection.info = info;
    connection.edges = songs.map(song => {
      const edge = new SongEdge();
      edge.cursor = SongsCursor.stringify({ _id: song._id, title: song.title });
      edge.node = song;
      return edge;
    });
    return connection;
  }

  @Mutation(type => SongPayload)
  async addPlaylistsToSong(@Ctx() ctx: Context, @Arg('input') input: AddPlaylistsToSongInput) {
    const auth = await AuthService.requireAuth(ctx);
    const song = await SongLoader(ctx).load(input.song);
    let songPlaylists = await SongPlaylistLoader(ctx).loadMany(input.songPlaylists) as SongPlaylist[];

    for (const songPlaylist of songPlaylists) {
      if (songPlaylist instanceof Error) {
        throw songPlaylist;
      }
      if (songPlaylist.authorId !== auth.userId) {
        throw new ApolloError(`Song playlist ${songPlaylist.id} cannot be edited by you`);
      }
    }

    for (const id of input.songPlaylists) {
      await this.songPlaylistResolver.addSongsToPlaylist(ctx, {
        songPlaylist: id,
        songs: [input.song],
      });
    }
    const payload = new SongPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._songId = song._id;
    return payload;
  }

  @Mutation(type => SongPayload)
  async removePlaylistsFromSong(@Ctx() ctx: Context, @Arg('input') input: RemovePlaylistsFromSongInput) {
    const auth = await AuthService.requireAuth(ctx);
    const song = await SongLoader(ctx).load(input.song);
    let songPlaylists = await SongPlaylistLoader(ctx).loadMany(input.songPlaylists) as SongPlaylist[];

    for (const songPlaylist of songPlaylists) {
      if (songPlaylist instanceof Error) {
        throw songPlaylist;
      }
      if (songPlaylist.authorId !== auth.userId) {
        throw new ApolloError(`Song playlist ${songPlaylist.id} cannot be edited by you`);
      }
    }

    for (const id of input.songPlaylists) {
      await this.songPlaylistResolver.removeSongsFromPlaylist(ctx, {
        songPlaylist: id,
        songs: [input.song],
      });
    }
    const payload = new SongPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._songId = song._id;
    return payload;
  }

}