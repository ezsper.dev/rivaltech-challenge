import {
  Ctx,
  Root,
  Resolver,
  FieldResolver,
  ResolverInterface,
} from 'type-graphql';
import { Context } from '../helpers';
import { User } from '../types';

@Resolver(of => User)
export class UserResolver implements ResolverInterface<User> {

  @FieldResolver()
  initials(@Root() user: User, @Ctx() ctx: Context) {
    const initials = user.displayName.match(/\b\w/g) || [];
    return ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
  }

}