import {
  Resolver,
  Query,
  FieldResolver,
  Ctx,
  Root,
  Arg,
  Args,
  Mutation,
  ResolverInterface,
} from 'type-graphql';
import { Properties, Property, Factory } from '@type-properties/core';
import { generate } from 'shortid';
import { Context } from '../helpers';
import slugify from 'slugify';
import { PostCategory, PostCategoryCounters, PostCategoryConnection, PostCategoryEdge, PageInfo } from '../types';
import { PostCategoryInput } from '../inputs';
import { PaginationArgs } from '../args';
import { Input, Payload } from '../interfaces';
import { PostCategoryPayload } from '../payloads';
import { PostCategoryLoader, PostCategoryCountersLoader } from '../loaders';
import { RepositoryService } from '../services';

@Properties()
class PostCategoryCursorProps {

  __typename = Property[0](
    type => String,
    def => 'PostCategoryCursor',
  );

  _categoryId = Property[1](
    type => String,
  );

}

export class PostCategoriesCursor extends Factory(PostCategoryCursorProps) {}

@Resolver(of => PostCategory)
export class PostCategoryResolver implements ResolverInterface<PostCategory> {

  @FieldResolver(type => PostCategoryCounters)
  counters(@Root() post: PostCategory, @Ctx() ctx: Context) {
    return PostCategoryCountersLoader(ctx).load(post.id);
  }

  @Query(type => PostCategory)
  postCategory(@Ctx() ctx: Context, @Arg('postId') postId: string) {
    return PostCategoryLoader(ctx).load(postId);
  }

  @Query(type => PostCategoryConnection)
  async postCategories(@Ctx() ctx: Context, @Args() args: PaginationArgs) {
    const client = await RepositoryService.connect(ctx);
    const postRepository = client.getRepository(PostCategory);
    const categories = await postRepository.find();
    const connection = new PostCategoryConnection();
    connection.nodes = categories;
    const info = new PageInfo();
    info.hasPrevPage = false;
    info.hasNextPage = false;
    connection.info = info;
    connection.edges = categories.map(category => {
      const edge = new PostCategoryEdge();
      edge.cursor = PostCategoriesCursor.stringify({ _categoryId: category._id });
      edge.node = category;
      return edge;
    });
    return connection;
  }

  @Mutation(type => PostCategoryPayload)
  async createPostCategory(@Ctx() ctx: Context, @Arg('input') input: PostCategoryInput) {
    const category = new PostCategory();
    category._id = generate();
    category.title = input.title;
    category.name = slugify(input.title);
    category.description = input.description;
    const counters = new PostCategoryCounters();
    counters._id = category._id;
    counters.posts = 0;
    const client = await RepositoryService.connect(ctx);
    const postRepository = client.getRepository(PostCategory);
    await postRepository.save(category);
    const countersRepository = client.getRepository(PostCategoryCounters);
    await countersRepository.save(counters);

    const payload = new PostCategoryPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._categoryId = category._id;

    return payload;
  }

  @Mutation(type => PostCategoryPayload)
  async updatePostCategory(@Ctx() ctx: Context, @Arg('postId') postId: string, @Arg('input') input: PostCategoryInput) {
    const category = await PostCategoryLoader(ctx).load(postId);

    let updated = false;

    if (category.title !== input.title) {
      category.title = input.title;
      updated = true;
    }

    if (category.description !== input.description) {
      category.description = input.description;
      updated = true;
    }

    if (updated) {
      const client = await RepositoryService.connect(ctx);
      const postRepository = client.getRepository(PostCategory);
      await postRepository.save(category);
    }

    const payload = new PostCategoryPayload();
    payload.clientMutationId = input.clientMutationId;
    payload._categoryId = category._id;

    return payload;
  }


  @Mutation(type => Payload)
  async removePostCategory(@Ctx() ctx: Context, @Arg('postId') postId: string, @Arg('input', { nullable: true }) input?: Input) {
    const post = await PostCategoryLoader(ctx).load(postId);

    const client = await RepositoryService.connect(ctx);
    const postRepository = client.getRepository(PostCategory);
    await postRepository.remove(post);

    const payload = new Payload();
    if (input) {
      payload.clientMutationId = input.clientMutationId;
    }
    return payload;
  }

}