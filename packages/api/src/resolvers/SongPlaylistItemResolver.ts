import {
  Resolver,
  Query,
  Ctx,
  Arg,
  FieldResolver,
  Root,
  ResolverInterface,
  ID,
} from 'type-graphql';
import { SongPlaylist, SongPlaylistItem } from '../types';
import { Context } from '../helpers';
import {SongPlaylistLoader, SongLoader, SongPlaylistItemLoader } from '../loaders';

@Resolver(of => SongPlaylistItem)
export class SongPlaylistItemResolver implements ResolverInterface<SongPlaylistItem> {

  @FieldResolver(type => SongPlaylistItem)
  songPlaylist(@Root() songPlaylistItem: SongPlaylistItem, @Ctx() ctx: Context) {
    return SongPlaylistLoader(ctx).load(songPlaylistItem.playlistId);
  }

  @FieldResolver(type => SongPlaylistItem)
  song(@Root() songPlaylistItem: SongPlaylistItem, @Ctx() ctx: Context) {
    return SongLoader(ctx).load(songPlaylistItem.songId);
  }

  @Query(type => SongPlaylist)
  songPlaylistItem(@Ctx() ctx: Context, @Arg('id', type => ID) id: string) {
    return SongPlaylistItemLoader(ctx).load(id);
  }

}