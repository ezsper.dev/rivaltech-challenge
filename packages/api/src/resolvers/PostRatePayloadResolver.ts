import {
  FieldResolver,
  Ctx,
  Root,
  Resolver,
  ResolverInterface,
} from 'type-graphql';
import { Context } from '../helpers';
import {
  PostLoader,
} from '../loaders';
import { PostRatePayload } from '../payloads';
import { PostResolver } from './PostResolver';

@Resolver(of => PostRatePayload)
export class PostRatePayloadResolver implements ResolverInterface<PostRatePayload> {

  @FieldResolver()
  async ratingScore(@Root() payload: PostRatePayload, @Ctx() ctx: Context) {
    const postResolver = new PostResolver();
    const post = await PostLoader(ctx).load(payload.postId);
    return await postResolver.ratingScore(post, ctx);
  }

}