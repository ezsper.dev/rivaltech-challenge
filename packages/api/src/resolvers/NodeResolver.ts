import {
  Query,
  Ctx,
  Arg,
  ID,
} from 'type-graphql';
import { Context } from '../helpers';
import { Node } from '../interfaces';
import {
  PostLoader,
  PostCountersLoader,
  UserLoader,
  CommentLoader,
} from '../loaders';

export class NodeResolver {

  @Query(type => Node)
  node(@Arg('id', type => ID) id: string, @Ctx() ctx: Context) {
    const { __typename } = Node.Id.decode(id);
    switch (__typename) {
      case 'User':
        return UserLoader(ctx).load(id);
      case 'Post':
        return PostLoader(ctx).load(id);
      case 'PostCounters':
        return PostCountersLoader(ctx).load(id);
      case 'Comment':
        return CommentLoader(ctx).load(id);
      default:
        throw new Error(`Invalid node id`);
    }
  }

}