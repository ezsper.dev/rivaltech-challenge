import {
  Resolver,
  FieldResolver,
  Ctx,
  Root,
  ResolverInterface,
} from 'type-graphql';
import { Context } from '../helpers';
import { AuthUser } from '../types';
import { PostOwner } from '../interfaces';
import { UserLoader } from '../loaders';


@Resolver(of => AuthUser)
export class AuthUserResolver implements ResolverInterface<AuthUser> {

  @FieldResolver(type => PostOwner)
  user(@Root() auth: AuthUser, @Ctx() ctx: Context) {
    return UserLoader(ctx).load(auth.userId);
  }

}