import { createEntityLoader } from '../helpers';
import { SongPlaylistItem } from '../types';

export const SongPlaylistItemLoader = createEntityLoader(SongPlaylistItem);
