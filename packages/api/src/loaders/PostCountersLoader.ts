import { createEntityLoader } from '../helpers';
import { PostCounters } from '../types';

export const PostCountersLoader = createEntityLoader(PostCounters);
