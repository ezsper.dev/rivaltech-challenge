import { createEntityLoader } from '../helpers';
import { PostRating } from '../types';

export const PostRatingLoader = createEntityLoader(PostRating);
