import { createEntityLoader } from '../helpers';
import { Song } from '../types';

export const SongLoader = createEntityLoader(Song);
