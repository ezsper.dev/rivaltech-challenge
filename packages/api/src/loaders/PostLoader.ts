import { createEntityLoader } from '../helpers';
import { Post } from '../types';

export const PostLoader = createEntityLoader(Post);
