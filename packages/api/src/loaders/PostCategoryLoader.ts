import { createEntityLoader } from '../helpers';
import { PostCategory } from '../types';

export const PostCategoryLoader = createEntityLoader(PostCategory);
