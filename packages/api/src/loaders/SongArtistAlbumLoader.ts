import { createEntityLoader } from '../helpers';
import { SongArtistAlbum } from '../types';

export const SongArtistAlbumLoader = createEntityLoader(SongArtistAlbum);
