import { createEntityLoader } from '../helpers';
import { User } from '../types';

export const UserLoader = createEntityLoader(User);
