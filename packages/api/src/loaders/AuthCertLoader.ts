import { createEntityLoader } from '../helpers';
import { AuthCert } from '../types';

export const AuthCertLoader = createEntityLoader(AuthCert);
