import { createEntityLoader } from '../helpers';
import { PostCategoryCounters } from '../types';

export const PostCategoryCountersLoader = createEntityLoader(PostCategoryCounters);
