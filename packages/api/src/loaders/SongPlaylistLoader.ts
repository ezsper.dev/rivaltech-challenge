import { createEntityLoader } from '../helpers';
import { SongPlaylist } from '../types';

export const SongPlaylistLoader = createEntityLoader(SongPlaylist);
