import { createEntityLoader } from '../helpers';
import { Comment } from '../types';

export const CommentLoader = createEntityLoader(Comment);
