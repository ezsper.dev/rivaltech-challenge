import { createEntityLoader } from '../helpers';
import { SongArtist } from '../types';

export const SongArtistLoader = createEntityLoader(SongArtist);
