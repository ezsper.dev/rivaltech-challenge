import { ApolloError } from 'apollo-server-express';

export class InternalServerError extends ApolloError {
  constructor() {
    super(`An internal server error has occurred`, 'INTERNAL_SERVER_ERROR');
  }

}