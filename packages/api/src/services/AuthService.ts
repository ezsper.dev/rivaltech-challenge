import { Properties, Property, Nullable, Factory } from '@type-properties/core';
import jwt from 'jsonwebtoken';
import { memoCtx } from '../helpers';
import { AuthUser, AuthAnonymous } from '../types';
import { UserLoader, AuthCertLoader } from '../loaders';
import { AuthenticationRequiredError, InvalidTokenError } from '../errors';

export namespace AuthService {

  @Properties()
  export class AuthSignedProps {
    issuedAt = Property[0](type => Nullable(Number));
  }

  export class AuthSigned extends Factory(AuthSignedProps) {}

  export const getAuth = memoCtx(ctx => {
    if (ctx.req.user) {
      const auth = new AuthUser();
      auth.user = (ctx.req.user as any);
      auth._userId = auth.user._id;
      auth.userId = auth.user.id;
      return auth;
    }
    return new AuthAnonymous();
  });

  export const requireAuth = memoCtx(async (ctx) => {
    const auth = getAuth(ctx);
    if (auth instanceof AuthAnonymous) {
      throw new AuthenticationRequiredError();
    }
    return auth;
  });
  export const signAuth = memoCtx(async (ctx) => {
    const auth = await requireAuth(ctx);
    const cert = await AuthCertLoader(ctx).load(auth.viewerId);
    const payload = AuthSigned.encode({
      issuedAt: Math.floor(Date.now() / 1000) - 30,
    });
    const token = jwt.sign(payload, {
      key: cert.privateKey,
      passphrase: cert.passphrase,
    }, { algorithm: 'RS256' });
    return `${auth.viewerId}.${token}`;
  });

  export const verifySignature =  memoCtx(async (ctx, signedToken: string) => {
    try {
      const match = signedToken.match(/([^.]+)\.([^.]+\.[^.]+\.[^.]+)/);
      if (!match) {
        throw new Error(`Expecting user first`);
      }
      const [, id, token] = match;
      const cert = await AuthCertLoader(ctx).load(id);
      // const keyObject = { key: cert.publicKey, passphrase: cert.passphrase };
      let payload = jwt.verify(
        token,
        cert.publicKey,
        { algorithms: ['RS256'] },
      );
      if (typeof payload !== 'string') {
        payload = payload.toString();
      }
      const decoded = AuthSigned.decode(Buffer.from(payload, 'utf8'));
      return Object.assign(decoded, { id });
    } catch (originalError) {
      const error = new InvalidTokenError();
      error.originalError = originalError;
      throw error;
    }
  });

  export const loadAuth =  memoCtx(async (ctx, signedToken: string) => {
    const { id } = await verifySignature(ctx, signedToken);
    try {
      ctx.req.user = await UserLoader(ctx).load(id);
    } catch (originalError) {
      const error = new InvalidTokenError();
      error.originalError = originalError;
      throw error;
    }
  });
}

