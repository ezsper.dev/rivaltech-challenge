import { Request } from 'express';
import { generateKeyPairSync } from 'crypto';
import { Passport } from 'passport';
import * as GithubPassport from 'passport-github';
import { generate } from 'shortid';
import { UserLoader } from '../loaders';
import { getContextFromRequest } from '../helpers';
import { RepositoryService } from './RepositoryService';
import { User, AuthCert } from '../types';

export namespace PassportService {
  // Yeah I know theses credentials shouldn't be implicitly here, but this is a challenge :)
  const GITHUB_CLIENT_ID = 'ae03aabbb207554034fc';
  const GITHUB_CLIENT_SECRET = '4d6fa535de32381b69a57259f6ad89909e52ceb1';

  export const authenticator = new Passport();

  const githubStrategy = new GithubPassport.Strategy(
    {
      clientID: GITHUB_CLIENT_ID,
      clientSecret: GITHUB_CLIENT_SECRET,
      passReqToCallback: true,
      callbackURL: 'http://localhost:7080/auth/github/callback',
    },
    (req, accessToken, refreshToken, profile, done) => {
      const ctx = getContextFromRequest(req);
      RepositoryService.connect(ctx).then(async client => {
        const userRepository = client.getRepository(User);
        let user = await userRepository.findOne({
          where: {_githubId: profile.id},
        });

        if (!user) {
          user = new User();
          user._id = generate();
          user.displayName = profile.displayName;
          user._githubId = profile.id;
          user.avatarSrc = (profile._json as any).avatar_url;
          await userRepository.save(user);

          const authCertRepository = client.getRepository(AuthCert);
          const cert = new AuthCert();
          cert.__typename = 'User';
          cert._id = user._id;
          cert.passphrase = 'my unique';
          const { publicKey, privateKey } = generateKeyPairSync('rsa', {
            modulusLength: 512,
            publicKeyEncoding: {
              type: "spki",
              format: "pem"
            },
            privateKeyEncoding: {
              type: "pkcs8",
              format: "pem",
              cipher: "aes-256-cbc",
              passphrase: cert.passphrase,
            }
          });
          cert.algorithm = 'rsa';
          cert.publicKey = publicKey;
          cert.privateKey = privateKey;
          await authCertRepository.save(cert);
        }

        done(null, user);
      });
    },
  );

  authenticator.use(githubStrategy);

  authenticator.serializeUser(function (user: User, done) {
    done(null, user.id);
  });

  authenticator.deserializeUser<User, string, Request>(function (req, id, done) {
    const ctx = getContextFromRequest(req);
    UserLoader(ctx).load(id)
      .then(user => done(null, user))
      .catch(error => done(error));
  });
}