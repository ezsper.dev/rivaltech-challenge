import { resolve as resolvePath } from 'path';
import { createConnection, Connection } from 'typeorm';
import { memoCtx } from '../helpers';

export namespace RepositoryService {

  let connection: Promise<Connection> | undefined;

  export const create = () => {
    connection = createConnection({
      type: 'sqlite',
      name: 'file',
      database: resolvePath(__dirname, '../../.sqlite'),
      entities: [resolvePath(__dirname, '../types/*.ts')],
      synchronize: true,
    });
    return connection;
  };

  export const connect = memoCtx(() => {
    if (connection) {
      return connection;
    }
    return create();
  });

}