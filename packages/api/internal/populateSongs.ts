import 'reflect-metadata';
import fs from 'fs';
import { resolve as resolvePath } from 'path';
import Hashids from 'hashids/cjs';
import shortid from 'shortid';
import { EntityNotFoundError } from '../src/errors';
import { RepositoryService } from '../src/services';
import { SongLoader, SongArtistLoader, SongArtistAlbumLoader } from '../src/loaders';
import { Song, SongArtist, SongArtistAlbum } from '../src/types';
import { mockContext, Context } from '../src/helpers';

interface LibraryDataItem {
  album: string;
  duration: number;
  title: string;
  id: number;
  artist: string;
}

const songArtistCache: { [key: string]: SongArtist | null } = {};

async function findSongArtist(ctx: Context, title: string) {
  const cacheKey = title;
  if (songArtistCache[cacheKey] !== undefined) {
    return songArtistCache[cacheKey] ;
  }
  const client = await RepositoryService.connect(ctx);
  const songArtistRepository = client.getRepository(SongArtist);
  const result = await songArtistRepository.findOne({
    select: ['_id'],
    where: { title },
  });
  return songArtistCache[cacheKey] = result
    ? await SongArtistLoader(ctx).load(SongArtist.Id.stringify({ _id: result._id }))
    : null;
}


const songArtistAlbumCache: { [key: string]: SongArtistAlbum | null } = {};

async function findSongArtistAlbum(ctx: Context, _artistId: string, title: string) {
  const cacheKey = title;
  if (songArtistAlbumCache[cacheKey] !== undefined) {
    return songArtistAlbumCache[cacheKey] ;
  }
  const client = await RepositoryService.connect(ctx);
  const songArtistAlbumRepository = client.getRepository(SongArtistAlbum);
  const result = await songArtistAlbumRepository.findOne({
    select: ['_id'],
    where: { title, _artistId },
  });

  return songArtistAlbumCache[cacheKey] = result
    ? await SongArtistAlbumLoader(ctx).load(SongArtistAlbum.Id.stringify({ _id: result._id }))
    : null;
}

async function createSong(
  ctx: Context,
  _id: string,
  _artistId: string,
  _albumId: string,
  title: string,
  duration: number,
) {
  const client = await RepositoryService.connect(ctx);
  const songRepository = client.getRepository(Song);
  const song = new Song();
  song._id = _id;
  song._artistId = _artistId;
  song._albumId = _albumId;
  song.title = title;
  song.duration = duration;
  song.sourceUrl = '';
  song.albumTrack = 0;
  await songRepository.save(song);
  return song;
}

async function createSongArtist(ctx: Context, _id: string, title: string) {
  const client = await RepositoryService.connect(ctx);
  const songArtistRepository = client.getRepository(SongArtist);
  const songArtist = new SongArtist();
  songArtist._id = _id;
  songArtist.title = title;
  songArtist.bio = '';
  await songArtistRepository.save(songArtist);
  return songArtist;
}

async function createSongArtistAlbum(ctx: Context, _id: string, _artistId: string, title: string) {
  const client = await RepositoryService.connect(ctx);
  const songArtistAlbumRepository = client.getRepository(SongArtistAlbum);
  const songArtistAlbum = new SongArtistAlbum();
  songArtistAlbum._id = _id;
  songArtistAlbum._artistId = _artistId;
  songArtistAlbum.title = title;
  songArtistAlbum.bio = '';
  await songArtistAlbumRepository.save(songArtistAlbum);
  return songArtistAlbum;
}


async function populateSongs() {
  const ctx = mockContext();
  const libraryData = JSON.parse(
    fs.readFileSync(resolvePath(__dirname, '../data/library.json')).toString(),
  ) as LibraryDataItem[];
  const hashids = new Hashids();
  for (const data of libraryData) {
    const _songId = hashids.encode(data.id);
    try {
      await SongLoader(ctx).load(Song.Id.stringify({ _id: _songId }));
    } catch (error) {
      if (!(error instanceof EntityNotFoundError)) {
        throw error;
      }
      let songArtist = await findSongArtist(ctx, data.artist);
      if (!songArtist) {
        songArtist = await createSongArtist(ctx, shortid(), data.artist);
      }
      let songArtistAlbum = await findSongArtistAlbum(ctx, songArtist._id, data.album);
      if (!songArtistAlbum) {
        songArtistAlbum = await createSongArtistAlbum(ctx, shortid(), songArtist._id, data.album);
      }
      await createSong(ctx, _songId, songArtist._id, songArtistAlbum._id, data.title, data.duration);
    }
  }
}

console.info(`Starting song population`);
populateSongs()
  .then(() => {
    console.info(`Populated songs`);
  })
  .catch(error => {
    console.error(`Something went wrong :(`);
    console.error(error);
  });