import React from 'react';

declare global {
  declare namespace JSX {
    interface IntrinsicElements {
      descr: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
    }
  }
}