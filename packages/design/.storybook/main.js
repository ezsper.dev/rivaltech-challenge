const path = require('path');

module.exports = {
  stories: [
    '../src/**/*.@(story|stories).@([tj]sx?|mdx)',
  ],
  addons: [
    {
      name: '@storybook/preset-create-react-app',
      options: {
        tsDocgenLoaderOptions: {
          tsconfigPath: path.resolve(__dirname, '../tsconfig.json'),
        },
      },
    },
    {
      name: '@storybook/addon-docs',
      options: {
        configureJSX: true,
      },
    },
    'storybook-addon-intl',
  ],
};