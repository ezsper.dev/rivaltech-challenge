import React from 'react';
import { addDecorator } from '@storybook/react';
import { setIntlConfig, withIntl } from 'storybook-addon-intl';
import { IntlProvider } from 'react-intl';
import CssBaseline from '@material-ui/core/CssBaseline';
import { theme } from '../src/module/styles/themes/default';
import { ThemeProvider } from '../src/module/styles/ThemeProvider';

addDecorator(storyFn => (
  <>
    <CssBaseline />
    <ThemeProvider theme={theme}>
      {storyFn()}
    </ThemeProvider>
  </>
));

const messages = {
  'en': { 'button.label': 'Click me!' },
  'ptBR': { 'button.label': 'Klick mich!' }
};

// Provide your formats (optional)
const formats = {
  'en': {
    'date': {
      'year-only': {
        'year': '2-digit',
      },
    },
  },
  'PtBR': {
    'date': {
      'year-only': {
        'year': 'numeric',
      },
    },
  },
};

const getMessages = (locale) => messages[locale];
const getFormats = (locale) => formats[locale];

// Set intl configuration
setIntlConfig({
  locales: ['en', 'de'],
  defaultLocale: 'en',
  getMessages,
  getFormats,
});

// Register decorator
addDecorator(withIntl);