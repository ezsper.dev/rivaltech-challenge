import React from 'react';
import Avatar, { AvatarProps } from '@material-ui/core/Avatar';
import { PostOwner } from '../../data/typings';
import { defineMessages, useIntl } from 'react-intl';

const messagePrefix = '@dxco/design/PostOwnerAvatar';

const messages = defineMessages({
  ariaLabel: {
    id: `${messagePrefix}/ariaLabel`,
    description: 'Message for aria label',
    defaultMessage: `user picture`,
  },
});

export interface PostOwnerAvatarProps extends Omit<AvatarProps, 'avatarSrc' | 'children'> {
  owner: Pick<PostOwner, 'avatarSrc' | 'initials' | 'displayName'>;
}

/**
 * Display the avatar for a post owner
 * @param props
 * @constructor
 */
export function PostOwnerAvatar(props: PostOwnerAvatarProps) {
  const { owner } = props;
  const { avatarSrc, initials, displayName } = owner;
  const intl = useIntl();
  return (
    <Avatar
      aria-label={intl.formatMessage(messages.ariaLabel, { displayName })}
      {...avatarSrc
        ? { src: avatarSrc }
        : { children: initials }
      }
    />
  );
}

export default PostOwnerAvatar;