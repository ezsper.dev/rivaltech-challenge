/// <reference types="@types/jest" />
import React, { ReactNode } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import PostOwnerAvatar from './PostOwnerAvatar';
import DefaultThemeProvider from '../../styles/DefaultThemeProvider';
import { userMocks } from '../../data/mockups';

const useMock = userMocks.eq(0);

const withContext = (component: ReactNode) => (
  <DefaultThemeProvider>
    <IntlProvider locale="en">
      {component}
    </IntlProvider>
  </DefaultThemeProvider>
);

describe('default', () => {
  test('renders correctly', () => {
    const { asFragment } = render(withContext(
      <PostOwnerAvatar owner={useMock} />,
    ));
    expect(asFragment()).toMatchSnapshot();
  });
});