import React from 'react';
import CardHeader, { CardHeaderProps } from '@material-ui/core/CardHeader';
import PostOwnerAvatar from '../PostOwnerAvatar';
import { PostOwner } from '../../data/typings';

export interface PostOwnerCardHeaderProps extends Omit<CardHeaderProps, 'avatar' | 'title'> {
  owner: Pick<PostOwner, 'avatarSrc' | 'initials' | 'displayName'>;
}

export function PostOwnerCardHeader(props: PostOwnerCardHeaderProps) {
  const { owner, ...rest } = props;
  return (
    <CardHeader
      {...rest}
      avatar={<PostOwnerAvatar owner={owner} />}
      title={owner.displayName}
    />
  );
}

export default PostOwnerCardHeader;