/// <reference types="@types/jest" />
import React, { ReactNode } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import PostOwnerCardHeader from './PostOwnerCardHeader';
import DefaultThemeProvider from '../../styles/DefaultThemeProvider';
import { userMocks } from '../../data/mockups';

const userMock = userMocks.eq(0);

const withContext = (component: ReactNode) => (
  <DefaultThemeProvider>
    <IntlProvider locale="en">
      {component}
    </IntlProvider>
  </DefaultThemeProvider>
);

describe('default', () => {
  test('renders correctly', () => {
    const { asFragment } = render(withContext(
      <PostOwnerCardHeader owner={userMock} />,
    ));
    expect(asFragment()).toMatchSnapshot();
  });
});