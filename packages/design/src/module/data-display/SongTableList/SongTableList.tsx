import React, { useCallback } from 'react';
import { createStyles, makeStyles, useTheme, Theme } from '../../styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Zoom from '@material-ui/core/Zoom';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { ControlPlayPauseButton } from '../../buttons/ControlPlayPauseButton';
import { Song, SongPlaylistItem, SongArtist, SongArtistAlbum } from '../../data/typings';

function reorder<T>(list: T[], startIndex: number, endIndex: number): T[] {
  const newArray = list.slice();
  const [removed] = newArray.splice(startIndex, 1);
  newArray.splice(endIndex, 0, removed);
  return newArray;
}

interface Data {
  title: string;
  artist: string;
  album: string;
  addedAt: string;
}

function createData(
  title: string,
  artist: string,
  album: string,
  addedAt: string,
): Data {
  return { title, artist, album, addedAt };
}

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key,
): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
}

const headCells: HeadCell[] = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Title' },
  { id: 'artist', numeric: false, disablePadding: false, label: 'Artist' },
  { id: 'addedAt', numeric: false, disablePadding: false, label: 'Added At' },
];

interface EnhancedTableProps {
  editable?: boolean;
  sortable?: boolean;
  classes: ReturnType<typeof useStyles>;
  numSelected: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string | null;
  rowCount: number;
}


function EnhancedTableHead(props: EnhancedTableProps) {
  const {
    classes,
    editable = false,
    sortable = true,
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
  } = props;
  const theme = useTheme();
  const createSortHandler = (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
    onRequestSort(event, property);
  };

  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen,
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell className={classes.checkboxCell} {...editable && { padding: 'checkbox' }}>
          <Zoom
            in={editable}
            timeout={transitionDuration}
          >
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{ 'aria-label': 'select all desserts' }}
            />
          </Zoom>
        </TableCell>
        <TableCell className={classes.playButtonCell} />
        {headCells.map((headCell, i) => (
          <TableCell
            key={headCell.id}
            align={i === headCells.length - 1 ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            {...sortable && {
              sortDirection: orderBy === headCell.id ? order : false,
            }}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
    playButtonCell: (props: SongTableListProps<any>) => ({
      width: props.editable ? 44 : 58,
      padding: `0px 0 0 ${props.editable ? 0 : 15}px`,
      transition: theme.transitions.create('all'),
    }),
    checkboxCell: {
      padding: props =>  props.editable ? undefined : '0',
      width: props =>  props.editable ? 48 : '0',
      position: 'relative',
      transition: theme.transitions.create('all'),
      '& > *': {
        position: 'absolute',
        top: 5,
        left: 0,
      },
    },
  }),
);

type SongPlaylistItemShape = Pick<SongPlaylistItem, 'createdAt'> & {
  song: Pick<Song, 'id' | 'title'> & {
    artist: Pick<SongArtist, 'id' | 'title'>;
    album: Pick<SongArtistAlbum, 'id' | 'title'>;
  };
};

export interface SongTableListProps<T extends SongPlaylistItemShape> {
  editable?: boolean;
  sortable?: boolean;
  defaultSelected?: any[];
  selected?: any[];
  onSelection?: (selected: any[]) => void;
  paginate?: boolean;
  items: T[];
  onReorder?: (items: T[]) => void;
}

export function SongTableList<T extends SongPlaylistItemShape>(props: SongTableListProps<T>) {
  const {
    defaultSelected,
    selected: selectedProp,
    editable = false,
    sortable = true,
    onSelection,
    paginate = false,
    items,
    onReorder,
  } = props;
  const classes = useStyles(props);
  const theme = useTheme();
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof Data | null>(null);
  const [selectedState, setSelectedState] = React.useState<string[]>(selectedProp || defaultSelected || []);
  const selected = selectedProp || selectedState;
  const [page] = React.useState(0);
  const [rowsPerPage] = React.useState(5);

  const rows = items.map(item => createData(
    item.song.title,
    item.song.artist.title,
    item.song.album.title,
    item.createdAt,
  ));

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof Data) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    if (!editable) {
      return;
    }
    if (event.target.checked) {
      const newSelecteds = items.map(item => item.song.id);
      if (!selectedProp) {
        setSelectedState(newSelecteds);
      }
      if (onSelection) {
        onSelection(newSelecteds);
      }
      return;
    }
    const emptySelected: any[] = [];
    if (!selectedProp) {
      setSelectedState(emptySelected);
    }
    if (onSelection) {
      onSelection(emptySelected);
    }
  }, [editable, items, selectedProp, onSelection]);

  const handleClick = useCallback((event: React.MouseEvent<unknown>, id: string) => {
    if (!editable) {
      return;
    }
    const selectedIndex = selected.indexOf(id);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    if (!selectedProp) {
      setSelectedState(newSelected);
    }
    if (onSelection) {
      onSelection(newSelected);
    }
  }, [editable, selected, selectedProp, onSelection]);

  const isSelected = (name: string) => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, items.length - page * rowsPerPage);

  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen,
  };

  const handleDragEnd = useCallback((result: any) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    if (onReorder) {
      onReorder(reorder(
        items,
        result.source.index,
        result.destination.index
      ));
    }
  }, [onReorder, items]);

  let displayRows = !sortable || orderBy == null? rows : stableSort(rows, getComparator(order, orderBy));
  if (paginate) {
    displayRows = displayRows
      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);
  }

  return (
    <div className={classes.paper}>
      <TableContainer>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          size="medium"
          aria-label="enhanced table"
        >
          <EnhancedTableHead
            editable={editable}
            classes={classes}
            numSelected={selected.length}
            order={order}
            orderBy={orderBy}
            onSelectAllClick={handleSelectAllClick}
            onRequestSort={handleRequestSort}
            rowCount={rows.length}
          />
          <DragDropContext onDragEnd={handleDragEnd}>
            <Droppable
              droppableId="table"
            >
              {(provided) => (
                <>
                  <TableBody
                    innerRef={provided.innerRef}
                    {...provided.droppableProps}>
                    {displayRows
                      .map((row, index) => {
                        const { song } = items[index];
                        const isItemSelected = isSelected(song.id);
                        const labelId = `enhanced-table-checkbox-${index}`;

                        return (
                          <Draggable
                            draggableId={song.id}
                            index={index}
                            key={song.id}
                          >
                            {(provided) => (
                              <TableRow
                                hover
                                onClick={(event) => handleClick(event, song.id)}
                                role="checkbox"
                                aria-checked={isItemSelected}
                                tabIndex={-1}
                                key={song.id}
                                selected={isItemSelected}
                                innerRef={provided.innerRef}
                                {...editable && provided.draggableProps}
                                {...editable && provided.dragHandleProps}
                              >
                                <TableCell
                                  className={classes.checkboxCell}
                                  {...editable && { padding: 'checkbox' }}>
                                  <Zoom
                                    in={editable}
                                    timeout={transitionDuration}
                                  >
                                    <Checkbox
                                      checked={isItemSelected}
                                      inputProps={{ 'aria-labelledby': labelId }}
                                    />
                                  </Zoom>
                                </TableCell>
                                <TableCell className={classes.playButtonCell}>
                                  <ControlPlayPauseButton playingPulse onClick={event => event.stopPropagation()} />
                                </TableCell>
                                <TableCell component="th" id={labelId} scope="row" padding="none">
                                  {row.title}
                                </TableCell>
                                <TableCell>{row.artist}</TableCell>
                                <TableCell align="right">{row.addedAt}</TableCell>
                              </TableRow>
                            )}
                          </Draggable>
                        );
                      })}
                    {paginate && emptyRows > 0 && (
                      <TableRow style={{ height: 53 * emptyRows }}>
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                  </TableBody>
                  {provided.placeholder}
                </>
              )}
            </Droppable>
          </DragDropContext>
        </Table>
      </TableContainer>
    </div>
  );
}