import React, { HTMLProps } from 'react';
import clsx from 'clsx';
import { FormattedDate, FormattedMessage } from 'react-intl';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import CardHeader from '@material-ui/core/CardHeader';
import Rating from '@material-ui/lab/Rating';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '../../styles';
import { Post, PostCategory, PostOwner, PostCounters } from '../../data/typings';

type PostType = Pick<Post, 'title' | 'subheading' | 'content' | 'excerpt' | 'publishedAt' | 'ratingScore'>
  & { category: Pick<PostCategory, 'id' | 'title'> }
  & { author: Pick<PostOwner, 'displayName' | 'avatarSrc' | 'initials'> }
  & { counters: Pick<PostCounters, 'comments'> };

export interface ArticleBodyProps extends HTMLProps<HTMLDivElement>{
  post: PostType;
}

export const useStyles = makeStyles(theme => ({
  /* Styles applied to the root element. */
  root: {
    flexGrow: 1,
  },
  content: {
    marginTop: theme.spacing(10),
    marginBottom: theme.spacing(5),
    '& img': {
      maxWidth: '100%',
    },
  },
  trendings: {
    marginTop: theme.spacing(2),
  },
}));

export const ArticleBody = (props: ArticleBodyProps) => {
  const { post, className, ...rest } = props;
  const { author } = post;
  const classes = useStyles();

  return (
    <article {...rest} className={clsx(classes.root, className)}>
      <Container maxWidth="md">
        <Typography gutterBottom variant="subtitle1" component="h6">
          {post.category.title}
        </Typography>
        <Typography component="h1" variant="h2">
            {post.title}
        </Typography>
        {post.subheading && (
          <Typography component="h2" variant="h5" gutterBottom>
            {post.subheading}
          </Typography>
        )}
        <Grid container>
          <Grid item xs={9} sm={6}>
            <CardHeader
              avatar={
                <Avatar
                  aria-label="user avatar"
                  {...author.avatarSrc
                    ? { src: author.avatarSrc }
                    : { children: author.initials }
                  }
                />
              }
              title={author.displayName}
              subheader={(
                <FormattedDate year="numeric" month="long" day="2-digit"  value={post.publishedAt} />
              )}
            />
          </Grid>
          <Grid item xs={3} sm={6}>
            <Box display="flex" alignItems="center" justifyContent="flex-end">
              <Typography variant="caption" component="div" color="textSecondary" className={classes.trendings}>
                {(
                  <>
                    <Box textAlign="right">
                      <FormattedMessage
                        id="comments"
                        defaultMessage={`{count, number} {count, plural,
                          one {Comment}
                          other {Comments}
                        }`}
                        values={{ count: post.counters.comments }}
                      />
                    </Box>
                    <Rating
                      size="small"
                      value={post.ratingScore/2}
                      precision={0.5}
                      readOnly
                    />
                  </>
                )}
              </Typography>
            </Box>
          </Grid>
        </Grid>
        <Typography
        component="div"
        variant="body1"
        className={classes.content}
        dangerouslySetInnerHTML={{__html: post.content }}
        />
      </Container>
    </article>
  );
};

export default ArticleBody;