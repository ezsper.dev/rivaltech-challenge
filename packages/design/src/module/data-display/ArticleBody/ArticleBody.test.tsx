/// <reference types="@types/jest" />
import React, { ReactNode } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import ArticleBody from './ArticleBody';
import DefaultThemeProvider from '../../styles/DefaultThemeProvider';
import { postMocks } from '../../data/mockups';

const postMock = postMocks.eq(0);

const withContext = (component: ReactNode) => (
  <DefaultThemeProvider>
    <IntlProvider locale="en">
      {component}
    </IntlProvider>
  </DefaultThemeProvider>
);

describe('default', () => {
  test('renders correctly', () => {
    const { asFragment } = render(withContext(
      <ArticleBody post={postMock} />,
    ));
    expect(asFragment()).toMatchSnapshot();
  });
});