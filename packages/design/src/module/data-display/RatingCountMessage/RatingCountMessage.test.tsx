/// <reference types="@types/jest" />
import React, { ReactNode } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import RatingCountMessage from './RatingCountMessage';
import DefaultThemeProvider from '../../styles/DefaultThemeProvider';

const withContext = (component: ReactNode) => (
  <DefaultThemeProvider>
    <IntlProvider locale="en">
      {component}
    </IntlProvider>
  </DefaultThemeProvider>
);

describe('default', () => {
  test('renders correctly', () => {
    const { asFragment } = render(withContext(
      <RatingCountMessage value={8} />,
    ));
    expect(asFragment()).toMatchSnapshot();
  });
});