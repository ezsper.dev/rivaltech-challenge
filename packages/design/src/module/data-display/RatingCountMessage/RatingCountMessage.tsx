import React, { createElement, ElementType } from 'react';
import { FormattedMessage, defineMessages } from 'react-intl';
import { OverrideProps, OverridableComponent } from '../../helpers/OverridableComponent';

const messagePrefix = '@dxco/design/RatingCountMessage';

const messages = defineMessages({
  ratings: {
    id: `${messagePrefix}/ratings`,
    description: 'Message to showing ratings count.',
    defaultMessage: `{value, number} {value, plural,
                      one {rating}
                      other {ratings}
                    }`,
  },
});

export interface RatingCountMessageTypeMap<P = {}, D extends ElementType = 'span'> {
  props: P & {
    value: number;
  };
  defaultComponent: D;
  classKey: string;
}


export type RatingCountMessageProps<
  D extends React.ElementType = RatingCountMessageTypeMap['defaultComponent'],
  P = {}
  > = OverrideProps<RatingCountMessageTypeMap<P, D>, D>;

export const RatingCountMessage = function CommentCount<D extends React.ElementType = RatingCountMessageTypeMap['defaultComponent'],
  P = {}>(props: RatingCountMessageProps<D, P>) {
  const {
    component,
    value,
    ...rest
  } = props;
  const children = (
    <FormattedMessage {...messages.ratings} values={{ value }} />
  );
  return createElement(component || 'span', rest, children);
} as OverridableComponent<RatingCountMessageTypeMap>;

export default RatingCountMessage;