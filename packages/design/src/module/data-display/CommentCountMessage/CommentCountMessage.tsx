import React, { createElement, ElementType } from 'react';
import { FormattedMessage, defineMessages } from 'react-intl';
import { OverrideProps, OverridableComponent } from '../../helpers/OverridableComponent';

const messagePrefix = '@dxco/design/RatingCountMessage';

export const messages = defineMessages({
  comments: {
    id: `${messagePrefix}/comments`,
    description: 'Message to showing comment count.',
    defaultMessage: `{value, number} {value, plural,
                      one {comment}
                      other {comments}
                  }`,
  },
});

export interface CommentCountMessageTypeMap<P = {}, D extends ElementType = 'span'> {
  props: P & {
    value: number;
  };
  defaultComponent: D;
  classKey: string;
}


export type CommentCountMessageProps<
  D extends React.ElementType = CommentCountMessageTypeMap['defaultComponent'],
  P = {}
  > = OverrideProps<CommentCountMessageTypeMap<P, D>, D>;

export const CommentCountMessage = function CommentCount<D extends React.ElementType = CommentCountMessageTypeMap['defaultComponent'],
  P = {}>(props: CommentCountMessageProps<D, P>) {
  const {
    component,
    value,
    ...rest
  } = props;
  const children = (
    <FormattedMessage {...messages.comments} values={{ value }} />
  );
  return createElement(component || 'span', rest, children);
} as OverridableComponent<CommentCountMessageTypeMap>;

export default CommentCountMessage;