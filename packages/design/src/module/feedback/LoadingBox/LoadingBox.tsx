import React, { useState, ReactNode, useEffect } from 'react';
import warning from 'warning';
import clsx from 'clsx';
import Box, { BoxProps } from '@material-ui/core/Box';
import Fade from '@material-ui/core/Fade';
import Zoom from '@material-ui/core/Zoom';
import CircularProgress, { CircularProgressProps } from '@material-ui/core/CircularProgress';
import { makeStyles, createStyles, useTheme } from '../../styles';

export interface LoadingBoxProps extends BoxProps {
  children?: ReactNode;
  minHeight?: number | string;
  minWidth?: number | string;
  whileLoading?: ReactNode;
  loading?: boolean;
  duration?: number;
  ProgressProps?: CircularProgressProps;
}

const useStyles = makeStyles(theme => createStyles({
  root: {
    position: 'relative',
  },
  progressContainer: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'relative',
    padding: theme.spacing(3),
    transition: theme.transitions.create('all'),
  },
  progress: {
    marginRight: theme.spacing(1),
  },
}));

export function LoadingBox(props: LoadingBoxProps) {
  const {
    children,
    className,
    minHeight,
    minWidth,
    whileLoading,
    ProgressProps,
    duration,
    loading: loadingProp,
    ...rest
  } = props;

  warning(!(duration != null && loadingProp != null),
    `You cannot use "duration" and "loading" props together`,
  );

  const classes = useStyles();
  const [loadingState, setLoadingState] = useState(true);
  const loading = loadingProp != null ? loadingProp : loadingState;
  const theme = useTheme();

  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen,
  };

  useEffect(() => {
    let timer: any;
    if (duration != null) {
      timer = setTimeout(() => setLoadingState(false), duration);
    }
    return () => {
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, [duration]);

  const loader = (
    <Box
      className={classes.progressContainer}
      style={{ minWidth, minHeight }}
      display="flex"
      alignItems="center"
      justifyContent="center">
      <Zoom timeout={transitionDuration} in={loading}>
        <CircularProgress className={classes.progress} size={20} {...ProgressProps} />
      </Zoom>
      {whileLoading}
    </Box>
  );

  const done = (
    <div>
      {children}
    </div>
  );

  return (
    <Box className={clsx(classes.root, className)} {...rest}>
      {[
        loader,
        done,
      ].map((content: any, i) => {
        const transitionIn = i === 0 ? loading : !loading;
        return (
          <Fade
            key={`${i}`}
            in={transitionIn}
            timeout={transitionDuration}
            style={{
              ...(!transitionIn && {
                position: 'absolute',
              }),
              transitionDelay: `${transitionIn ? transitionDuration.exit : 0}ms`,
            }}
            unmountOnExit
          >
            {content}
          </Fade>
        );
      })}
    </Box>
  );
}

export default LoadingBox;