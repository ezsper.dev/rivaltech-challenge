import { Children, cloneElement, useCallback, isValidElement, ReactNode, ReactElement } from 'react';

export interface StopTouchPropagationProps {
  onMouseDown?: any;
  onTouchStart?: any;
  children?: ReactNode;
}

const stopEvent = function (event: Event) {
  event.stopPropagation();
  event.preventDefault();
};

/**
 * This component is util to stop ripple effect when a button is inside CardActionArea
 * @param props
 * @constructor
 */
export function StopTouchPropagation(props: StopTouchPropagationProps): ReactElement | null {
  const { onMouseDown, onTouchStart, children } = props;

  const handleMouseDown = useCallback((event: Event) => {
    stopEvent(event);
    if (onMouseDown) {
      onMouseDown(event);
    }
  }, [onMouseDown]);

  const handleTouchStart = useCallback((event: Event) => {
    stopEvent(event);
    if (onTouchStart) {
      onTouchStart(event);
    }
  }, [onTouchStart]);

  const child = children && Children.only(children);

  if (isValidElement(child)) {
    return cloneElement(child as any, {
      onMouseDown: handleMouseDown,
      onTouchStart: handleTouchStart,
    });
  }

  return child as any;
}

export default StopTouchPropagation;