import React, { ReactNode, HTMLProps } from 'react';
import clsx from 'clsx';
import { makeStyles } from '../../styles';

export interface BasicLayoutProps extends HTMLProps<HTMLDivElement>  {
  header?: ReactNode;
  footer?: ReactNode;
  children?: ReactNode;
}

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
  },
  main: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(2),
  },
  content: {
    flex: 1,
  },
  header: {},
  footer: {
    marginTop: 'auto',
  },
}));

export const BasicLayout = (props: BasicLayoutProps) => {
  const {
    className,
    header,
    footer,
    children,
    ...rest
  } = props;
  const classes = useStyles();
  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <div className={classes.header}>
        {header}
      </div>
      <div className={classes.content}>
        {children}
      </div>
      <div className={classes.footer}>
        {footer}
      </div>
    </div>
  );
};

export default BasicLayout;