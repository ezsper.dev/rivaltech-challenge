import React from 'react';
import { ThemeProvider, ThemeProviderProps } from './ThemeProvider';
import { theme } from './themes/default';

export const DefaultThemeProvider = (props: Omit<ThemeProviderProps, 'theme'>) => (
  <ThemeProvider theme={theme} {...props} />
);

export default DefaultThemeProvider;