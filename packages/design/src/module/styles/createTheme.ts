import createMuiTheme, { ThemeOptions as MuiThemeOptions } from '@material-ui/core/styles/createMuiTheme';

export interface ThemeOptions extends MuiThemeOptions {}

export const createTheme = (options?: ThemeOptions) => {
  const { ...muiOptions } = options;
  return createMuiTheme(muiOptions);
};