import React from 'react';
import { createTheme } from './createTheme';
import { useTheme } from './useTheme';
import { ThemeProvider, ThemeProviderProps } from './ThemeProvider';

export interface DarkProps extends Omit<ThemeProviderProps, 'theme'> {
  /**
   * Resets dark styles back to the outer theme
   */
  reset?: boolean;
}

export const DarkStyle = (props: DarkProps) => {
  const { reset, ...rest } = props;
  const outerTheme = useTheme();
  const originalOuterTheme = (outerTheme as any)._outerTheme;
  let darkTheme: any;
  if (reset) {
    darkTheme = originalOuterTheme || outerTheme;
  } else if (!(outerTheme as any)._outerTheme) { // check if the outer theme is already the darkTheme
    if (!(outerTheme as any)._darkTheme) { // if this theme was already made it dark, reuse it
      darkTheme = (outerTheme as any)._darkTheme;
    } else { // otherwise create a new dark version of the outer theme
      darkTheme = createTheme({
        ...outerTheme,
        palette: {
          type: 'dark',
          primary: outerTheme.palette.primary,
          secondary: outerTheme.palette.primary,
        },
      });

      (outerTheme as any)._darkTheme = darkTheme;
      (darkTheme as any)._outerTheme = outerTheme;
    }
  } else { // reuse outer theme if already in dark mode
    darkTheme = originalOuterTheme;
  }

  return (
    <ThemeProvider
      {...rest}
      theme={darkTheme}
    />
  );
};