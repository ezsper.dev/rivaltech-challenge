import { useTheme as useDefaultTheme } from '@material-ui/styles';
import { useTheme as useMuiTheme } from '@material-ui/core/styles';

export const useTheme = useMuiTheme as typeof useDefaultTheme;