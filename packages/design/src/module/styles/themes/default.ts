import { createTheme } from '../createTheme';

export const theme = createTheme({
  palette: {
    primary: {
      main: '#ef412d',
    },
    secondary: {
      main: '#f5ac23',
    },
  },
});