import { Theme as MuiTheme } from './Theme';

declare module '@material-ui/styles/DefaultTheme/index' {
  export interface DefaultTheme extends MuiTheme {}
}

export {
  getContrastRatio,
  getLuminance,
} from '@material-ui/core/styles';

export {
  default as createPalette,
} from '@material-ui/core/styles/createPalette';

export {
  createStyles,
  withTheme,
  withStyles,
  createGenerateClassName,
} from '@material-ui/styles';

export { makeStyles } from './makeStyles';
export { useTheme } from './useTheme';
export { useUniqueId } from './useUniqueId';

export * from './createTheme';
export * from './ThemeProvider';
export * from './Theme';
export * from './colorManipulator';