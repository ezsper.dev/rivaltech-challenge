import React  from 'react';
import {
  ThemeProviderProps as MuiThemeProviderProps,
} from '@material-ui/styles';
import {
  ThemeProvider as MuiThemeProvider,
} from '@material-ui/core/styles';
import { Theme } from './Theme';

export interface ThemeProviderProps extends MuiThemeProviderProps<Theme> {}

export const ThemeProvider = (props: ThemeProviderProps) => {
  const { children, ...rest } = props;
  return (
    <MuiThemeProvider {...rest}>
      {children}
    </MuiThemeProvider>
  );
};