import { Theme as MuiTheme } from '@material-ui/core/styles';

export interface Theme extends MuiTheme {}