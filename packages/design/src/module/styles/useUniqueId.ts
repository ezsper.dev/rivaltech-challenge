import { useMemo } from 'react';
import { useTheme } from './useTheme';

const $nextTick$ = Symbol.for('uniqueId.nextTick');
/**
 * Generate unique ids for A11Y
 * @param prefix
 */

export const useUniqueId = (prefix: string, defaultId?: string) => {
  const theme = useTheme();
  const tick = useMemo(() => {
    return theme[$nextTick$] == null
      ? (theme[$nextTick$] = 0)
      : ++theme[$nextTick$];
  }, [theme]);
  return defaultId || `${prefix}${tick}`;
};