import { makeStyles as makeDefaultStyles } from '@material-ui/styles'
import { makeStyles as makeMuiStyles } from '@material-ui/core/styles';

export const makeStyles = makeMuiStyles as typeof makeDefaultStyles;