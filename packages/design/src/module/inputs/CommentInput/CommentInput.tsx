import React, { useState, useCallback, HTMLProps } from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import Avatar from '@material-ui/core/Avatar';
import { ProgressButton } from '../../buttons/ProgressButton';
import { PostOwner } from '../../data/typings';

export interface CommentInputProps extends Omit<HTMLProps<HTMLFormElement>, 'onSubmit'> {
  onSubmit?: (content: string, clear: () => void) => void;
  submitting?: boolean;
  defaultValue?: string;
  author?: Pick<PostOwner, 'avatarSrc' | 'initials' | 'displayName'> | null;
}

export const CommentInput = (props: CommentInputProps) => {
  const { author, onSubmit, style, defaultValue,  submitting = false, ...rest } = props;
  const [value, setValue] = useState(defaultValue || '');
  const handleChange = useCallback<Exclude<TextFieldProps['onChange'], undefined>>((event) => {
    setValue(event.target.value);
  }, []);
  const handleDiscard = useCallback(() => {
    if (!submitting) {
      setValue('');
    }
  }, [submitting]);
  const handleSubmit = useCallback(() => {
    if (onSubmit) {
      onSubmit(value, () => setValue(''));
    }
  }, [onSubmit, value]);
  return (
    <form style={{ maxWidth: 600, ...style }} {...rest}>
      <Grid container spacing={1} alignItems="flex-start">
        <Grid item>
          <Avatar
            aria-label="user avatar"
            style={{ marginTop: 15}}
            {...author && author.avatarSrc
              ? { src: author.avatarSrc }
              : { children: author && author.initials }
            }
          />
        </Grid>
        <Grid item style={{ flex: 1 }}>
          <TextField
            onChange={handleChange}
            id="input-with-icon-grid"
            label="Comment"
            multiline
            value={value}
            disabled={submitting || !author}
            placeholder="Tell your thoughts"
            fullWidth
          />
        </Grid>
      </Grid>
      <Box display="flex" justifyContent="flex-end">
        <Button
          disabled={submitting || !author}
          style={{ marginRight: 10 }}
          onClick={handleDiscard}>
          Discard
        </Button>
        <ProgressButton
          onClick={handleSubmit}
          variant="contained"
          disabled={submitting || !author}
          loading={submitting}
          color="primary">
          Publish
        </ProgressButton>
      </Box>
    </form>
  );
};

export default CommentInput;