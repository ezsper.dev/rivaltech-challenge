import React, { useState, useCallback, useEffect, useRef, MouseEvent, ChangeEvent, ReactNode } from 'react';
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import SwipeableViews from 'react-swipeable-views';
import Fade from '@material-ui/core/Fade';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { ProgressButton } from '../../buttons/ProgressButton';
import { LoadingBox } from '../../feedback/LoadingBox';
import { IconButtonPopover, IconButtonPopoverProps } from '../../buttons/IconButtonPopover';
import { SongPlaylistPrivacySelect } from '../SongPlaylistPrivacySelect';
import { useTheme } from '../../styles';

type Playlist = {
  id: string;
  title: string;
};

interface CreateNewInput {
  title: string;
  privacy: 'PRIVATE' | 'PUBLIC' | 'UNLISTED';
}

export interface SongPlaylistSelectProps extends Omit<IconButtonPopoverProps, 'icon' | 'aria-label'> {
  disableCreateNew?: boolean;
  createNewInput?: CreateNewInput;
  createNewInputDefault?: Partial<CreateNewInput>;
  isCreatingNew?: boolean;
  onCreateNewClick?: (event: MouseEvent<HTMLAnchorElement>) => void;
  createNewLabel?: string;
  createNewHref?: string;
  defaultSelectedPlaylists?: string[];
  selectedPlaylists?: string[];
  onSelectedPlaylists?: (selectedPlaylists: string[]) => void;
  loadingPlaylists?: boolean;
  playlists?: Array<Playlist>;
  onOpen?: () => void;
  noPlaylistsMessage?: ReactNode;
  onCreateNewChange?: (data: CreateNewInput) => void;
  onCreateNewSubmit?: (data: CreateNewInput, done: () => void) => void;
}

const emptyList: any[] = [];
const emptyObj: any = {};

export function SongPlaylistSelect(props: SongPlaylistSelectProps) {
  const {
    onOpen,
    loadingPlaylists = false,
    createNewLabel = `Create new playlist`,
    noPlaylistsMessage = `Sorry! You have no playlists available.`,
    disableCreateNew = false,
    isCreatingNew = false,
    defaultSelectedPlaylists,
    selectedPlaylists: selectedPlaylistsProp,
    onSelectedPlaylists,
    createNewInputDefault = emptyObj as Partial<CreateNewInput>,
    createNewInput: createNewInputProp,
    playlists = emptyList as Array<Playlist>,
    onCreateNewChange,
    onCreateNewSubmit,
    ...rest
  } = props;

  const theme = useTheme();
  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen,
  };
  const hasPlaylists = playlists.length > 0;
  const [selectedPlaylistsState, setSelectedPlaylistsState] = useState(
    () => defaultSelectedPlaylists || [],
  );
  const selectedPlaylists = selectedPlaylistsProp || selectedPlaylistsState;
  const handleCheckboxChange = useCallback((item: Playlist, event: ChangeEvent<HTMLInputElement>) => {
    const newSelectedPlaylists = selectedPlaylists.slice();
    const index = newSelectedPlaylists.findIndex(id => id === item.id);
    if (event.target.checked) {
      if (index < 0) {
        newSelectedPlaylists.push(item.id);
      }
    } else if (index >= 0) {
      newSelectedPlaylists.splice(index, 1);
    }

    if (selectedPlaylistsProp == null) {
      setSelectedPlaylistsState(newSelectedPlaylists);
    }
    if (onSelectedPlaylists) {
      return onSelectedPlaylists(newSelectedPlaylists);
    }
  }, [selectedPlaylists, onSelectedPlaylists, selectedPlaylistsProp]);

  const [isCreateNew, setCreateNew] = useState(() =>
    (isCreatingNew || (!loadingPlaylists && !hasPlaylists)) && !disableCreateNew,
  );
  const handleCreateNew = useCallback(() => {
    setCreateNew(true);
  }, []);

  const controlled = useRef({
    preventEffect: false,
  });


  const [open, setOpen] = useState(false);

  const shouldReturnToList = !open && hasPlaylists && !isCreatingNew && !disableCreateNew;
  useEffect(() => {
    let timer: any;
    if (shouldReturnToList) {
      timer = setTimeout(() => setCreateNew(false), 200);
    }
    return () => {
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, [shouldReturnToList]);

  const handleButtonClick = useCallback(() => {
    setOpen(open => {
      if (!open && onOpen) {
        onOpen();
      }
      return !open;
    });
  }, [onOpen]);

  const handleCancelClick = useCallback(() =>
    hasPlaylists
      ? setCreateNew(false)
      : setOpen(false),
    [hasPlaylists],
  );

  const [createNewInputState, setCreateNewInputState] = useState<CreateNewInput>(() => ({
    title:  createNewInputDefault.title || '',
    privacy: createNewInputDefault.privacy || 'PRIVATE',
  }));

  const createNewInput = createNewInputProp || createNewInputState;

  const handleCreateNewChange = useCallback((event: ChangeEvent<{ name?: string; value: unknown }>, ...args) => {
    if (!event.target.name) {
      return;
    }

    const createNewInputChange = {
      ...createNewInput,
      [event.target.name]: event.target.value,
    };

    if (!createNewInputProp) {
      setCreateNewInputState(createNewInputChange);
    }

    if (onCreateNewChange) {
      onCreateNewChange(createNewInputChange);
    }
  }, [createNewInput, setCreateNewInputState, createNewInputProp, onCreateNewChange]);

  const doneCallback = useCallback(() => {
    if (!createNewInputProp) {
      setCreateNewInputState({
        title: '',
        privacy: 'PRIVATE',
      });
    }
    controlled.current.preventEffect = true;
    setCreateNew(false);
  }, []);

  const handleCreateNewSubmit = useCallback(() => {
    if (isCreatingNew || !onCreateNewSubmit) {
      return;
    }
    return onCreateNewSubmit(createNewInput, doneCallback);
  }, [isCreatingNew, isCreateNew, createNewInputProp, createNewInput, onCreateNewSubmit]);

  useEffect(() => {
    if (controlled.current.preventEffect) {
      return;
    }
    if (!isCreateNew && !loadingPlaylists && !hasPlaylists && !disableCreateNew) {
      setCreateNew(true);
    }
  }, [isCreateNew, loadingPlaylists, hasPlaylists, disableCreateNew]);

  const actionsRef = useRef<{ updateHeight(): void }>();

  useEffect(() => {
    if (actionsRef.current) {
      actionsRef.current.updateHeight();
    }
  }, [open, loadingPlaylists, playlists]);

  return (
    <IconButtonPopover
      {...rest}
      aria-label="add to playlist"
      icon={<PlaylistAddIcon />}
      onClick={handleButtonClick}
      PopoverProps={{
        open,
        onClose: () => {
          setOpen(false);
        },
      }}
      PaperProps={{
        style: { minWidth: 200, maxWidth: 300 },
      }}
      width="auto">
      <LoadingBox
        loading={loadingPlaylists}
        whileLoading="Loading playlists"
      >
        <SwipeableViews
            animateHeight
            disableLazyLoading
            index={isCreateNew ? 1 : 0}
            {...{ action: (actions: any) => actionsRef.current = actions } as {}}
        >
          <div>
            <Fade timeout={transitionDuration} in={open && !isCreateNew} unmountOnExit>
              <List component="div">
                {!disableCreateNew && (
                  <>
                    <ListItem button onClick={handleCreateNew}>
                      <ListItemIcon>
                        {isCreatingNew
                          ? <CircularProgress color="primary" size={15} />
                          : <LibraryAddIcon fontSize="small" />}
                      </ListItemIcon>
                      <Typography variant="inherit">{createNewLabel}</Typography>
                    </ListItem>
                    {hasPlaylists && <Divider />}
                  </>
                )}
                {playlists.map(playlist => (
                  <ListItem
                    {...{ component: 'label' }}
                    button
                    tabIndex="-1"
                    key={playlist.id}
                  >
                    <ListItemIcon>
                      <Checkbox
                        style={{ padding: 0 }}
                        size="small"
                        checked={selectedPlaylists.includes(playlist.id)}
                        onChange={event => handleCheckboxChange(playlist, event)} />
                    </ListItemIcon>
                    <Typography variant="inherit">{playlist.title}</Typography>
                  </ListItem>
                ))}
                {disableCreateNew && playlists.length < 1 && (
                  <ListItem>
                    {noPlaylistsMessage}
                  </ListItem>
                )}
              </List>
            </Fade>
          </div>
          <div>
            <Fade timeout={transitionDuration} in={open && isCreateNew} unmountOnExit>
             <div style={{ padding: `10px 20px` }}>
               <Typography gutterBottom component="h5" variant="h6">
                 Create new playlist
               </Typography>
               <TextField
                 style={{ marginTop: 15 }}
                 label="Title"
                 name="title"
                 value={createNewInput.title}
                 InputLabelProps={{
                   shrink: true,
                 }}
                 disabled={isCreatingNew}
                 autoFocus
                 fullWidth
                 onChange={handleCreateNewChange}
                 placeholder="Insert playlist name"
               />
               <SongPlaylistPrivacySelect
                 fullWidth
                 style={{ marginTop: 15 }}
                 value={createNewInput.privacy}
                 disabled={isCreatingNew}
                 onChange={handleCreateNewChange}
               />
               <div style={{ marginTop: 20, textAlign: 'right' }}>
                 <Button
                   onClick={handleCancelClick}>
                   {hasPlaylists ? 'Back' : 'Cancel'}
                 </Button>
                 <ProgressButton
                   onClick={handleCreateNewSubmit}
                   loading={isCreatingNew}
                   disabled={isCreatingNew}
                   color="primary"
                   variant="contained"
                   style={{ marginLeft: 10 }}>
                   Create
                 </ProgressButton>
               </div>
             </div>
            </Fade>
          </div>
        </SwipeableViews>
      </LoadingBox>
    </IconButtonPopover>
  );
}

export default SongPlaylistSelect;