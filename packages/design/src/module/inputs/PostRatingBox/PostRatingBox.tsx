import React, { HTMLProps}  from 'react';
import clsx from 'clsx';
import Rating, { RatingProps } from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import LinearProgress from '@material-ui/core/LinearProgress';
import StarIcon from '@material-ui/icons/Star';
import { makeStyles, createStyles } from '../../styles';
import RatingCountMessage from '../../data-display/RatingCountMessage';

export interface PostRatingBoxProps extends Omit<HTMLProps<HTMLDivElement>, 'onChange' | 'value'> {
  RatingProps?: RatingProps;
  avgValue: number;
  loading?: boolean;
  value: number | null;
  total: number;
  onChange?: RatingProps['onChange'];
}

const useStyles = makeStyles(() => createStyles({
  root: {
    width: 300,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

export const PostRatingBox = (props: PostRatingBoxProps) => {
  const {
    className,
    value,
    avgValue,
    total,
    RatingProps,
    loading = false,
    onChange,
    ...rest
  } = props;
  const classes = useStyles();
  return (
    <div className={clsx(className, classes.root)}
      {...rest}
    >
      <Box display="flex" flexDirection="row" alignItems="center" mb={2} borderColor="transparent">
        <FormControl>
          <Box display="flex" flexDirection="column" alignItems="center">
            <Rating
              size="large"
              value={value ?  value / 2 : undefined}
              name="rating" /* TODO externalizze to props */
              icon={<StarIcon color="primary" fontSize="inherit" />}
              emptyIcon={<StarBorderIcon fontSize="inherit" />}
              precision={0.5}
              onChange={onChange}
              {...RatingProps}
            />
          </Box>
        </FormControl>
        <div style={{ padding: '0 20px', height: '30px'}}>
          <Divider orientation="vertical" />
        </div>
        <Box display="flex" flexDirection="column" alignItems="center">
          <Rating
            size="small"
            readOnly
            value={avgValue / 2}
            precision={0.5}
          />
          <Divider />
          <RatingCountMessage
            value={total}
            component={Typography}
            variant="body2"
          />
        </Box>
      </Box>
      {loading && (
        <LinearProgress style={{ width: '100%'}} />
      )}
    </div>
  );
};

export default PostRatingBox;