import React from 'react';
import clsx from 'clsx';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import InputBase, { InputBaseProps } from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import LinearProgress from '@material-ui/core/LinearProgress';
import SearchIcon from '@material-ui/icons/Search';

export interface SearchBoxProps extends InputBaseProps {
  PaperProps?: PaperProps;
  loading?: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 400,
    },
    inner: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  }),
);

export function SearchBox(props: SearchBoxProps) {
  const { className, style, PaperProps, loading = false, ...rest } = props;
  const classes = useStyles();

  return (
    <Paper className={clsx(classes.root, className)} style={style}>
      <div className={classes.inner}>
        <InputBase
          {...rest}
          className={clsx(classes.input)}
        />
        <IconButton className={classes.iconButton} aria-label="search">
          <SearchIcon />
        </IconButton>
      </div>
      {loading && (<LinearProgress variant="query" />)}
    </Paper>
  );
}

export default SearchBox;