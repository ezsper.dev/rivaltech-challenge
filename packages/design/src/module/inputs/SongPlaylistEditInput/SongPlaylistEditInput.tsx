import React, { useState, useCallback, HTMLProps, useRef, ChangeEvent, useEffect } from "react";
import clsx from 'clsx';
import { ContentState, Editor, EditorState } from 'draft-js';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { SongPlaylistSelect } from '../SongPlaylistSelect';
import { SongTableList, SongTableListProps } from '../../data-display/SongTableList';
import { SongPlaylistPrivacySelect } from '../SongPlaylistPrivacySelect';
import { makeStyles, createStyles, lighten } from '../../styles';
import { Song, SongArtist, SongArtistAlbum, SongPlaylist, SongPlaylistItem } from "../../data/typings";

const useStyles = makeStyles(theme => createStyles({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
        color: theme.palette.secondary.main,
        backgroundColor: lighten(theme.palette.secondary.light, 0.85),
      }
      : {
        color: theme.palette.text.primary,
        backgroundColor: theme.palette.secondary.dark,
      },
  title: {
    flex: '1 1 100%',
  },
  published: {
    display: 'inline-flex',
    marginRight: theme.spacing(1),
    justifyContent: 'flex-end',
  }
}));

type SongPlaylistItemShape = Pick<SongPlaylistItem, 'createdAt'> & {
  song: Pick<Song, 'id' | 'title'> & {
    artist: Pick<SongArtist, 'id' | 'title'>;
    album: Pick<SongArtistAlbum, 'id' | 'title'>;
  };
};

export interface CommentInputProps<T extends SongPlaylistItemShape> extends Omit<HTMLProps<HTMLFormElement>, 'onSubmit'>, Pick<SongTableListProps<T>, 'onReorder'> {
  onSubmit?: (content: { title?: string }, clear: () => void) => void;
  submitting?: boolean;
  title?: string;
  onTitleChange?: (newTitle: string) => void;
  privacy?: SongPlaylist['privacy'];
  onPrivacyChange?: (newPrivacy: SongPlaylist['privacy']) => void;
  items: T[];
}

const editorStateFromText = (text: string | null | undefined) => {
  if (!text) {
    return EditorState.createEmpty();
  }
  return EditorState.createWithContent(ContentState.createFromText(text));
};

export function SongPlaylistEditInput<T extends SongPlaylistItemShape>(props: CommentInputProps<T>) {
  const classes = useStyles(props);
  const {
    onSubmit,
    items,
    title: titleProp,
    onTitleChange,
    privacy: privacyProp,
    onPrivacyChange,
    submitting = false,
    onReorder,
    ...rest
  } = props;
  const [selected, setSelected] = useState([]);
  const [title, setTitle] = useState<EditorState>(() => editorStateFromText(titleProp));
  const pointer = useRef({
    title,
  }).current;

  useEffect(() => {
    if (title.getCurrentContent().getPlainText() !== titleProp) {
      setTitle(editorStateFromText(titleProp));
    }
  }, [titleProp]);

  const handleTitleSave = useCallback((state: EditorState) => {
    pointer.title = state;
    setTitle(state);
    if (onTitleChange) {
      onTitleChange(state.getCurrentContent().getPlainText());
    }
  }, [onTitleChange, pointer]);

  const handleSelection = useCallback((newSelection) => {
    setSelected(newSelection);
  }, []);

  const handleSubmit = useCallback(() => {
    if (onSubmit) {
      onSubmit({ title: title.toString() }, () => {
        setTitle(editorStateFromText(undefined));
      });
    }
  }, [onSubmit, title]);

  const handlePrivacyChange = useCallback((event: ChangeEvent<{ value: string }>) => {
    if (onPrivacyChange) {
      onPrivacyChange(event.target.value as any);
    }
  }, [onPrivacyChange]);

  if (submitting) {}

  const preventReturn = () => 'handled' as any;
  const numSelected = selected.length;
  return (
    <form {...rest} onSubmit={handleSubmit}>
      <Grid container spacing={1} alignItems="flex-start">
        <Grid item style={{ flex: 1 }}>
          <Toolbar
            className={clsx(classes.root, {
              [classes.highlight]: numSelected > 0,
            })}
          >
            {numSelected > 0 ? (
              <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
                {numSelected} selected
              </Typography>
            ) : (
              <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                <Editor
                  editorState={title}
                  onChange={handleTitleSave}
                  placeholder="Unnamed Playlist"
                  handleReturn={preventReturn}
                />
              </Typography>
            )}
            {numSelected < 1 && (
              <div className={classes.published}>
                <SongPlaylistPrivacySelect
                  defaultValue={privacyProp}
                  onChange={handlePrivacyChange as any}
                  variant="outlined"
                />
              </div>
            )}
            {numSelected > 0 && (
              <div className={classes.published}>
                <Tooltip title="Add to playlist">
                  <SongPlaylistSelect
                    PopoverProps={{
                      anchorOrigin: {
                        vertical: 'top',
                        horizontal: 'right',
                      },
                      transformOrigin: {
                        vertical: 'top',
                        horizontal: 'right',
                      },
                    }}
                  />
                </Tooltip>
                <Tooltip title="Delete">
                  <IconButton aria-label="delete">
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              </div>
            )}
          </Toolbar>
        </Grid>
      </Grid>
      <SongTableList
        items={items}
        onReorder={onReorder}
        editable selected={selected}
        onSelection={handleSelection} />
    </form>
  );
};

export default SongPlaylistEditInput;