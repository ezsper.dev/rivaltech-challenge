import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import Select, { SelectProps } from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import PublicIcon from '@material-ui/icons/Public';
import LockIcon from '@material-ui/icons/Lock';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import FormControl from '@material-ui/core/FormControl';
import { useUniqueId } from '../../styles/useUniqueId';

export interface SongPlaylistPrivacySelectProps extends SelectProps {}

export function SongPlaylistPrivacySelect(props: SongPlaylistPrivacySelectProps) {
  const {
    style,
    variant,
    fullWidth,
    labelId: labelIdProp,
    ...rest
  } = props;
  const labelId = useUniqueId('privacy', labelIdProp);
  return (
    <FormControl
      size="small"
      fullWidth={fullWidth}
      variant={variant}
      style={{ minWidth: 200, ...style }}>
      <InputLabel id={labelId}>
        Privacy
      </InputLabel>
      <Select
        variant={variant}
        labelId={labelId}
        fullWidth={fullWidth}
        name="privacy"
        label="Privacy"
        {...rest}
      >
        <MenuItem value="PRIVATE">
          <ListItemIcon>
            <LockIcon fontSize="small"/>
          </ListItemIcon>
          <Typography variant="inherit">Private</Typography>
        </MenuItem>
        <MenuItem value="UNLISTED">
          <ListItemIcon>
            <VisibilityOffIcon fontSize="small"/>
          </ListItemIcon>
          <Typography variant="inherit">Not Listed</Typography>
        </MenuItem>
        <MenuItem value="PUBLIC">
          <ListItemIcon>
            <PublicIcon fontSize="small"/>
          </ListItemIcon>
          <Typography variant="inherit">Public</Typography>
        </MenuItem>
      </Select>
    </FormControl>
  );
};

export default SongPlaylistPrivacySelect;