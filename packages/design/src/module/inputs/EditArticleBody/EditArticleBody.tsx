import React, { useState, useMemo, useRef, useEffect, useCallback } from 'react';
import { Editor, EditorState, ContentState, convertToRaw } from 'draft-js';
import { stateToHTML, Options as ToHTMLOptions } from 'draft-js-export-html';
import { stateFromHTML, Options as FromHTMLOptions } from 'draft-js-import-html';
import MUIRichTextEditor from 'mui-rte';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles, createStyles } from '../../styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import 'draft-js/dist/Draft.css';
import { Post, PostCategory } from '../../data/typings';

type PostType = Pick<Post, 'title' | 'subheading' | 'content' | 'excerpt'>
  & { category: PostCategoryType | null };
type PostCategoryType = Pick<PostCategory, 'id' | 'title'>;

export interface EditArticleBodyProps {
  post?: PostType;
  disabled?: boolean;
  richTextToolbarStickyTop?: number;
  categories?: PostCategoryType[];
  htmlParsingToOptions?: ToHTMLOptions;
  htmlParsingFromOptions?: FromHTMLOptions;
  onTitleChange?: (newTitle: string) => void;
  onSubheadingChange?: (newSubheading: string) => void;
  onContentChange?: (newContent: string) => void;
  onCategoryChange?: (newCategory: PostCategoryType | null) => void;
}

const useStyles = makeStyles(theme => createStyles({
  /* Styles applied to the root element. */
  root: {
    height: 264,
    flexGrow: 1,
    maxWidth: 400,
  },
  content: (props: EditArticleBodyProps) => ({
    marginTop: theme.spacing(5),
    '& #content-root img': {
      maxWidth: '100%',
    },
    '& #content-toolbar': {
      position: 'sticky',
      top: 0,
      backgroundColor: fade(theme.palette.background.paper, 0.9),
      transform: `translate(0, ${props.richTextToolbarStickyTop || 0}px)`,
      transition: theme.transitions.create('transform'),
      zIndex: theme.zIndex.appBar - 1,
    },
  }),
}));


const preventReturn = () => 'handled' as any;

const SelectCategory = (props: EditArticleBodyProps) => {
  const { onCategoryChange, post, categories } = props;
  const [open, setOpen] = useState(false);
  const [options, setOptions] = useState<PostCategoryType[]>([]);
  const loading = open && (!categories || categories.length === 0);
  const postCategory = post ? post.category || undefined : undefined;
  useEffect(() => {
    if (!open) {
      setOptions(categories || (postCategory ? [postCategory] : []));
    }
  }, [open, postCategory, categories]);
  const handleChangeEvent = useCallback((event: any, item: PostCategoryType | null) => {
    if (onCategoryChange) {
      onCategoryChange(item);
    }
  }, [onCategoryChange]);
  return (
    <Autocomplete
      id="category"
      style={{ width: 300 }}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      value={postCategory}
      getOptionSelected={(option, value) => option.id === value.id}
      getOptionLabel={option => option.title}
      onChange={handleChangeEvent}
      options={options}
      loading={loading}
      renderInput={params => (
        <TextField
          {...params}
          label="Category"
          placeholder="Select a category"
          fullWidth
          disabled={props.disabled}
          size="small"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
};

const editorStateFromText = (text: string | null | undefined) => {
  if (!text) {
    return EditorState.createEmpty();
  }
  return EditorState.createWithContent(ContentState.createFromText(text));
};

const editorStateFromHTML = (html: string | null | undefined, options: any) => {
  if (!html) {
    return EditorState.createEmpty();
  }
  return EditorState.createWithContent(stateFromHTML(html, options));
};

// TODO FIX
const hasFocus = (state: EditorState) => state.getSelection().getHasFocus();

const richTextControls = [
  'title', 'bold', 'italic', 'underline', 'strikethrough', 'highlight', 'undo', 'redo',
  'link', 'media', 'numberList', 'bulletList', 'quote', 'code', 'clear',
];

export const defaultHtmlParsingToOptions: ToHTMLOptions = {
  entityStyleFn: (entity: any) => {
    const entityType = entity.get('type').toLowerCase();
    if (entityType === 'image') {
      const data = entity.getData();
      return {
        element: 'img',
        attributes: {
          src: data.src || data.url,
        },
        style: {},
      };
    }
    return;
  },
};

export const defaultHtmlParsingFromOptions: FromHTMLOptions = {
  customInlineFn: (element: any, {Style, Entity}: any) => {
    if (element.tagName === 'SPAN' && element.className === 'emphasis') {
      return Style('ITALIC');
    } else if (element.tagName === 'IMG') {
      return Entity('IMAGE', { url: element.getAttribute('src') });
    }
  },
};

export const EditArticleBody = (props: EditArticleBodyProps) => {
  const {
    post,
    onTitleChange,
    onSubheadingChange,
    onContentChange,
    disabled,
    htmlParsingToOptions = defaultHtmlParsingToOptions,
    htmlParsingFromOptions = defaultHtmlParsingFromOptions,
  } = props;

  const classes = useStyles(props);

  const [title, setTitle] = useState<EditorState>(() => editorStateFromText(undefined));
  const [subheading, setSubheading] = useState<EditorState>(() => editorStateFromText(undefined));
  const [content, setContent] = useState<EditorState>(() => editorStateFromHTML(undefined, htmlParsingFromOptions));

  const pointer = useRef({
    title,
    subheading,
    content,
  }).current;

  const postTitle = post ? post.title : null;
  const postSubheading = post ? post.subheading : null;
  const postContent = post ? post.content : null;

  useEffect(() => {
    if (!hasFocus(pointer.title)) {
      setTitle(editorStateFromText(postTitle));
    }
  }, [postTitle, pointer]);

  useEffect(() => {
    if (!hasFocus(pointer.subheading)) {
      setSubheading(editorStateFromText(postSubheading));
    }
  }, [postSubheading, pointer]);

  useEffect(() => {
    if (!hasFocus(pointer.content)) {
      setContent(editorStateFromHTML(postContent, htmlParsingFromOptions));
    }
  }, [postContent, pointer, htmlParsingFromOptions]);

  const handleTitleSave = useCallback((state: EditorState) => {
    pointer.title = state;
    setTitle(state);
    if (onTitleChange) {
      onTitleChange(state.getCurrentContent().getPlainText());
    }
  }, [onTitleChange, pointer]);

  const handleSubheadingSave = useCallback((state: EditorState) => {
    pointer.subheading = state;
    setSubheading(state);
    if (onSubheadingChange) {
      onSubheadingChange(state.getCurrentContent().getPlainText());
    }
  }, [onSubheadingChange, pointer]);

  const handleContentSave = useCallback((state: EditorState) => {
    pointer.content = state;
    if (onContentChange) {
      onContentChange(stateToHTML(state.getCurrentContent(), htmlParsingToOptions));
    }
  }, [onContentChange, pointer, htmlParsingToOptions]);

  const contentValue = useMemo(() => JSON.stringify(convertToRaw(content.getCurrentContent())), [content]);
  return (
    <Container maxWidth="md">
        <Box mb={2}>
          <SelectCategory {...props} />
        </Box>
        <Typography component="div" variant="h2">
          <Editor
            editorState={title}
            readOnly={disabled}
            onChange={handleTitleSave}
            placeholder="Title"
            handleReturn={preventReturn}
          />
        </Typography>
        <Typography component="div" variant="h5" gutterBottom>
          <Editor
            editorState={subheading}
            readOnly={disabled}
            onChange={handleSubheadingSave}
            placeholder="Subheading"
            handleReturn={preventReturn}
          />
        </Typography>
        <Typography component="div" variant="body1" gutterBottom className={classes.content}>
          <MUIRichTextEditor
            id="content"
            readOnly={disabled}
            draftEditorProps={{
              placeholder: 'Tell your story...'
            } as any}
            label="Tell your story..."
            value={contentValue}
            controls={richTextControls}
            onChange={handleContentSave}
          />
        </Typography>
    </Container>
  );
};

export default EditArticleBody;