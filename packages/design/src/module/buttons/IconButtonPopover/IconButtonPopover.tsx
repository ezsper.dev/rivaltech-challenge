import React, { ReactNode, useCallback, ComponentType } from 'react';
import IconButton, { IconButtonProps } from '@material-ui/core/IconButton';
import Popover, { PopoverProps } from '@material-ui/core/Popover';
import { PaperProps } from '@material-ui/core/Paper';
import { makeStyles, createStyles, useUniqueId } from '../../styles';

export interface IconButtonPopoverProps extends IconButtonProps {
  ['aria-label']: string;
  icon: ReactNode;
  maxHeight?: string | number;
  width?: string | number;
  PopoverComponent?: ComponentType<PopoverProps>;
  PopoverProps?: Partial<PopoverProps>;
  PaperProps?: Partial<PaperProps>;
}

const useStyles = makeStyles(theme => createStyles({
  menu: {},
}));

export function IconButtonPopover(props: IconButtonPopoverProps) {
  const {
    children,
    'aria-label': ariaLabel,
    'aria-controls': ariaControls,
    PopoverComponent = Popover,
    PopoverProps,
    PaperProps,
    onClick,
    icon,
    ...rest
  } = props;
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const classes = useStyles(props);

  const stopEvent = useCallback((e: any) => {
    e.stopPropagation();
  }, []);

  const handleClick = useCallback((event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
    if (onClick) {
      return onClick(event);
    }
  }, [onClick]);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const ariaControlsGenerated = useUniqueId(ariaLabel, ariaControls);

  return (
    <>
      <IconButton
        key="button"
        aria-label={ariaLabel}
        aria-controls={ariaControlsGenerated}
        aria-haspopup="true"
        onMouseDown={stopEvent}
        onTouchStart={stopEvent}
        {...rest}
        onClick={handleClick}
      >
        {icon}
      </IconButton>
      <PopoverComponent
        id={ariaControlsGenerated}
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        onMouseDown={stopEvent}
        onTouchStart={stopEvent}
        {...PopoverProps}
        PaperProps={{
          ...(PopoverProps && PopoverProps.PaperProps),
          ...PaperProps,
          className: classes.menu,
        }}
      >
        {children}
      </PopoverComponent>
    </>
  );
}

export default IconButtonPopover;