import React from 'react'
import Button, { ButtonProps } from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

export interface ProgressButtonProps extends ButtonProps {
  loading?: boolean;
}

export function ProgressButton(props: ProgressButtonProps) {
  const {
    loading = false,
    children,
    startIcon,
    color,
    disabled,
    ...rest
  } = props;
  const progressColor = disabled ? color === 'default' ? 'inherit' : color : 'inherit';
  return (
    <Button
      {...rest}
      disabled={disabled}
      startIcon={loading ? <CircularProgress color={progressColor} size={15} /> : startIcon}
      color={color}>
      {children}
    </Button>
  );
}

export default ProgressButton;