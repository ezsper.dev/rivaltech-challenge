import React, { ReactNode, useState, useCallback } from 'react';
import clsx from 'clsx';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import Zoom from '@material-ui/core/Zoom';
import { ControlButton, ControlButtonProps } from '../ControlButton';
import { makeStyles, createStyles, fade } from '../../styles';

const useStyles = makeStyles(theme => createStyles({
  '@keyframes pulsing': {
    '0%': {
      boxShadow: `0 0 0 0 ${fade(theme.palette.primary.main, 0)}`,
    },
    '100%': {
      boxShadow: `0 0 0 10px ${fade(theme.palette.primary.main, 0.7)}`,
    },
  },
  relative: {
    position: 'relative',
    display: 'inline-block',
  },
  icon: {
    fontSize: 'inherit',
  },
  pulse: {
    display: 'inline-block',
    pointerEvents: 'none',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: 100,
    boxShadow: `0 0 0 0px ${fade(theme.palette.primary.main, 0)}`,
    transition: theme.transitions.create('all'),
  },
  playing: {
    animationName: '$pulsing',
    animationDuration: '1.5s',
    animationTimingFunction: 'ease-out',
    animationDelay: '0',
    animationPlayState: 'running',
    animationFillMode: 'none',
    animationDirection: 'alternate',
    animationIterationCount: 'infinite',
  },
}));
// <ControlButton>{playing ? <PlayArrowIcon> : <PlayArrowIcon>}</ControlButton>

export type ControlPlayPauseButtonMode =  'playing' | 'paused';
export interface ControlPlayPauseButtonProps extends Omit<ControlButtonProps<ControlPlayPauseButtonMode>, 'transition'> {
  playingPulse?: boolean;
  pulseRadius?: number;
  classNameContainer?: string;
  children?: ReactNode | ((mode: string | boolean) => ReactNode);
}

export function ControlPlayPauseButton(props: ControlPlayPauseButtonProps) {
  const {
    playingPulse,
    pulseRadius,
    defaultMode,
    mode: modeProp,
    onModeChange,
    className,
    classNameContainer,
    ...rest
  } = props;
  const classes = useStyles(props);
  const mode = modeProp || defaultMode || 'paused';
  const [playing, setPlaying] = useState(() => mode === 'playing');
  const handleModeChange = useCallback((newMode: any, event: any) => {
    setPlaying(newMode === 'playing');
    if (onModeChange) {
      return onModeChange(newMode, event);
    }
  }, [onModeChange]);
  return (
    <span
      className={clsx(classNameContainer, classes.relative)}>
      <span className={clsx(classes.pulse, {
        [classes.playing]: playingPulse && playing,
      })} />
      <ControlButton
        {...rest}
        mode={modeProp}
        defaultMode={defaultMode}
        className={clsx(className, classes.relative)}
        onModeChange={handleModeChange}
        modes={['paused', 'playing']}
        transition={Zoom}>
        {mode => React.createElement(mode === 'paused' ? PlayArrowIcon : PauseIcon, { className: classes.icon })}
      </ControlButton>
    </span>
  );
}