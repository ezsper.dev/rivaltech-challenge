import React, { ReactNode, MouseEvent, useState, useCallback, useRef, useEffect } from 'react';
import clsx from 'clsx';
import IconButton, { IconButtonProps } from '@material-ui/core/IconButton';
import { makeStyles, createStyles, useTheme } from '../../styles';

const useStyles = makeStyles(theme => createStyles({
  root: {
    background: theme.palette.background.default,
  },
}));
// <ControlButton>{playing ? <PlayArrowIcon> : <PlayArrowIcon>}</ControlButton>

export interface ControlButtonProps<Mode extends string = string> extends IconButtonProps {
  defaultMode?: Mode;
  mode?: Mode;
  onModeChange?: (value: Mode, event: MouseEvent<HTMLButtonElement>) => void;
  modes?: Mode[];
  children?: ReactNode | ((mode: Mode) => ReactNode);
  transition?: any;
}

export function ControlButton<Mode extends string>(props: ControlButtonProps<Mode>) {
  const {
    mode: modeProp,
    defaultMode,
    onModeChange,
    onClick,
    modes,
    children,
    transition: Transition,
    className,
    ...rest
  } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [modeState, setModeState] = useState(defaultMode || (modes && modes[0]));
  const mode = modeProp ? modeProp : modeState;
  const mountRef = useRef(false);

  const handleClick = useCallback((event: MouseEvent<HTMLButtonElement>) => {
    if (mode && modes && modes.length > 0) {
      const index = modes.indexOf(mode);
      const nextValue = (
        index === modes.length - 1
          ? modes[0]
          : modes[index + 1]
      );

      if (nextValue !== mode) {
        if (onModeChange) {
          onModeChange(nextValue, event);
        }
        if (modeProp === undefined) {
          setModeState(nextValue);
        }
      }
    }

    if (onClick) {
      return onClick(event);
    }
  }, [mode, modeProp, modes, onClick, onModeChange]);

  useEffect(() => {
    mountRef.current = true;
  });

  const transitionDuration = {
    enter: mountRef.current ? theme.transitions.duration.enteringScreen : 0,
    exit: theme.transitions.duration.leavingScreen,
  };

  const renderIcon = () => {
    if (!modes) {
      return children;
    }

    if (modes.length < 1) {
      throw new Error();
    }

    return modes.map(name => {
      const icon = typeof children === 'function' ? children(name) : children;
      const transitionIn = name === mode;

      if (!Transition) {
        return (
          <React.Fragment key={name}>
            {transitionIn ? icon : null}
          </React.Fragment>
        );
      }

      return (
        <Transition
          key={name}
          in={transitionIn}
          timeout={transitionDuration}
          style={{
            ...(!transitionIn && {
              position: 'absolute',
            }),
          }}
          unmountOnExit
        >
          {icon}
        </Transition>
      );
    });
  };

  return (
    <IconButton
      className={clsx(className, classes.root)}
      aria-label="play/pause"
      size="small"
      {...rest}
      onClick={handleClick}
    >
      {renderIcon()}
    </IconButton>
  );
}