import React from 'react';
import { Optional } from 'utility-types';
import Menu, { MenuProps } from '@material-ui/core/Menu';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { IconButtonPopover, IconButtonPopoverProps } from '../IconButtonPopover';

export interface IconButtonMenuProps extends Omit<Optional<IconButtonPopoverProps, 'icon'>, 'PopoverComponent' | 'PopoverProps'> {
  MenuProps?: Partial<MenuProps>;
}

export function IconButtonMenu(props: IconButtonMenuProps) {
  const {
    children,
    MenuProps,
    icon,
    ...rest
  } = props;

  return (
    <IconButtonPopover
      {...rest}
      icon={icon ? icon : <MoreVertIcon />}
      PopoverProps={MenuProps}
      PopoverComponent={Menu}>
      {children}
    </IconButtonPopover>
  );
}

export default IconButtonMenu;