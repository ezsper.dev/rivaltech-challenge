import React, { ReactNode } from 'react';
import clsx from 'clsx';
import * as ReactIs from 'react-is';
import AppBar, { AppBarProps } from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';
import { RivalTechLogo } from '../../logos/RivalTechLogo';
import { makeStyles, createStyles, useTheme } from '../../styles';

export interface HeaderSectionProps extends Omit<AppBarProps, 'variant'> {
  sticked?: boolean;
  extended?: boolean;
  variant?: AppBarProps['variant'] | 'transparent';
  scrollTarget?: () => Window | HTMLElement;
  action?: ReactNode;
  nav?: ReactNode;
  children?: ReactNode;
}

function HideOnScroll(props: any) {
  const { scrollTarget, children } = props;
  const trigger = useScrollTrigger({ target: scrollTarget ? scrollTarget() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children(!trigger)}
    </Slide>
  );
}

const useStyles = makeStyles(theme => createStyles({
  transparent: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
  },
  alignCenter: {
    alignItems: 'center',
  },
  toolbar: {
    minHeight: 64,
  },
  toolbarExtended: {
    minHeight: 130,
    alignItems: 'flex-start',
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(2),
  },
  logo: {
    flexGrow: 1,
    alignSelf: 'flex-end',
  },
  menu: {
    flexGrow: 1,
    marginLeft: 30,
  },
  avatar: {
    margin: theme.spacing(1, 1.5),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  action: {
    display: 'flex',
    alignItems: 'center',
    '& > div:not(:first-child)': {
      marginLeft: theme.spacing(2),
    },
  },
}));

export function HeaderSection(props: HeaderSectionProps) {
  const {
    sticked = false,
    scrollTarget,
    variant,
    action,
    nav,
    className,
    children,
    ...rest
  } = props;
  const theme = useTheme();
  const isTransparent = variant === 'transparent';
  const classes = useStyles();
  const appBar = (fixed = false) => (
    <AppBar
      {...isTransparent
        ? { elevation: fixed ? 2 : 0, color: 'transparent' }
        : {}
      }
      position={sticked ? 'fixed' : 'static'}
      className={clsx(fixed && isTransparent && classes.transparent, className)}
      {...rest}
    >
      <Toolbar>
        <RivalTechLogo
          primaryColor={
            isTransparent
              ? theme.palette.primary.main
              : theme.palette.primary.contrastText}
          secondaryColor={
            isTransparent
              ? theme.palette.primary.contrastText
              : theme.palette.primary.light}
          height="40px"
        />
        <div className={classes.menu}>
          {nav && (
            <nav>
              {React.Children.map(
                ReactIs.isFragment(nav)
                  ? nav.props.children
                  : nav,
                button => {
                  if (React.isValidElement(button)) {
                    return React.cloneElement(button, { className: classes.menuButton } as any);
                  }
                  return button;
                },
              )}
            </nav>
          )}
        </div>
        {action && (
          <div className={classes.action}>
            {React.Children.map(action,
              child => (<div>{child}</div>),
            )}
          </div>
        )}
      </Toolbar>
    </AppBar>
  );
  return (
    <>
      {sticked
        ? (
          <>
            <HideOnScroll {...props}>
              {appBar}
            </HideOnScroll>
            <Toolbar />
          </>
        )
        : appBar()
      }
      {children}
    </>
  );
}

export default HeaderSection;