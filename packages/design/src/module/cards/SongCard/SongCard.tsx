import React, {ReactNode} from 'react';
import clsx from 'clsx';
import Card, { CardProps } from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardMedia from '@material-ui/core/CardMedia';
import CardActionArea from '@material-ui/core/CardActionArea';
import { ControlPlayPauseButton, ControlPlayPauseButtonProps } from '../../buttons/ControlPlayPauseButton';
import { makeStyles, createStyles } from '../../styles';
import { Song, SongArtist, SongArtistAlbum } from '../../data/typings';

const useStyles = makeStyles(theme => createStyles({
  root: {
    minWidth: 275,
    maxWidth: 800,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  details: {
    display: 'flex',
    alignItems: 'stretch',
  },
  content: {
    flex: 1,
  },
  cover: {
    width: 200,
    backgroundColor: '#333',
    position: 'relative',
    display: 'flex',
  },
  media: {
    width: '100%',
    flex: 1,
  },
  action: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    paddingRight: 30,
  },
  playButtonContainer: {
    position: 'absolute',
    top: '50%',
    right: -50,
    zIndex: 99,
    transform: `translate(-50%, -50%)`,
    display: 'inline-block',
    background: theme.palette.background.default,
    borderRadius: 100,
    border: `1px solid ${theme.palette.divider}`,
  },
  playButton: {
    fontSize: 45,
  },
}));

export interface SongCardProps extends CardProps, Pick<ControlPlayPauseButtonProps, 'onModeChange'> {
  playing?: boolean;
  song: Pick<Song, 'title' | 'id'>
    & { artist: Pick<SongArtist, 'title' | 'id'> }
    & { album?: Pick<SongArtistAlbum, 'title' | 'id'> | null };
  action?: ReactNode;
}

export function SongCard(props: SongCardProps) {
  const { song, className, action, playing, ...rest } = props;
  const classes = useStyles();
  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardActionArea component="div" className={classes.details}>
        <CardContent className={classes.content}>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            {song.artist.title}
          </Typography>
          <Typography variant="h5" component="h2">
            {song.title}
          </Typography>
          {song.album && (
            <Typography className={classes.pos} color="textSecondary">
              {song.album.title}
            </Typography>
          )}
          {/*
          <Typography variant="body2" component="p">
            well meaning and kindly.
            <br />
            {'"a benevolent smile"'}
          </Typography>
          */}
        </CardContent>
        <div className={classes.action}>
          {action}
          <div className={classes.playButtonContainer}>
            <ControlPlayPauseButton
              playingPulse
              mode={playing != null ? playing ? 'playing' : 'paused' : undefined}
              className={classes.playButton}
              onMouseDown={event => event.stopPropagation()}
            />
          </div>
        </div>
        <div className={classes.cover}>
          <CardMedia
            className={classes.media}
            image={`https://source.unsplash.com/400x300/?music&${song.title}`}
            title={song.title}
          />
        </div>
      </CardActionArea>
    </Card>
  );
}

export default SongCard;