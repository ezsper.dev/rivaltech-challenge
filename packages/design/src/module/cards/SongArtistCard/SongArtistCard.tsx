import React from 'react';
import Card from '@material-ui/core/Card';
import Avatar from '@material-ui/core/Avatar';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActionArea from '@material-ui/core/CardActionArea';
import { ControlPlayPauseButton } from '../../buttons/ControlPlayPauseButton';
import { makeStyles, createStyles } from '../../styles';

const useStyles = makeStyles(theme => createStyles({
  root: {
    display: 'inline-block',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 12,
  },
  details: {
    display: 'flex',
    alignItems: 'stretch',
  },
  content: {
    flex: 1,
  },
  picture: {
    position: 'relative',
  },
  title: {
    marginTop: theme.spacing(2),
    width: 200,
  },
  media: {
    width: '100%',
    flex: 1,
  },
  playButtonContainer: {
    position: 'absolute',
    bottom: 5,
    right: 5,
    background: theme.palette.background.default,
    borderRadius: 100,
    border: `1px solid ${theme.palette.divider}`,
  },
  playButton: {
    fontSize: 45,
  },
  avatar: {
    width: 200,
    height: 200,
  },
}));

export function SongArtistCard() {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardActionArea component="div" className={classes.details}>
        <CardContent className={classes.content}>
          <div className={classes.picture}>
            <Avatar className={classes.avatar} src={`https://source.unsplash.com/400x300/?music`} />
            <div className={classes.playButtonContainer}>
              <ControlPlayPauseButton
                playingPulse
                className={classes.playButton}
                onMouseDown={event => event.stopPropagation()}
              />
            </div>
          </div>
          <Typography className={classes.title} align="center" variant="h6" component="h2">
            Dua Lipa
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default SongArtistCard;