import React, { ReactNode } from 'react';
import { FormattedDate } from 'react-intl';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Typography from '@material-ui/core/Typography';
import PostOwnerAvatar from '../../data-display/PostOwnerAvatar';
import { createStyles, makeStyles } from '../../styles';
import { Comment, PostOwner } from '../../data/typings';

export type CommentType = Pick<Comment, 'content' | 'publishedAt'>
  & { author: Pick<PostOwner, 'displayName' | 'avatarSrc' | 'initials'> };

export interface CommentCardProps {
  comment: CommentType;
  cardHeaderActions?: ReactNode;
}

const useStyles = makeStyles(theme =>
  createStyles({
    /* Styles applied to the root element. */
    root: {
      maxWidth: 645,
    },
    media: {
      height: 140,
    },
  }),
);

export const CommentCard = (props: CommentCardProps) => {
  const {
    comment,
    cardHeaderActions,
  } = props;

  const { author } = comment;

  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={<PostOwnerAvatar owner={author} />}
        action={cardHeaderActions}
        title={author.displayName}
        subheader={(
          <FormattedDate
            year="numeric"
            month="long"
            day="2-digit"
            value={comment.publishedAt}
          />
        )}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {comment.content}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default CommentCard;