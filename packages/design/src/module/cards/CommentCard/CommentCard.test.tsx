/// <reference types="@types/jest" />
import React, { ReactNode } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import CommentCard from './CommentCard';
import DefaultThemeProvider from '../../styles/DefaultThemeProvider';
import { commentMocks } from '../../data/mockups';

const commentMock = commentMocks.eq(0);

const withContext = (component: ReactNode) => (
  <DefaultThemeProvider>
    <IntlProvider locale="en">
      {component}
    </IntlProvider>
  </DefaultThemeProvider>
);

describe('default', () => {
  test('renders correctly', () => {
    const { asFragment } = render(withContext(
      <CommentCard comment={commentMock} />,
    ));
    expect(asFragment()).toMatchSnapshot();
  });
});