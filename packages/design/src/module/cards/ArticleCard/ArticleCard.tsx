import React, {Children, ReactNode} from 'react';
import clsx from 'clsx';
import { FormattedDate } from 'react-intl';
import Card, { CardProps } from '@material-ui/core/Card';
import { ButtonBaseTypeMap } from '@material-ui/core/ButtonBase';
import CardActionArea, { CardActionAreaProps } from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { AvatarProps } from "@material-ui/core/Avatar";
import Rating from '@material-ui/lab/Rating';
import CommentCountMessage from '../../data-display/CommentCountMessage';
import { makeStyles, createStyles } from '../../styles';
import { Post, PostOwner, PostCategory, PostCounters } from '../../data/typings';
import StopTouchPropagation from '../../utils/StopTouchPropagation';
import PostOwnerCardHeader from '../../data-display/PostOwnerCardHeader';

export interface ArticleCardProps<
  D extends React.ElementType = ButtonBaseTypeMap['defaultComponent'],
  P = {}> extends CardProps {
  post: Pick<Post, 'title' | 'excerpt' | 'publishedAt' | 'thumbnailSrc' | 'ratingScore'>
    & { category: Pick<PostCategory, 'title'> }
    & { author: Pick<PostOwner, 'avatarSrc' | 'initials' | 'displayName'> }
    & { counters: Pick<PostCounters, 'comments'> };
  landscape?: boolean;
  action?: ReactNode;
  CardActionAreaProps?: CardActionAreaProps<D, P>;
  AvatarProps?: AvatarProps;
}

const useStyles = makeStyles(theme => createStyles({
  /* Styles applied to the root element. */
  root: ({ landscape }) => ({
    ...(landscape
      ? { display: 'flex', width: 600 }
      : { maxWidth: 345 })
  }),
  media: ({ landscape }: ArticleCardProps<any, any>) => ({
    ...(landscape
      ? { width: 151 }
      : { height: 140 }),
  }),
  landscapeArea: {
    display: 'flex',
    alignItems: 'stretch',
  },
  trendings: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  separator: {
    display: 'inline-block',
    margin: '0 .3em',
    '&::after': {
      content: '\\00B7',
    },
  },
  landscapeLeft: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
  },
}));

export const ArticleCard = <
  D extends React.ElementType = ButtonBaseTypeMap['defaultComponent'],
  P = {}>(props: ArticleCardProps<D, P>) => {
  const {
    className,
    post,
    action,
    AvatarProps,
    CardActionAreaProps,
    landscape = false,
    ...rest
  } = props;
  const classes = useStyles(props);

  const cardMedia = (
    <CardMedia
      className={classes.media}
      image={post.thumbnailSrc || undefined}
      title={post.title}
    />
  );

  const cardContent = (
    <CardContent>
      <Typography gutterBottom variant="subtitle2" component="h6">
        {post.category.title}
      </Typography>
      <Typography gutterBottom variant="h5" component="h2">
        {post.title}
      </Typography>
      <Typography variant="body2" color="textSecondary" component="p">
        {post.excerpt}
      </Typography>
      <Typography variant="caption" component="div" color="textSecondary" className={classes.trendings}>
        {(
          <>
            <Rating
              size="small"
              defaultValue={post.ratingScore/2}
              precision={0.5}
              readOnly
            />
            {<span className={classes.separator} />}
            <CommentCountMessage value={post.counters.comments} />
          </>
        )}
      </Typography>
    </CardContent>
  );

  const cardHeader = (
    <PostOwnerCardHeader
      {...AvatarProps}
      owner={post.author}
      subheader={(
        <FormattedDate
          year="numeric"
          month="long"
          day="2-digit"
          value={post.publishedAt}
        />
      )}
      action={
        Children.map(action, child => (
          <StopTouchPropagation>
            {child}
          </StopTouchPropagation>
        ))
      }
    />
  );

  let inner: ReactNode;

  if (landscape) {
    inner = (
      <>
        <div className={classes.landscapeLeft}>
          {cardContent}
          {cardHeader}
        </div>
        {cardMedia}
      </>
    );
  } else {
    inner = (
      <>
        {cardMedia}
        {cardContent}
        {cardHeader}
      </>
    );
  }

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <CardActionArea
        component="a"
        {...CardActionAreaProps as any}
        className={clsx(
          CardActionAreaProps && CardActionAreaProps.className,
          landscape && classes.landscapeArea,
        )}
      >
        {inner}
      </CardActionArea>
    </Card>
  );
};

export default ArticleCard;