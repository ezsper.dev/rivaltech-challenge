/// <reference types="@types/jest" />
import React, { ReactNode } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import ArticleCard from './ArticleCard';
import DefaultThemeProvider from '../../styles/DefaultThemeProvider';
import { postMocks } from '../../data/mockups';

const withContext = (component: ReactNode) => (
  <DefaultThemeProvider>
    <IntlProvider locale="en">
      {component}
    </IntlProvider>
  </DefaultThemeProvider>
);

test('renders correctly', () => {
  const { asFragment } = render(withContext(
    <ArticleCard post={postMocks.eq(0)} />,
  ));
  expect(asFragment()).toMatchSnapshot();
});