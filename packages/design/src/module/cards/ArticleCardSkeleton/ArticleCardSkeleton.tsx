import React, {ReactNode} from 'react';
import clsx from 'clsx';
import Card, { CardProps } from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Skeleton from '@material-ui/lab/Skeleton';
import CardHeader from "@material-ui/core/CardHeader";
import { makeStyles } from '../../styles';

export interface ArticleCardSkeletonProps extends CardProps {
  landscape?: boolean;
}

const useStyles = makeStyles({
  /* Styles applied to the root element. */
  root: ({ landscape }: ArticleCardSkeletonProps) => ({
    ...(landscape
      ? { display: 'flex', alignItems: 'stretch', width: 600 }
      : { maxWidth: 345 })
  }),
  media: ({ landscape }: ArticleCardSkeletonProps) => ({
    ...(landscape
      ? { width: 151 }
      : { height: 140 }),
  }),
  landscapeLeft: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
  },
});

export const ArticleCardSkeleton = (props: ArticleCardSkeletonProps) => {
  const { landscape = false, className, ...rest } = props;
  const classes = useStyles(props);

  const cardMedia = landscape
    ? <Skeleton animation="wave" height="auto" variant="rect" className={classes.media} />
    : <Skeleton animation="wave" variant="rect" className={classes.media} />;
  const cardContent = (
    <CardContent>
      <Skeleton height={30} />
      <Skeleton height={30} width="60%" style={{ marginBottom: 12 }} />
      <Skeleton animation="wave" height={18} style={{ marginBottom: 6 }} />
      <Skeleton animation="wave" height={18} width="90%" style={{ marginBottom: 6 }} />
      <Skeleton animation="wave" height={18} width="80%" />
    </CardContent>
  );
  const cardHeader = (
    <CardHeader
      avatar={
        <Skeleton animation="wave" variant="circle" width={40} height={40} />
      }
      title={<Skeleton animation="wave" height={15} width="80%" style={{ marginBottom: 6 }} />}
      subheader={<Skeleton animation="wave" height={15} width="40%" />}
    />
  );

  let inner: ReactNode;

  if (landscape) {
    inner = (
      <>
        <div className={classes.landscapeLeft}>
          {cardContent}
          {cardHeader}
        </div>
        {cardMedia}
      </>
    );
  } else {
    inner = (
      <>
        {cardMedia}
        {cardContent}
        {cardHeader}
      </>
    );
  }

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      {inner}
    </Card>
  );
};

export default ArticleCardSkeleton;