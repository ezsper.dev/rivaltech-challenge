/// <reference types="@types/jest" />
import React, { ReactNode } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import ArticleCardSkeleton from './ArticleCardSkeleton';
import DefaultThemeProvider from '../../styles/DefaultThemeProvider';

const withContext = (component: ReactNode) => (
  <DefaultThemeProvider>
    <IntlProvider locale="en">
      {component}
    </IntlProvider>
  </DefaultThemeProvider>
);

describe('default', () => {
  test('renders correctly', () => {
    const { asFragment } = render(withContext(
      <ArticleCardSkeleton />,
    ));
    expect(asFragment()).toMatchSnapshot();
  });
});

describe('landscape', () => {
  test('renders correctly', () => {
    const { asFragment } = render(withContext(
      <ArticleCardSkeleton landscape />,
    ));
    expect(asFragment()).toMatchSnapshot();
  });
});