import React, { forwardRef, SVGProps } from 'react';
import { createStyles, makeStyles } from '../../styles';
import clsx from 'clsx';

declare global {
  namespace JSX {
    interface IntrinsicElements {
      descr: React.DetailedHTMLProps<React.HTMLAttributes<HTMLElement>, HTMLElement>;
    }
  }
}

export interface RivalTechLogoProps extends SVGProps<SVGSVGElement> {
  primaryColor?: string;
  secondaryColor?: string;
  title?: string;
  descr?: string;
}

const useStyles = makeStyles(theme => createStyles({
  root: {},
  primaryColor: (props: RivalTechLogoProps) => ({
    fill: props.primaryColor || theme.palette.primary.dark,
  }),
  secondaryColor: props => ({
    fill: props.secondaryColor || theme.palette.primary.dark,
  }),
}));

export const RivalTechLogo = forwardRef<SVGSVGElement, RivalTechLogoProps>(
  function Logo(props, ref) {
    const {
      className,
      primaryColor,
      secondaryColor,
      title: _title,
      descr: _descr,
      ...rest
    } = props;
    const classes = useStyles(props);
    return (
      <svg
        viewBox="0 0 144.4 105.1"
        {...rest}
        className={clsx(classes.root, className)}
        ref={ref}>
        {_title && <title>{_title}</title>}
        {_descr && <descr>{_descr}</descr>}
        {props.children}
        <path className={classes.primaryColor}
              d="M142.08,53.09c-1.15,2.51-3.55,2.69-5.82,3.21-34.7,7.84-69.38,15.78-104.12,23.48-5.75,1.27-10.55,3.51-14.58,7.87-4.5,4.9-9.39,9.44-14.12,14.12-.69.68-1.36,1.68-2.49,1.27S0,101.41,0,100.46Q0,68,0,35.54c0-3.87,2.88-4.81,6-5.5,32.6-7.33,65.22-14.59,97.8-22,11.09-2.55,22.38-4.3,33.2-8h5c1,3.58.48,7.24.45,10.85C142.3,24.93,143,39,142.08,53.09Z"/>
        <path className={classes.secondaryColor}
              d="M66.45,46.09c1.47-5.29,2.93-9.07,3.51-13,.85-5.65,4.17-7.34,9.78-7C76.45,36.81,73.15,47.34,70,57.9c-.78,2.6-3.06,2.24-4.82,2.82-2.27.74-2.33-1.44-2.77-2.66C59.51,50.27,56.76,42.42,54,34.59c-.54-1.51-1.73-3.49,1.22-3.49,2.23,0,5.17-3.35,6.69,1C63.35,36.3,64.66,40.57,66.45,46.09Z"/>
        <path className={classes.secondaryColor}
              d="M108.89,50.9c0-10.22.08-19.67,0-29.11,0-4.67,3.75-3.47,6.05-3.93,3.41-.69,1.82,2.34,1.86,3.65.18,5.8.14,11.61,0,17.41,0,2.32.3,3.37,3,2.52,3.14-1,6.44-1.45,9.68-2.15,1.36,6.65,1.36,6.67-5.15,8.17C119.38,48.6,114.37,49.68,108.89,50.9Z"/>
        <path className={classes.secondaryColor}
              d="M41.86,49.51c0-3.82,0-7.64,0-11.45a5.64,5.64,0,0,1,6.69-5.55c1.5.25,1.26,1.26,1.26,2.19q0,13.44,0,26.89c0,4.54-4,3.28-6.27,3.78C40.71,66,42,63.19,41.93,62,41.75,57.81,41.86,53.66,41.86,49.51Z"/>
        <path className={classes.secondaryColor}
              d="M34.18,60.52c-2.15-3.1-3.53-5.68,0-9.2,2.89-2.86,2.89-7.1.49-10.63-2.22-3.28-5.58-3.85-9.32-2.94-3.23.79-6.47,1.59-9.75,2.11-2.21.36-3.11,1.27-3.08,3.58q.14,13,0,26c0,2.25.72,2.81,2.95,2.5,3.65-.5,5.84-1.81,5.12-6a33.5,33.5,0,0,1,0-5.49,1.78,1.78,0,0,1,1.35-1.85c1-.27,1.53.41,1.95,1.15.57,1,1.08,2.06,1.63,3.09C28.9,69.11,28.9,69.11,36,67.15c.48-.13.93-.32,1.8-.63C36.4,64.2,35.41,62.28,34.18,60.52ZM22.4,51.78c-2.74.5-1.43-1.86-1.76-2.74-.53-3.89,2.26-3.73,4.47-4.08,1.74-.27,3.09.52,3,2.4C27.94,50.91,25,51.32,22.4,51.78Z"/>
        <path className={classes.secondaryColor}
              d="M104.29,48.22c-2.94-7.58-6-15.13-9-22.66-.51-1.26-.47-3.29-2.7-2.82-2,.43-4.44.18-5.33,2.87-3.54,10.65-7.12,21.28-10.92,32.61,4.19-1.31,8.4-.35,9.79-5.85.77-3.08,8.7-3,10.85-.42,2.34,2.86,4.9.35,7.16-.15S104.74,49.38,104.29,48.22ZM88.43,43.76C89.35,40.94,90.1,38.63,91,36,94.62,43.05,94.62,43.05,88.43,43.76Z"/>
      </svg>
    );
  },
);

export default RivalTechLogo;