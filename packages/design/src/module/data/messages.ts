import { defineMessages } from 'react-intl';

const prefix = '@dxco/design';

export const messages = defineMessages({
  comments: {
    id: `${prefix}/comments`,
    description: 'Message to showing comment counts.',
    defaultMessage: `{count, number} {count, plural,
                      one {Comment}
                      other {Comments}`,
  },
});

export default messages;
