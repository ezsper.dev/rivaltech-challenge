import sample from 'lodash/sample';
import sampleSize from 'lodash/sampleSize';
import {
  User,
  PostOwner,
  Post,
  PostCategory,
  PostCounters,
  Comment,
  Song,
  SongArtist,
  SongArtistAlbum,
  SongPlaylist,
  SongPlaylistItem,
} from './typings';

export class Mocks<T> {

  readonly items: T[];

  constructor(items: T[]) {
    this.items = items;
  }

  eq(index: number) {
    return this.items[index];
  }

  sample() {
    return sample(this.items);
  }

  samples(length: number) {
    return sampleSize(this.items, length);
  }

  toArray() {
    return this.items.slice(0);
  }

  *[Symbol.iterator]() {
    for (const item of this.items) {
      yield item;
    }
  }

}

export type CommentMock = Pick<Comment, 'content' | 'publishedAt'>
  & { author: PostOwnerMock };
export type UserMock = Pick<User, 'avatarSrc' | 'initials' | 'displayName'>;
export type PostOwnerMock = Pick<PostOwner, 'avatarSrc' | 'initials' | 'displayName'>;
export type PostCategoryMock = Pick<PostCategory, 'id' | 'title'>;
export type PostCountersMock = Pick<PostCounters, 'comments' | 'raters'>;
export type PostMock = Pick<Post, 'title' | 'subheading' | 'content' | 'excerpt' | 'publishedAt' | 'ratingScore' | 'link' | 'thumbnailSrc'>
  & { author: PostOwnerMock }
  & { category: PostCategoryMock }
  & { counters: PostCountersMock };
export type SongArtistMock = Pick<SongArtist, 'id' | 'title'>;
export type SongArtistAlbumMock = Pick<SongArtistAlbum, 'id' |'title'>
  & { artist: SongArtistMock };
export type SongMock = Pick<Song, 'id' | 'title'>
  & { artist: SongArtistMock }
  & { album?: SongArtistAlbumMock | null };
export type SongPlaylistItemMock = Pick<SongPlaylistItem, 'id' | 'createdAt'> & {
  song: SongMock;
};


export type SongPlaylistMock = Pick<SongPlaylist, 'id' | 'title'>;

export const userMocks = new Mocks<UserMock>([
  {
    displayName: 'Ezequiel S. Pereira',
    initials: 'EP',
    avatarSrc: 'https://avatars3.githubusercontent.com/u/798804?v=4',
  },
]);


export const postCategoryMocks = new Mocks<PostCategoryMock>([
  { id: '1', title: 'Software Engineering' },
]);

export const postMocks = new Mocks<PostMock>([
  {
    link: '#',
    title: 'Hello World',
    subheading: 'My Subheading',
    content:
      `<figure><img src="https://images.unsplash.com/photo-1556911220-bff31c812dba?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1670&q=80" /></figure>It is a <b>long</b> established fact that a reader will be distracted by the readable content 
      of a page when looking at its layout. The point of using Lorem Ipsum is that it has a
      more-or-less normal distribution of letters, as opposed to using 'Content here, content
      here', making it look like readable English.`,
    publishedAt: new Date('2020-02-10T23:38:16.980Z').toDateString(),
    excerpt: `It is a long established fact that a reader will be distracted by the readable content 
      of a page when lo oking at its layout.`,
    thumbnailSrc: 'https://source.unsplash.com/collection/190727/600x300',
    author: userMocks.eq(0),
    category: postCategoryMocks.eq(0),
    ratingScore: 7,
    counters: {
      comments: 2,
      raters: 54,
    },
  },
]);


export const commentMocks = new Mocks<CommentMock>([
  {
    content: 'This is a comment',
    publishedAt: new Date('2020-02-10T23:38:16.980Z').toDateString(),
    author: userMocks.eq(0),
  },
]);

export const songArtistMocks = new Mocks<SongArtistMock>([
  {
    id: 'mock1',
    title: 'Vampire Weekend',
  },
]);
export const songArtistAlbumMocks = new Mocks<SongArtistAlbumMock>([
  {
    id: 'mock1',
    title: 'Vampire Weekend',
    artist: songArtistMocks.eq(0),
  },
]);

export const songMocks = new Mocks<SongMock>([
  {
    id: 'mock1',
    title: 'Walcott',
    album: songArtistAlbumMocks.eq(0),
    artist: songArtistMocks.eq(0),
  },
  {
    id: 'mock2',
    title: 'Walcott2',
    album: songArtistAlbumMocks.eq(0),
    artist: songArtistMocks.eq(0),
  },
  {
    id: 'mock3',
    title: 'Walcott3',
    album: songArtistAlbumMocks.eq(0),
    artist: songArtistMocks.eq(0),
  },
]);

export const songPlaylistMocks = new Mocks<SongPlaylistMock>([
  {
    id: 'mock1',
    title: 'Playlist 1',
  },
  {
    id: 'mock2',
    title: 'Playlist 2',
  },
  {
    id: 'mock3',
    title: 'Playlist 4',
  },
]);

export const songPlaylistItemMocks = new Mocks<SongPlaylistItemMock>([
  {
    id: 'mock1',
    song: songMocks.eq(0),
    createdAt: '2020-02-20',
  },
  {
    id: 'mock2',
    song: songMocks.eq(1),
    createdAt: '2020-02-20',
  },
  {
    id: 'mock3',
    song: songMocks.eq(2),
    createdAt: '2020-02-20',
  },
]);
