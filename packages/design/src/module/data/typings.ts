export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: any }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any;
};

export type AddPlaylistsToSongInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The song */
  song: Scalars['ID'];
  /** The song playlists */
  songPlaylists: Array<Scalars['ID']>;
};

export type AddSongsToPlaylistInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The song playlist id */
  songPlaylist: Scalars['ID'];
  /** The items to compose playlist */
  songs: Array<Scalars['ID']>;
};

export type Auth = {
  type: AuthType;
  viewerId?: Maybe<Scalars['ID']>;
  viewer?: Maybe<Viewer>;
};

export type AuthAnonymous = Node & Auth & {
  __typename?: 'AuthAnonymous';
  id: Scalars['ID'];
  type: AuthType;
  viewerId?: Maybe<Scalars['ID']>;
  viewer?: Maybe<Viewer>;
};

export enum AuthType {
  Anonymous = 'ANONYMOUS',
  User = 'USER'
}

export type AuthUser = Auth & Node & {
  __typename?: 'AuthUser';
  type: AuthType;
  viewerId: Scalars['ID'];
  viewer: User;
  id: Scalars['ID'];
  _userId: Scalars['String'];
  userId: Scalars['ID'];
  user: User;
};

export type Comment = {
  __typename?: 'Comment';
  _id: Scalars['String'];
  id: Scalars['ID'];
  content: Scalars['String'];
  authorId: Scalars['ID'];
  author: PostOwner;
  _postId: Scalars['String'];
  postId: Scalars['ID'];
  post: Post;
  createdAt: Scalars['DateTime'];
  publishedAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type CommentConnection = {
  __typename?: 'CommentConnection';
  edges: Array<CommentEdge>;
  nodes: Array<Comment>;
  totalCount: Scalars['Int'];
  info: PageInfo;
};

export type CommentEdge = {
  __typename?: 'CommentEdge';
  cursor: Scalars['ID'];
  node: Comment;
};

export type CommentInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  content: Scalars['String'];
};

export type CommentPayload = Payload & {
  __typename?: 'CommentPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  _commentId: Scalars['String'];
  commentId: Scalars['ID'];
  comment: Comment;
};


export type Input = {
  clientMutationId?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  createComment: CommentPayload;
  updateComment: CommentPayload;
  removeComment: Payload;
  createPostCategory: PostCategoryPayload;
  updatePostCategory: PostCategoryPayload;
  removePostCategory: Payload;
  createPost: PostPayload;
  updatePost: PostPayload;
  removePost: Payload;
  rate: PostRatePayload;
  createSongPlaylist: SongPlaylistPayload;
  addSongsToPlaylist: SongPlaylistPayload;
  removeSongsFromPlaylist: SongPlaylistPayload;
  reorderSongsOfPlaylist: SongPlaylistPayload;
  addPlaylistsToSong: SongPayload;
  removePlaylistsFromSong: SongPayload;
};


export type MutationCreateCommentArgs = {
  input: CommentInput;
  postId: Scalars['ID'];
};


export type MutationUpdateCommentArgs = {
  input: CommentInput;
  commentId: Scalars['ID'];
};


export type MutationRemoveCommentArgs = {
  input?: Maybe<Input>;
  commentId: Scalars['String'];
};


export type MutationCreatePostCategoryArgs = {
  input: PostCategoryInput;
};


export type MutationUpdatePostCategoryArgs = {
  input: PostCategoryInput;
  postId: Scalars['String'];
};


export type MutationRemovePostCategoryArgs = {
  input?: Maybe<Input>;
  postId: Scalars['String'];
};


export type MutationCreatePostArgs = {
  input: PostInput;
};


export type MutationUpdatePostArgs = {
  input: PostInput;
  id: Scalars['ID'];
};


export type MutationRemovePostArgs = {
  input?: Maybe<Input>;
  postId: Scalars['String'];
};


export type MutationRateArgs = {
  input: PostRateInput;
};


export type MutationCreateSongPlaylistArgs = {
  input: SongPlaylistInput;
};


export type MutationAddSongsToPlaylistArgs = {
  input: AddSongsToPlaylistInput;
};


export type MutationRemoveSongsFromPlaylistArgs = {
  input: RemoveSongsFromPlaylistInput;
};


export type MutationReorderSongsOfPlaylistArgs = {
  input: ReorderSongsOfPlaylistInput;
};


export type MutationAddPlaylistsToSongArgs = {
  input: AddPlaylistsToSongInput;
};


export type MutationRemovePlaylistsFromSongArgs = {
  input: RemovePlaylistsFromSongInput;
};

export type Node = {
  id: Scalars['ID'];
};

export type PageInfo = {
  __typename?: 'PageInfo';
  endCursor?: Maybe<Scalars['ID']>;
  startCursor?: Maybe<Scalars['ID']>;
  hasPrevPage: Scalars['Boolean'];
  hasNextPage: Scalars['Boolean'];
};

export type Payload = {
  clientMutationId?: Maybe<Scalars['String']>;
};

export type Post = Node & {
  __typename?: 'Post';
  id: Scalars['ID'];
  _id: Scalars['String'];
  title: Scalars['String'];
  name: Scalars['String'];
  link: Scalars['String'];
  subheading?: Maybe<Scalars['String']>;
  thumbnailSrc?: Maybe<Scalars['String']>;
  posterSrc?: Maybe<Scalars['String']>;
  content: Scalars['String'];
  excerpt?: Maybe<Scalars['String']>;
  authorId: Scalars['ID'];
  author: PostOwner;
  counters: PostCounters;
  ratingScore: Scalars['Int'];
  viewerRatingScore?: Maybe<Scalars['Int']>;
  _categoryId: Scalars['ID'];
  categoryId: Scalars['ID'];
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  publishedAt: Scalars['DateTime'];
  category: PostCategory;
};


export type PostExcerptArgs = {
  resolve?: Maybe<Scalars['Boolean']>;
};

export type PostCategory = Node & {
  __typename?: 'PostCategory';
  id: Scalars['ID'];
  _id: Scalars['String'];
  name: Scalars['String'];
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  counters: PostCategoryCounters;
};

export type PostCategoryConnection = {
  __typename?: 'PostCategoryConnection';
  edges: Array<PostCategoryEdge>;
  nodes: Array<PostCategory>;
  totalCount: Scalars['Int'];
  info: PageInfo;
};

export type PostCategoryCounters = {
  __typename?: 'PostCategoryCounters';
  _id: Scalars['String'];
  id: Scalars['ID'];
  posts: Scalars['Int'];
};

export type PostCategoryEdge = {
  __typename?: 'PostCategoryEdge';
  cursor: Scalars['ID'];
  node: Scalars['ID'];
};

export type PostCategoryInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The category title */
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
};

export type PostCategoryPayload = Payload & {
  __typename?: 'PostCategoryPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  _categoryId: Scalars['String'];
  categoryId: Scalars['ID'];
  category: PostCategory;
};

export type PostConnection = {
  __typename?: 'PostConnection';
  edges: Array<PostEdge>;
  nodes: Array<Post>;
  totalCount: Scalars['Int'];
  info: PageInfo;
};

export type PostCounters = {
  __typename?: 'PostCounters';
  _id: Scalars['String'];
  id: Scalars['ID'];
  comments: Scalars['Int'];
  raters: Scalars['Int'];
  ratersOnScore1: Scalars['Int'];
  ratersOnScore2: Scalars['Int'];
  ratersOnScore3: Scalars['Int'];
  ratersOnScore4: Scalars['Int'];
  ratersOnScore5: Scalars['Int'];
  ratersOnScore6: Scalars['Int'];
  ratersOnScore7: Scalars['Int'];
  ratersOnScore8: Scalars['Int'];
  ratersOnScore9: Scalars['Int'];
  ratersOnScore10: Scalars['Int'];
};

export type PostEdge = {
  __typename?: 'PostEdge';
  cursor: Scalars['ID'];
  node: Post;
};

export type PostInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The post title */
  title: Scalars['String'];
  subheading?: Maybe<Scalars['String']>;
  content: Scalars['String'];
  excerpt?: Maybe<Scalars['String']>;
  categoryId: Scalars['ID'];
};

export type PostOwner = {
  id: Scalars['ID'];
  displayName: Scalars['String'];
  initials: Scalars['String'];
  avatarSrc?: Maybe<Scalars['String']>;
};

export type PostPayload = Payload & {
  __typename?: 'PostPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  _postId: Scalars['String'];
  postId: Scalars['ID'];
  post: Post;
  postCounters: PostCounters;
};

export type PostRateInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  postId: Scalars['ID'];
  ratingScore?: Maybe<Scalars['Int']>;
};

export type PostRatePayload = Payload & {
  __typename?: 'PostRatePayload';
  clientMutationId?: Maybe<Scalars['String']>;
  _postId: Scalars['String'];
  postId: Scalars['ID'];
  ratingScore: Scalars['Float'];
};

export type Query = {
  __typename?: 'Query';
  me: Auth;
  comment: Post;
  comments: CommentConnection;
  node: Node;
  postCategory: PostCategory;
  postCategories: PostCategoryConnection;
  post: Post;
  postByShortId: Post;
  posts: PostConnection;
  viewerPostRatingScore?: Maybe<Scalars['Int']>;
  songArtistAlbum: SongArtistAlbum;
  songArtistAlbums: SongArtistAlbumConnection;
  artist: SongArtist;
  artists: SongArtistConnection;
  songPlaylistItem: SongPlaylist;
  songPlaylist: SongPlaylist;
  songPlaylistByShortId: SongPlaylist;
  songPlaylists: SongPlaylistConnection;
  mySongPlaylists: SongPlaylistConnection;
  song: Song;
  songs: SongConnection;
};


export type QueryCommentArgs = {
  commentId: Scalars['String'];
};


export type QueryCommentsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['ID']>;
  after?: Maybe<Scalars['ID']>;
  postId: Scalars['ID'];
};


export type QueryNodeArgs = {
  id: Scalars['ID'];
};


export type QueryPostCategoryArgs = {
  postId: Scalars['String'];
};


export type QueryPostCategoriesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['ID']>;
  after?: Maybe<Scalars['ID']>;
};


export type QueryPostArgs = {
  id: Scalars['ID'];
};


export type QueryPostByShortIdArgs = {
  _id: Scalars['String'];
};


export type QueryPostsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['ID']>;
  after?: Maybe<Scalars['ID']>;
  categories?: Maybe<Array<Scalars['ID']>>;
  searchTerm?: Maybe<Scalars['String']>;
};


export type QueryViewerPostRatingScoreArgs = {
  postId: Scalars['ID'];
};


export type QuerySongArtistAlbumArgs = {
  id: Scalars['ID'];
};


export type QuerySongArtistAlbumsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['ID']>;
  after?: Maybe<Scalars['ID']>;
  searchTerm?: Maybe<Scalars['String']>;
};


export type QueryArtistArgs = {
  id: Scalars['ID'];
};


export type QueryArtistsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['ID']>;
  after?: Maybe<Scalars['ID']>;
  searchTerm?: Maybe<Scalars['String']>;
};


export type QuerySongPlaylistItemArgs = {
  id: Scalars['ID'];
};


export type QuerySongPlaylistArgs = {
  id: Scalars['ID'];
};


export type QuerySongPlaylistByShortIdArgs = {
  _id: Scalars['String'];
};


export type QuerySongPlaylistsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['ID']>;
  after?: Maybe<Scalars['ID']>;
  searchTerm?: Maybe<Scalars['String']>;
  authorId?: Maybe<Scalars['ID']>;
};


export type QueryMySongPlaylistsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['ID']>;
  after?: Maybe<Scalars['ID']>;
  searchTerm?: Maybe<Scalars['String']>;
};


export type QuerySongArgs = {
  id: Scalars['ID'];
};


export type QuerySongsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['ID']>;
  after?: Maybe<Scalars['ID']>;
  searchTerm?: Maybe<Scalars['String']>;
};

export type RemovePlaylistsFromSongInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The song */
  song: Scalars['ID'];
  /** The song playlists */
  songPlaylists: Array<Scalars['ID']>;
};

export type RemoveSongsFromPlaylistInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The song playlist id */
  songPlaylist: Scalars['ID'];
  /** The items to compose playlist */
  songs: Array<Scalars['ID']>;
};

export type ReorderSongsOfPlaylistInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The song playlist id */
  songPlaylist: Scalars['ID'];
  /** The song items to remove from playlist */
  songItem: Array<Scalars['ID']>;
  /** The new sort order */
  sortOrder: Scalars['Int'];
};

export type Song = Node & {
  __typename?: 'Song';
  id: Scalars['ID'];
  title: Scalars['String'];
  _artistId: Scalars['ID'];
  artistId: Scalars['ID'];
  artist: SongArtist;
  _albumId?: Maybe<Scalars['ID']>;
  albumId: Scalars['ID'];
  album: SongArtistAlbum;
  sourceUrl: Scalars['String'];
  duration: Scalars['Int'];
  myPlaylists: SongPlaylistConnection;
};

export type SongArtist = Node & {
  __typename?: 'SongArtist';
  id: Scalars['ID'];
  title: Scalars['String'];
  bio: Scalars['String'];
  albums: Array<SongArtistAlbum>;
  songs: Array<Song>;
  pictureSrc?: Maybe<Scalars['String']>;
};

export type SongArtistAlbum = Node & {
  __typename?: 'SongArtistAlbum';
  id: Scalars['ID'];
  title: Scalars['String'];
  bio: Scalars['String'];
  _artistId: Scalars['ID'];
  artistId: Scalars['ID'];
  artist: SongArtist;
  songs: Array<Song>;
  coverImageSrc?: Maybe<Scalars['String']>;
};

export type SongArtistAlbumConnection = {
  __typename?: 'SongArtistAlbumConnection';
  edges: Array<SongArtistAlbumEdge>;
  nodes: Array<SongArtistAlbum>;
  info: PageInfo;
};

export type SongArtistAlbumEdge = {
  __typename?: 'SongArtistAlbumEdge';
  cursor: Scalars['ID'];
  node: SongArtistAlbum;
};

export type SongArtistConnection = {
  __typename?: 'SongArtistConnection';
  edges: Array<SongArtistEdge>;
  nodes: Array<SongArtist>;
  info: PageInfo;
};

export type SongArtistEdge = {
  __typename?: 'SongArtistEdge';
  cursor: Scalars['ID'];
  node: SongArtist;
};

export type SongConnection = {
  __typename?: 'SongConnection';
  edges: Array<SongEdge>;
  nodes: Array<Song>;
  info: PageInfo;
};

export type SongEdge = {
  __typename?: 'SongEdge';
  cursor: Scalars['ID'];
  node: Song;
};

export type SongPayload = Payload & {
  __typename?: 'SongPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  _songId: Scalars['String'];
  songId: Scalars['ID'];
  song: Song;
};

export type SongPlaylist = Node & {
  __typename?: 'SongPlaylist';
  id: Scalars['ID'];
  _id: Scalars['String'];
  title: Scalars['String'];
  items: Array<SongPlaylistItem>;
  authorId: Scalars['ID'];
  author: SongPlaylistOwner;
  privacy: Scalars['String'];
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
};

export type SongPlaylistConnection = {
  __typename?: 'SongPlaylistConnection';
  edges: Array<SongPlaylistEdge>;
  nodes: Array<SongPlaylist>;
  info: PageInfo;
};

export type SongPlaylistEdge = {
  __typename?: 'SongPlaylistEdge';
  cursor: Scalars['ID'];
  node: SongPlaylist;
};

export type SongPlaylistInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  /** The song playlist title */
  title: Scalars['String'];
  /** The items to compose playlist */
  songs: Array<Scalars['ID']>;
  /** If the playlist is public */
  privacy: Scalars['String'];
};

export type SongPlaylistItem = Node & {
  __typename?: 'SongPlaylistItem';
  id: Scalars['ID'];
  _songId: Scalars['ID'];
  _playlistId: Scalars['ID'];
  playlistId: Scalars['ID'];
  playlist: SongPlaylist;
  sortOrder: Scalars['Float'];
  songId: Scalars['ID'];
  song: Song;
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  songPlaylist: SongPlaylistItem;
};

export type SongPlaylistOwner = {
  id: Scalars['ID'];
  displayName: Scalars['String'];
  initials: Scalars['String'];
  avatarSrc?: Maybe<Scalars['String']>;
};

export type SongPlaylistPayload = Payload & {
  __typename?: 'SongPlaylistPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  _songPlaylistId: Scalars['String'];
  songPlaylistId: Scalars['ID'];
  songPlaylist: SongPlaylist;
};

export type SongSearchResult = {
  __typename?: 'SongSearchResult';
  artistResults: Array<SongArtist>;
  albumResults: Array<SongArtistAlbum>;
  songs: Array<Song>;
};

export type User = Node & Viewer & PostOwner & SongPlaylistOwner & {
  __typename?: 'User';
  id: Scalars['ID'];
  displayName: Scalars['String'];
  initials: Scalars['String'];
  avatarSrc?: Maybe<Scalars['String']>;
  _id: Scalars['String'];
};

export type Viewer = {
  id: Scalars['ID'];
  displayName: Scalars['String'];
  initials: Scalars['String'];
  avatarSrc?: Maybe<Scalars['String']>;
};
