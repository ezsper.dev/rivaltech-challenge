import React from 'react';
import { IntlProvider } from 'react-intl';
import { DefaultThemeProvider } from './module/styles/DefaultThemeProvider';

export default () => {
  return (
    <IntlProvider locale="en">
      <DefaultThemeProvider>
        The library
      </DefaultThemeProvider>
    </IntlProvider>
  );
};