# Rival Technologies Design

This package holds the design to be implemented in the app.

## Installation of the project
```bash
npm install
```

## Developing
```bash
npm run develop
```

## Structure

### module folder

all components in `./src/module` are agnostic, this means that all components have exposed props to be connected by
any state provider like Redux or GraphQL. When you execute the package `release` or `dist` command, the module folder
will be used as project root.
