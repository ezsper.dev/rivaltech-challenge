import React from 'react';
import { FooterSection } from '@rivaltech-challenge/design/sections/FooterSection';

export const Footer = () => {
  return (<FooterSection />);
};