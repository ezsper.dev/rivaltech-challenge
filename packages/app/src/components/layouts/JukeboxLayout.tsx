import React, { ReactNode } from 'react';
import { Link } from 'react-router-dom';
import { BasicLayout, BasicLayoutProps } from '@rivaltech-challenge/design/layouts/BasicLayout';
import { LoadingBox } from '@rivaltech-challenge/design/feedback/LoadingBox';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import { useAuth } from '../context/AuthContext';
import { useMySongPlaylistsQuery } from '../../graphql/queries/MySongPlaylistsQuery';

export interface JukeboxLayoutProps extends BasicLayoutProps {
  children: NonNullable<ReactNode>;
}

function MyPlaylists() {
  const { data, loading } = useMySongPlaylistsQuery();

  return (
    <LoadingBox
      loading={loading}
      whileLoading="Loading playlists"
      style={{ width: '100%' }}>
        {data && data.mySongPlaylists.nodes.map((node) => (
          <ListItem component={Link} button key={node.id} to={`/music/playlists/${node._id}`}>
            <ListItemText primary={node.title} />
          </ListItem>
        ))}
        {data && data.mySongPlaylists.nodes.length === 0 && (
          <ListItem>
            <ListItemText secondary="No playlists created, go ahead and create one! :)" />
          </ListItem>
        )}
    </LoadingBox>
  );
}

export function JukeboxLayout(props: JukeboxLayoutProps) {
  const { children, ...rest } = props;
  const auth = useAuth();
  return (
    <BasicLayout
      {...rest}
      style={{ background: '#f9f9f9' }}
    >
      <Grid container style={{ flex: 1 }}>
        <Grid item sm={2} md={3} lg={2} style={{ background: 'white', borderRight: '1px solid #eee' }}>
          <Box p={0} display="flex" justifyContent="strech">
            <List
              component="nav"
              aria-labelledby="nested-list-subheader"
              subheader={
                <ListSubheader component="div" id="nested-list-subheader">
                  Playlists
                </ListSubheader>
              }
              style={{ width: '100%' }}
            >
              {auth.me.viewer
                  ? <MyPlaylists />
                  : (
                  <ListItem>
                    <ListItemText secondary="You must login in order to see your playlists." />
                  </ListItem>
                )}
            </List>
          </Box>
        </Grid>
        <Grid item sm={10} md={9} lg={10}>
          <Box mt={8} mb={15}>
            <Container maxWidth="md">
              {children}
            </Container>
          </Box>
        </Grid>
      </Grid>
    </BasicLayout>
  );
}

export default JukeboxLayout;