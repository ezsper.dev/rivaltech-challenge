import React, { memo, Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import  Backdrop from '@material-ui/core/Backdrop';
import  CircularProgress from '@material-ui/core/CircularProgress';
import { ThemeProvider } from '@rivaltech-challenge/design/styles';
import { theme } from '@rivaltech-challenge/design/styles/themes/default';
import { ApolloProvider } from '@apollo/react-hooks';
import { IntlProvider } from 'react-intl';
import { AuthProvider } from './AuthContext';
import { store } from '../../store';
import { client } from '../../graphql';
import { ErrorBoundary } from '../ErrorBoundary';

const Loading = () => (
  <Backdrop open>
    <CircularProgress color="inherit" />
  </Backdrop>
);

export const AppContext = memo(function AppContext({ children }) {
  return (
    <IntlProvider locale="en">
      <ErrorBoundary>
        <Suspense fallback={<Loading />}>
          <Provider store={store}>
            <ApolloProvider client={client}>
              <AuthProvider>
                <ThemeProvider theme={theme}>
                  <BrowserRouter>
                    {children}
                  </BrowserRouter>
                </ThemeProvider>
              </AuthProvider>
            </ApolloProvider>
          </Provider>
        </Suspense>
      </ErrorBoundary>
    </IntlProvider>
  );
});