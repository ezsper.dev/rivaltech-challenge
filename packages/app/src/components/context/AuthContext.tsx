import React, { createContext, useContext, useMemo, ReactNode } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AuthStore, RootState, Dispatch } from '../../store';
import { AuthQuery } from '../../graphql';
import { useLazyAuthQuery } from '../../graphql/queries/AuthQuery';

export interface AuthContextValue extends AuthQuery {

}

export const AuthContext = createContext<AuthContextValue | null>(null);

export interface AuthProviderProps {
  children?: ReactNode;
}

const mapStateToProps = (state: RootState, props: AuthProviderProps) => ({
  token: state.auth.token,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      removeToken: AuthStore.removeToken,
    },
    dispatch,
  );


export type EnhancedAuthProviderProps = ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>
  & AuthProviderProps;

const enhance = connect(mapStateToProps, mapDispatchToProps);

function BaseAuthProvider(props: EnhancedAuthProviderProps) {
  const { token, removeToken, children } = props;
  const [fetch, { data, loading, error }] = useLazyAuthQuery();

  // whenever token changes request a new data
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useMemo(() => fetch(), [fetch, token]);

  // memoize auth context value
  const value = useMemo<AuthContextValue | null>(() => {
    if (!data) {
      return null;
    }
    return { ...data };
  }, [data]);

  if (error) {
    // if error is a invalid token error
    const graphqlError = error.graphQLErrors[0];
    if (graphqlError && graphqlError.extensions) {
      const { code } = graphqlError.extensions;
      if (code === 'INVALID_TOKEN_ERROR') {
        // removes invalid token
        removeToken();
        return null;
      }
    }
    // otherwise throw the error
    throw error;
  }

  if (loading) {
    return null;
  }

  return (
    <AuthContext.Provider value={value}>
      {children}
    </AuthContext.Provider>
  );
}

export const AuthProvider = enhance(BaseAuthProvider);

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error();
  }
  return context;
};