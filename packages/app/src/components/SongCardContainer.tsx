import React from 'react';
import { SongCard, SongCardProps } from '@rivaltech-challenge/design/cards/SongCard';
import { SongPlaylistSelectContainer } from './SongPlaylistSelectContainer';

export interface SongCardContainerProps extends Omit<SongCardProps, 'action'> {

}

export function SongCardContainer(props: SongCardProps) {
  const {
    song,
    ...rest
  } = props;
  return (
    <SongCard
      song={song}
      action={<SongPlaylistSelectContainer songId={song.id} />}
      {...rest}
    />
  );
}