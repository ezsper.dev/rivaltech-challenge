import React, {useCallback} from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import LinearProgress from '@material-ui/core/LinearProgress';
import Box from '@material-ui/core/Box';
import { CommentCard } from '@rivaltech-challenge/design/CommentCard';
import { CommentInput } from '@rivaltech-challenge/design/CommentInput';
import { useAuth } from './context/AuthContext';
import { useCommentListQuery, CommentListQueryVariables } from '../graphql/queries/CommentListQuery';
import {useCreateCommentMutation} from "../graphql/mutations/CreateCommentMutation";

export interface PostCommentsProps {
  variables: CommentListQueryVariables;
  onPublish?: () => void;
}

export const PostComments = (props: PostCommentsProps) => {
  const { variables, onPublish } = props;
  const auth = useAuth();
  const { data, error, refetch } = useCommentListQuery(variables);
  const [createComment, { loading: publishingComment }] = useCreateCommentMutation();

  const handleCommentSubmit = useCallback((content: string, clear: () => void) => {
    if (data) {
      createComment({
        variables: {
          postId: variables.postId,
          input: { content },
        },
      }).then(() => {
        clear();
        if (onPublish) {
          onPublish();
        }
        refetch();
      });
    }
  }, [createComment, onPublish, data, variables.postId, refetch]);
  if (error) {
    throw error;
  }

  if (!data) {
    return <LinearProgress variant="query" />;
  }

  return (
    <div>
      <Box mb={5}>
        <CommentInput
          submitting={publishingComment}
          onSubmit={handleCommentSubmit}
          author={auth.me.viewer}
        />
      </Box>
      <GridList cellHeight="auto" cols={1}>
        {data.comments.edges.map(({ node }) => (
          <GridListTile key={node.id} cols={1}>
            <div style={{ padding: 5 }}>
              <CommentCard comment={node} />
            </div>
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
};