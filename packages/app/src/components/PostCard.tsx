import React from 'react';
import { Link } from 'react-router-dom';
import { ArticleCardSkeleton } from '@rivaltech-challenge/design/ArticleCardSkeleton';
import { ArticleCard, ArticleCardProps } from '@rivaltech-challenge/design/ArticleCard';
import { usePostCardQuery } from '../graphql/queries/PostCardQuery';

export interface PostCardProps extends Omit<ArticleCardProps, 'post'> {
  id: string;
}

export const PostCard = (props: PostCardProps) => {
  const { id, landscape, className, style, ...rest } = props;
  const { data } = usePostCardQuery({ id });

  if (!data) {
    return (
      <ArticleCardSkeleton
        className={className}
        style={style}
        landscape={landscape}
      />
    );
  }
  const { post } = data;
  return (
    <ArticleCard
      CardActionAreaProps={{
        component: Link,
        to: `/p/${post._id}/${post.name}`,
      } as any}
      className={className}
      style={style}
      post={post}

      {...rest}
    />
  );
};