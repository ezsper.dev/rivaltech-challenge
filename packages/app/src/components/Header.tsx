import React, { useCallback } from 'react';
import HeaderSection, { HeaderSectionProps } from '@rivaltech-challenge/design/HederSection';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import ButtonBase from '@material-ui/core/ButtonBase';
import PostOwnerAvatar from '@rivaltech-challenge/design/PostOwnerAvatar';
import IconButtonMenu from '@rivaltech-challenge/design/IconButtonMenu';
import GitHubIcon from '@material-ui/icons/GitHub';
import { useAuth } from './context/AuthContext';
import { useStore, AuthStore } from '../store';

export interface HeaderProps extends HeaderSectionProps {

}


export const Header = (props: HeaderProps) => {
  const { action: routeAction, ...rest } = props;
  const { me: { viewer } } = useAuth();
  const store = useStore();
  const logoutCallback = useCallback(() => {
    store.dispatch(AuthStore.removeToken());
  }, [store]);
  const action = [
    ...(Array.isArray(routeAction)
      ? routeAction
      : [<React.Fragment key="routeAction">{routeAction}</React.Fragment>]
    ),
    ...(viewer
      ? [
          <ButtonBase key="avatar" centerRipple focusRipple>
            <PostOwnerAvatar owner={viewer} />
          </ButtonBase>,
          <IconButtonMenu color="inherit" aria-label="settings">
            <MenuItem onClick={logoutCallback}>Logout</MenuItem>
          </IconButtonMenu>,
        ]
      : [
          <Button key="login" variant="outlined" color="inherit" startIcon={<GitHubIcon />} component={Link} to="/login/github/">
            Login
          </Button>
      ]
    ),
  ];
  return (
    <HeaderSection
      {...rest}
      nav={
        <>
          <Button component={Link} to="/" color="inherit">Home</Button>
          <Button component={Link} to="/explore" color="inherit">Explore</Button>
          <Button component={Link} to="/music" color="inherit">Jukebox</Button>
          <Button component={Link} to="/metrics" color="inherit">Metrics</Button>
        </>
      }
      action={action}
    />
  );
};