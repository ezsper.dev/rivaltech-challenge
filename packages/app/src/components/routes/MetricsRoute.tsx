import React from 'react';
import { Link } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { BasicLayout } from '@rivaltech-challenge/design/layouts/BasicLayout';
import { makeStyles } from '@material-ui/core/styles';
import { Header } from '../Header';
import { Footer } from '../Footer';

export interface MetricsRouteProps {}

const useStyles = makeStyles(theme => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
}));

const MetricsRoute = (props: MetricsRouteProps) => {
  const classes = useStyles();
  const newPostBtn = (
    <Button key="newStory" variant="outlined" color="inherit" component={Link} to="/new/post">
      New Story
    </Button>
  );

  return (
    <BasicLayout
      header={<Header action={[newPostBtn]} />}
      footer={<Footer />}
    >
      <div className={classes.heroContent}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h3" align="center" color="textPrimary" gutterBottom>
            Metrics
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary" paragraph>
            Sorry, did not had the time to do this :'(
          </Typography>
        </Container>
      </div>
    </BasicLayout>
  );
};

export default MetricsRoute;