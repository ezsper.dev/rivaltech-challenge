import React, { useMemo, useCallback } from 'react';
import { bindActionCreators } from 'redux';
import { useHistory } from 'react-router-dom';
import { BasicLayout } from '@rivaltech-challenge/design/layouts/BasicLayout';
import { EditArticleBody } from '@rivaltech-challenge/design/EditArticleBody';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { Header } from '../Header';
import { Footer } from '../Footer';
import { useAuth } from '../context/AuthContext';
import { usePostCategoriesQuery } from '../../graphql/queries/PostCategoriesQuery';
import { useCreatePostMutation } from '../../graphql/mutations/CreatePostMutation';
import { useSelector, useDispatch, PostDraftStore } from '../../store';

export interface NewPostRouteProps {}

const NewPostRoute = (props: NewPostRouteProps) => {
  const auth = useAuth();
  const {
    draft,
    publishingDraft,
  } = useSelector(state => ({
    draft: state.postDraft.draft,
    publishingDraft: state.postDraft.publishingDraft,
  }));
  const canPublish = auth.me.viewer && !publishingDraft;
  const history = useHistory();
  const dispatch = useDispatch();
  const [createPost] = useCreatePostMutation();
  const actions = useMemo(() =>
    bindActionCreators({
      updateDraftTitle: PostDraftStore.updateDraftTitle,
      updateDraftSubheading: PostDraftStore.updateDraftSubheading,
      updateDraftContent: PostDraftStore.updateDraftContent,
      updateDraftCategory: PostDraftStore.updateDraftCategory,
      clearDraft: PostDraftStore.clearDraft,
      setPublishing: PostDraftStore.setPublishing,
    }, dispatch),
    [dispatch]);
  const postCategoriesQuery = usePostCategoriesQuery();
  const categories = postCategoriesQuery.data
    && postCategoriesQuery.data.postCategories.nodes;

  const handlePublish = useCallback(() => {
    if (!canPublish) {
      return;
    }
    if (!draft.category) {
      alert('Selecione a categoria');
      return;
    }

    actions.setPublishing(true);

    createPost({
      variables: {
        input: {
          title: draft.title,
          subheading: draft.subheading,
          content: draft.content,
          categoryId: draft.category.id,
        },
      }
    })
    .finally(() => {
      actions.setPublishing(false);
    }).then(({ data }) => {
      actions.clearDraft();
      if (data) {
        const { post } = data.createPost;
        history.push(`/p/${post._id}/${post.name}`);
      }
    });
  }, [createPost, canPublish, history, actions, draft]);

  const discardBtn = (
    <Button key="discard" variant="contained" color="default" onClick={actions.clearDraft}>
      Discard
    </Button>
  );

  let publishBtn = (
    <Button key="publish" variant="contained" color="secondary" onClick={handlePublish}>
      {publishingDraft ? 'Publishing...' : 'Publish' }
    </Button>
  );

  if (!auth.me.viewer) {
    publishBtn = (
      <Tooltip title="You must login to publish" aria-label="not allowed">
        {publishBtn}
      </Tooltip>
    );
  }
  return (
    <BasicLayout
      header={
        <Header
          sticked
          action={[
            discardBtn,
            publishBtn,
          ]}
        />
      }
      footer={<Footer />}
    >
      <Box mt={8} mb={15}>
        <EditArticleBody
          post={draft}
          disabled={publishingDraft}
          categories={categories}
          onTitleChange={actions.updateDraftTitle}
          onSubheadingChange={actions.updateDraftSubheading}
          onContentChange={actions.updateDraftContent}
          onCategoryChange={actions.updateDraftCategory}
        />
      </Box>
    </BasicLayout>
  );
};

export default NewPostRoute;