import React, { useMemo, useCallback } from 'react';
import {Route, RouteProps, useRouteMatch, Link} from 'react-router-dom';
import { BasicLayout } from '@rivaltech-challenge/design/layouts/BasicLayout';
import { EditArticleBody } from '@rivaltech-challenge/design/EditArticleBody';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { Header } from '../Header';
import { Footer } from '../Footer';
import { useAuth } from '../context/AuthContext';
import { usePostCategoriesQuery } from '../../graphql/queries/PostCategoriesQuery';
import { useUpdatePostMutation } from '../../graphql/mutations/UpdatePostMutation';
import { useSinglePostLazyQuery } from "../../graphql/queries/SinglePostQuery";

export interface EditPostRouteProps extends Omit<RouteProps, 'render'> {}

const EditPostRoute = (props: EditPostRouteProps) => {
  const auth = useAuth();
  const match = useRouteMatch<{ _id: string, name: string }>();
  const { _id } = match.params;
  const [execute, { called, data, refetch }] = useSinglePostLazyQuery({ _id });
  if (!called) {
    execute();
  }

  const post = data ? data.post : null;
  const newPost = useMemo(() => ({ ...post }), [post]) as any;
  const handleTitleChange = useCallback((title: string) => newPost.title = title, [newPost]);
  const handleSubheadingChange = useCallback((subheading: string) => newPost.subheading = subheading, [newPost]);
  const handleContentChange = useCallback((content: string) => newPost.content = content, [newPost]);
  const handleCategoryChange = useCallback((category: any) => newPost.category = category, [newPost]);


  const [updatePost, { loading: updatingPost }] = useUpdatePostMutation();
  const postCategoriesQuery = usePostCategoriesQuery();
  const categories = postCategoriesQuery.data
    && postCategoriesQuery.data.postCategories.nodes;

  const handlePublish = useCallback(() => {
    if (!post) {
      return;
    }

    if (!post.category) {
      alert('You must select a category');
      return;
    }

    updatePost({
      variables: {
        id: post.id,
        input: {
          title: newPost.title,
          subheading: newPost.subheading,
          content: newPost.content,
          categoryId: newPost.category.id,
        },
      }
    }).then(() => refetch());
  }, [updatePost, newPost, post, refetch]);

  if (!post) {
    return null;
  }

  const viewBtn = (
    <Button key="viewStory" component={Link} to={`/p/${post._id}/${post.name}`} variant="outlined" color="inherit">
      View Story
    </Button>
  );
  
  let saveBtn = (
    <Button key="saveStory" variant="contained" color="secondary" disabled={!auth.me.viewer || updatingPost} onClick={handlePublish}>
      {updatingPost ? 'Saving...' : 'Save' }
    </Button>
  );

  if (!auth.me.viewer) {
    saveBtn = (
      <Tooltip title="You must login to publish" aria-label="add">
        {saveBtn}
      </Tooltip>
    );
  }
  return (
    <Route {...props}>
      <BasicLayout
        header={
          <Header
            sticked
            action={[
              viewBtn,
              saveBtn,
            ]}
          />
        }
        footer={<Footer />}
      >
        <Box mt={8} mb={15}>
          <EditArticleBody
            post={post}
            disabled={updatingPost}
            categories={categories}
            onTitleChange={handleTitleChange}
            onSubheadingChange={handleSubheadingChange}
            onContentChange={handleContentChange}
            onCategoryChange={handleCategoryChange}
          />
        </Box>
      </BasicLayout>
    </Route>
  );
};

export default EditPostRoute;