import React from 'react';
import { useLocation, Redirect, RouteProps } from 'react-router-dom';
import { useStore, AuthStore } from '../../store';

export interface AuthTokenRouteProps extends Omit<RouteProps, 'render'> {}

const AuthTokenRoute = (props: RouteProps) => {
  const store = useStore();
  const match = useLocation().search.match(/token=(.+)(&|$)/);
  const token = match ? decodeURIComponent(match[1]) : null;
  if (token) {
    store.dispatch(AuthStore.setToken(token));
  }
  return <Redirect to="/" push />;
};

export default AuthTokenRoute;