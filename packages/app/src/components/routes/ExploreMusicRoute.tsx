import React, { useCallback, useMemo, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { SearchBox } from '@rivaltech-challenge/design/SearchBox';
import GridList from '@material-ui/core/GridList';
import Typography from '@material-ui/core/Typography';
import GridListTile from '@material-ui/core/GridListTile';
import Box from '@material-ui/core/Box';
import { Header } from '../Header';
import { Footer } from '../Footer';
import { SongCardContainer } from '../SongCardContainer';
import { useExploreMusicResultsQuery } from '../../graphql/queries/ExploreMusicResultsQuery';
import { useSelector, useDispatch, ExploreFilterStore } from '../../store';
import { JukeboxLayout } from '../layouts/JukeboxLayout';

export interface ExploreRouteProps  {}


const ExploreMusicRoute = (props: ExploreRouteProps) => {
  const { searchTerm } = useSelector(state => state.exploreFilter);
  const dispatch = useDispatch();
  const actions = useMemo(() => bindActionCreators({
    setCategoryFilter: ExploreFilterStore.setCategoryFilter,
    changeSearchTerm: ExploreFilterStore.changeSearchTerm,
  }, dispatch), [dispatch]);
  useEffect(() => {
    return () => {
      // clears search term on unmount
      actions.changeSearchTerm('');
    };
  }, [])
  const handleSearchTermChange = useCallback((event: any) => {
    actions.changeSearchTerm(event.target.value);
  }, [actions]);
  const { loading: loadingMusicResults, data: musicResultsData } = useExploreMusicResultsQuery({ searchTerm, first: 15 });

  return (
    <JukeboxLayout
      header={<Header />}
      footer={<Footer />}
    >
      <Box mt={5} mb={5} display="flex" justifyContent="center">
        <SearchBox
          placeholder="Type your favorite music"
          loading={loadingMusicResults}
          onChange={handleSearchTermChange}
        />
      </Box>
      <GridList cellHeight="auto" cols={1}>
        {musicResultsData && (musicResultsData.songs.edges.length === 0
            ? (<Typography>No result was found</Typography>)
            : (musicResultsData.songs.edges.map(({ node: song }) => (
              <GridListTile key={song.id} cols={1}>
                <div style={{ padding: 5 }}>
                  <SongCardContainer song={song} />
                </div>
              </GridListTile>
            )))
        )}
      </GridList>
    </JukeboxLayout>
  );
};

export default ExploreMusicRoute;