import React from 'react';

export const HomeRoute = React.lazy(() => import('./HomeRoute'));
export const AuthTokenRoute = React.lazy(() => import('./AuthTokenRoute'));
export const LoginGithubRoute = React.lazy(() => import('./LoginGithubRoute'));
export const NewPostRoute = React.lazy(() => import('./NewPostRoute'));
export const EditPostRoute = React.lazy(() => import('./EditPostRoute'));
export const PostRoute = React.lazy(() => import('./PostRoute'));
export const ExploreRoute = React.lazy(() => import('./ExploreRoute'));
export const MetricsRoute = React.lazy(() => import('./MetricsRoute'));
export const ExploreMusicRoute = React.lazy(() => import('./ExploreMusicRoute'));
export const MusicPlaylistRoute = React.lazy(() => import('./MusicPlaylistRoute'));