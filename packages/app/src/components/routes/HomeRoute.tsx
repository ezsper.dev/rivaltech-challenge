import React from 'react';
import { Link } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { BasicLayout } from '@rivaltech-challenge/design/layouts/BasicLayout';
import { makeStyles } from '@material-ui/core/styles';
import { Header } from '../Header';
import { Footer } from '../Footer';

export interface HomeRouteProps {}

const useStyles = makeStyles(theme => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
}));

const HomeRoute = (props: HomeRouteProps) => {
  const classes = useStyles();
  const newPostBtn = (
    <Button key="newStory" variant="outlined" color="inherit" component={Link} to="/new/post">
      New Story
    </Button>
  );

  return (
    <BasicLayout
      header={<Header action={[newPostBtn]} />}
      footer={<Footer />}
    >
      {/* Hero unit */}
      <div className={classes.heroContent}>
        <Container maxWidth="sm">
          <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
            Free your mind with this simple app
          </Typography>
          <Typography variant="h5" align="center" color="textSecondary" paragraph>
            This app uses Redux for UX states and Apollo GraphQL for realtime data.
          </Typography>
          <div className={classes.heroButtons}>
            <Grid container spacing={2} justify="center">
              <Grid item>
                <Button component={Link} to="/new/post" variant="contained" color="primary">
                  Create your first story
                </Button>
              </Grid>
              <Grid item>
                <Button component={Link} to="/explore" variant="outlined" color="primary">
                  Explore our storybase
                </Button>
              </Grid>
            </Grid>
          </div>
        </Container>
      </div>
    </BasicLayout>
  );
};

export default HomeRoute;