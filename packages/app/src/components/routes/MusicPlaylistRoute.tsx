import React, { useState, useCallback, useEffect } from "react";
import { RouteChildrenProps } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import { LoadingBox } from '@rivaltech-challenge/design/feedback/LoadingBox';
import { SongPlaylistEditInput } from '@rivaltech-challenge/design/inputs/SongPlaylistEditInput';
import { makeStyles } from '@material-ui/core/styles';
import { JukeboxLayout } from '../layouts/JukeboxLayout';
import { Header } from '../Header';
import { Footer } from '../Footer';
import { useSongPlaylistByShortIdQuery } from '../../graphql/queries/SongPlaylistByShortIdQuery';

export interface MusicPlaylistRouteProps extends RouteChildrenProps<{ id: string }> {}

const useStyles = makeStyles(theme => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
}));

const MusicPlaylistRoute = (props: MusicPlaylistRouteProps) => {
  const { match } = props;
  if (!match) {
    throw new Error();
  }
  const { data, loading } = useSongPlaylistByShortIdQuery({
    variables: {
      _id: match.params.id,
    },
  });
  const classes = useStyles();

  const [items, setItems] = useState(() => data ? data.songPlaylistByShortId.items : []);

  useEffect(() => {
    if (data) {
      setItems(data.songPlaylistByShortId.items);
    }
  }, [data]);

  const handleReorder = useCallback((newItems: typeof items) => {
    setItems(newItems);
  }, []);

  return (
    <JukeboxLayout
      header={<Header />}
      footer={<Footer />}
    >
      <div className={classes.heroContent}>
        <Container maxWidth="md">
          <LoadingBox
            loading={loading}
            minHeight={300}
            whileLoading="Loading your playlist's songs">
            {data && (
              <SongPlaylistEditInput
                onReorder={handleReorder}
                title={data.songPlaylistByShortId.title}
                privacy={data.songPlaylistByShortId.privacy}
                items={items}
              />
            )}
          </LoadingBox>
        </Container>
      </div>
    </JukeboxLayout>
  );
};

export default MusicPlaylistRoute;