import React, { useCallback, useEffect, useMemo } from "react";
import { bindActionCreators } from 'redux';
import { BasicLayout } from '@rivaltech-challenge/design/layouts/BasicLayout';
import { SearchBox } from '@rivaltech-challenge/design/SearchBox';
import { Link } from "react-router-dom";
import Button from '@material-ui/core/Button';
import GridList from '@material-ui/core/GridList';
import Typography from '@material-ui/core/Typography';
import GridListTile from '@material-ui/core/GridListTile';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Header } from '../Header';
import { Footer } from '../Footer';
import { usePostCategoriesQuery } from '../../graphql/queries/PostCategoriesQuery';
import { useExplorePostQuery } from '../../graphql/queries/ExplorePostsQuery';
import { useSelector, useDispatch, ExploreFilterStore } from '../../store';
import { PostCard } from '../PostCard';

export interface ExploreRouteProps  {}

const ExploreRoute = (props: ExploreRouteProps) => {
  const { categories, searchTerm } = useSelector(state => state.exploreFilter);
  const dispatch = useDispatch();
  const actions = useMemo(() => bindActionCreators({
    setCategoryFilter: ExploreFilterStore.setCategoryFilter,
    changeSearchTerm: ExploreFilterStore.changeSearchTerm,
  }, dispatch), [dispatch]);
  const handleSearchTermChange = useCallback((event: any) => {
    actions.changeSearchTerm(event.target.value);
  }, [actions]);
  useEffect(() => {
    return () => {
      // clears search term on unmount
      actions.changeSearchTerm('');
    };
  }, [])
  const handleCategoryChange = useCallback((event: any) => {
    actions.setCategoryFilter(event.target.value, event.target.checked);
  }, [actions]);
  const { loading:loadingCategories , data: categoriesData } = usePostCategoriesQuery();
  const { loading: loadingPosts, data: postsData } = useExplorePostQuery({ categories, searchTerm });
  const newPostBtn = (
    <Button key="newStory" variant="outlined" color="inherit" component={Link} to="/new/post">
      New Story
    </Button>
  );
  return (
    <BasicLayout
      style={{ background: '#f9f9f9' }}
      header={<Header action={[newPostBtn]} />}
      footer={<Footer />}
    >
      <Grid container style={{ flex: 1 }}>
        <Grid item sm={2} md={3} lg={2} style={{ background: 'white', borderRight: '1px solid #eee' }}>
          <Box p={2} display="flex">
            <FormControl component="fieldset">
              <FormLabel component="legend" style={{ marginBottom: 10, marginTop: 10 }}>Categories</FormLabel>
              <FormGroup>
                {loadingCategories && <LinearProgress />}
                {categoriesData && categoriesData.postCategories.nodes.map(node => (
                  <FormControlLabel key={node.id}
                    control={<Checkbox onChange={handleCategoryChange} value={node.id} />}
                    label={node.title}
                  />
                ))}
              </FormGroup>
            </FormControl>
          </Box>
        </Grid>
        <Grid item sm={10} md={9} lg={10}>
          <Box mt={8} mb={15}>
            <Container maxWidth="md">
              <Box mt={5} mb={5}>
                <SearchBox
                  placeholder="Search for an story"
                  loading={loadingPosts}
                  onChange={handleSearchTermChange}
                />
              </Box>
              <GridList cellHeight="auto" cols={3}>
                {postsData && (postsData.posts.edges.length === 0
                  ? (<Typography>No result was found</Typography>)
                  : (postsData.posts.edges.map(({ node: { id } }) => (
                      <GridListTile key={id} cols={1}>
                        <div style={{ padding: 5 }}>
                          <PostCard id={id} />
                        </div>
                      </GridListTile>
                    )))
                )}
              </GridList>
            </Container>
          </Box>
        </Grid>
      </Grid>
    </BasicLayout>
  );
};

export default ExploreRoute;