import { config } from '../../config';

export interface AuthTokenRouteProps {}

const LoginGithubRoute = (props: AuthTokenRouteProps) => {
  window.location.href = config.githubAuthUri;
  return null;
};

export default LoginGithubRoute;