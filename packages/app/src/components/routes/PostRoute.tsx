import React, { useCallback } from 'react';
import { useRouteMatch, Redirect, Link } from 'react-router-dom';
import { BasicLayout } from '@rivaltech-challenge/design/layouts/BasicLayout';
import { ArticleBody } from '@rivaltech-challenge/design/ArticleBody';
import { PostRatingBox } from '@rivaltech-challenge/design/PostRatingBox';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import { Header } from '../Header';
import { Footer } from '../Footer';
import { PostComments } from '../PostComments';
import { useAuth } from '../context/AuthContext';
import { useSinglePostLazyQuery } from '../../graphql/queries/SinglePostQuery';
import { useRatePostMutation } from '../../graphql/mutations/RatePostMutation';

export interface PostRouteProps {}

const PostRoute = (props: PostRouteProps) => {
  const match = useRouteMatch<{ _id: string, name?: string }>();
  const { _id, name } = match.params;
  const auth = useAuth();
  const [execute, { called, data, refetch }] = useSinglePostLazyQuery({ _id });
  const [ratePost, { loading: ratingPost }] = useRatePostMutation();

  const handlePostRate = useCallback((event: any, value: number | null) => {
    if (data) {
      ratePost({
        variables: {
          input: {
            postId: data.post.id,
            ratingScore: value && value * 2,
          }
        },
      }).then(() => refetch());
    }
  }, [ratePost, data, refetch]);

  if (!called) {
    execute();
  }

  if (!data) {
    return null;
  }

  const { post } = data;

  if (!name) {
    return (<Redirect to={`/p/${post._id}/${post.name}`} />)
  }

  let editPostBtn = auth.me.viewer && auth.me.viewer.id === post.author.id
    ? (
      <Button key="editStory" variant="contained" color="primary" component={Link} to={`/p/${post._id}/${post.name}/edit`}>
        Edit
      </Button>
    )
    : null;

  let newPostBtn = (
    <Button key="newStory" variant="outlined" component={Link} to="/new/post">
      New Story
    </Button>
  );

  return (
    <BasicLayout
      header={
        <Header
          variant="transparent"
          sticked
          action={[
            newPostBtn,
            editPostBtn,
          ]}
        />}
      footer={<Footer />}
    >
      <Box mt={8}>
        <Container maxWidth="md">
          <Box mb={2}>
            <ArticleBody post={post} />
          </Box>
        </Container>
        <Box style={{ background: '#f9f9f9' }} pb={15}>
          <Container maxWidth="md">
            <Divider />
            <Box mt={4} mb={2}>
              <PostRatingBox
                loading={ratingPost}
                onChange={handlePostRate}
                value={post.viewerRatingScore || null}
                total={post.counters.raters}
                avgValue={post.ratingScore}
              />
            </Box>
            <Divider />
            <Box mt={4}>
              <PostComments onPublish={refetch} variables={{ postId: post.id }} />
            </Box>
          </Container>
        </Box>
      </Box>
    </BasicLayout>
  );
};

export default PostRoute;