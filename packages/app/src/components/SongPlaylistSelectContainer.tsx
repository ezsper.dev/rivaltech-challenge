import React, { useCallback, useState } from 'react';
import { SongPlaylistSelect, SongPlaylistSelectProps } from '@rivaltech-challenge/design/inputs/SongPlaylistSelect';
import { useAuth } from './context/AuthContext';
import { useLazyMySongPlaylistsQuery } from '../graphql/queries/MySongPlaylistsQuery';
import { useLazySongWithMyPlaylistsQuery } from '../graphql/queries/SongWithMyPlaylistsQuery';
import { useCreateSongPlaylistMutation } from '../graphql/mutations/CreateSongPlaylistMutation';
import { useAddPlaylistsToSongMutation } from '../graphql/mutations/AddPlaylistsToSongMutation';
import { useRemovePlaylistsFromSongMutation } from '../graphql/mutations/RemovePlaylistsFromSongMutation';

export interface SongPlaylistSelectContainerProps {
  songId: string;
}

function SongPlaylistSelectContainerLogged(props: SongPlaylistSelectContainerProps) {
  const { songId } = props;
  const auth = useAuth();

  if (!auth.me.viewer) {
    throw new Error();
  }

  const [createSongPlaylist, { loading: isCreatingNew }] = useCreateSongPlaylistMutation();
  const [addPlaylistsToSong] = useAddPlaylistsToSongMutation();
  const [removePlaylistsToSong] = useRemovePlaylistsFromSongMutation();

  const [selectedPlaylists, setSelectedPlaylists] = useState<string[]>(() => []);

  const [
    loadMySongPlaylists,
    {
      data: mySongPlaylists,
      refetch: refetchMySongPlaylists,
      called: calledMySongPlaylists,
    },
  ] = useLazyMySongPlaylistsQuery({});
  const [
    loadSongWithMyPlaylists,
    { refetch: refetchSongWithMyPlaylists },
  ] = useLazySongWithMyPlaylistsQuery({
    variables: { id: songId },
    onCompleted: (data) => {
      setSelectedPlaylists(data.song.myPlaylists.nodes.map(node => node.id));
    },
  });

  const handleOpen = useCallback(() => {
    if (!calledMySongPlaylists) {
      loadSongWithMyPlaylists();
      loadMySongPlaylists();
    }
  }, [
    calledMySongPlaylists,
    loadSongWithMyPlaylists,
    loadMySongPlaylists,
  ]);

  const handleNewSubmit = useCallback<Exclude<SongPlaylistSelectProps['onCreateNewSubmit'], undefined>>((data, done) => {
    createSongPlaylist({
      variables: {
        input: {
          ...data,
          songs: [songId],
        },
      },
    }).then(() =>
      Promise.all([
        refetchSongWithMyPlaylists(),
        refetchMySongPlaylists(),
      ]),
    ).then(() => setTimeout(done, 100));
  }, [createSongPlaylist, refetchSongWithMyPlaylists, refetchMySongPlaylists]);

  const handleSelectedPlaylists = useCallback((newPlaylistSelection: string[]) => {
    const toAdd: string[] = [];
    const toRemove: string[] = [];
    const currentPlaylists = Object.fromEntries(selectedPlaylists.map(key => [key, true]));
    for (const id of newPlaylistSelection) {
      if (currentPlaylists[id]) {
        delete currentPlaylists[id];
        continue;
      }
      toAdd.push(id);
    }
    for (const id of Object.keys(currentPlaylists)) {
      toRemove.push(id);
    }

    Promise.all<any, any>([
      toAdd.length > 0 && addPlaylistsToSong({
        variables: {
          input: {
            song: songId,
            songPlaylists: toAdd,
          },
        },
      }),
      toRemove.length > 0 && removePlaylistsToSong({
        variables: {
          input: {
            song: songId,
            songPlaylists: toRemove,
          },
        },
      }),
    ]).catch(() => {});
    setSelectedPlaylists(newPlaylistSelection);
  }, [selectedPlaylists]);

  return (
    <SongPlaylistSelect
      onOpen={handleOpen}
      isCreatingNew={isCreatingNew}
      onCreateNewSubmit={handleNewSubmit}
      loadingPlaylists={!mySongPlaylists}
      playlists={mySongPlaylists && mySongPlaylists.mySongPlaylists.nodes}
      selectedPlaylists={selectedPlaylists}
      onSelectedPlaylists={handleSelectedPlaylists}
    />
  );
}

export function SongPlaylistSelectContainer(props: SongPlaylistSelectContainerProps) {
  const auth = useAuth();

  if (!auth.me.viewer) {
    return (
      <SongPlaylistSelect
        disableCreateNew
        noPlaylistsMessage="Sorry! You must login in order to add to a playlist"
      />
    );
  }

  return (
    <SongPlaylistSelectContainerLogged {...props} />
  );
}