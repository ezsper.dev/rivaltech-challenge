import React, { memo } from 'react';
import { Switch, Route } from 'react-router-dom';
import {
  HomeRoute,
  AuthTokenRoute,
  LoginGithubRoute,
  NewPostRoute,
  PostRoute,
  ExploreRoute,
  EditPostRoute,
  MetricsRoute,
  ExploreMusicRoute,
  MusicPlaylistRoute,
} from './routes';

export const AppRoutes = memo(function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={HomeRoute} />
      <Route path="/explore" exact component={ExploreRoute} />
      <Route path="/auth" exact component={AuthTokenRoute} />
      <Route path="/login/github" exact component={LoginGithubRoute} />
      <Route path="/new/post" exact component={NewPostRoute} />
      <Route path="/p/:_id" exact component={PostRoute} />
      <Route path="/p/:_id/:name" exact component={PostRoute} />
      <Route path="/p/:_id/:name/edit" exact component={EditPostRoute} />
      <Route path="/metrics" exact component={MetricsRoute} />
      <Route path="/music" exact component={ExploreMusicRoute} />
      <Route path="/music/playlists/:id" exact component={MusicPlaylistRoute} />
    </Switch>
  );
});
