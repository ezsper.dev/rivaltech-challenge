import * as ActionType from './ActionType';
import { createAction } from '../../utils/createAction';
import {Post, PostCategory} from '../../graphql';

export type PostCategoryType = Pick<PostCategory, 'id' | 'title'>;
export type PostType = Pick<Post, 'title' | 'subheading' | 'content'>
  & { category: PostCategoryType | null };

export { ActionType };

export function updateDraftTitle(newTitle: string) {
  return createAction(ActionType.UPDATE_DRAFT_TITLE, { newTitle });
}

export interface UpdateDraftTitleAction extends ReturnType<typeof updateDraftTitle> {}

export function updateDraftSubheading(newSubheading: string) {
  return createAction(ActionType.UPDATE_DRAFT_SUBHEADING, { newSubheading });
}

export interface UpdateDraftSubheadingAction extends ReturnType<typeof updateDraftSubheading> {}


export function updateDraftContent(newContent: string) {
  return createAction(ActionType.UPDATE_DRAFT_CONTENT, { newContent });
}

export interface UpdateDraftContentAction extends ReturnType<typeof updateDraftContent> {}


export function updateDraftCategory(newCategory: PostCategoryType | null) {
  return createAction(ActionType.UPDATE_DRAFT_CATEGORY, { newCategory });
}

export interface UpdateDraftCategoryAction extends ReturnType<typeof updateDraftCategory> {}


export function clearDraft() {
  return createAction(ActionType.CLEAR_DRAFT);
}

export interface ClearDraftAction extends ReturnType<typeof clearDraft> {}

export function setPublishing(publishing: boolean) {
  return createAction(ActionType.SET_PUBLISHING, { publishing });
}

export interface SetPublishingAction extends ReturnType<typeof setPublishing> {}

export type Action =
  UpdateDraftTitleAction
  | UpdateDraftSubheadingAction
  | UpdateDraftContentAction
  | UpdateDraftCategoryAction
  | ClearDraftAction
  | SetPublishingAction;