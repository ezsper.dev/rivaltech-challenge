import { ActionType, Action, PostType } from './actions';

export interface PostDraftState {
  draft: PostType;
  publishingDraft: boolean;
}


const initialState: PostDraftState = {
  draft: {
    title: '',
    subheading: '',
    content: '',
    category: null,
  },
  publishingDraft: false,
};

const cache = localStorage.getItem('postDraft');
const recoveredState = cache ? JSON.parse(cache) as PostDraftState : initialState;

export function reducer(state = recoveredState, action: Action): PostDraftState {
  const newState = (() => {
    switch (action.type) {
      case ActionType.UPDATE_DRAFT_TITLE: {
        return {
          ...state,
          draft: {...state.draft, title: action.newTitle},
        };
      }
      case ActionType.UPDATE_DRAFT_SUBHEADING: {
        return {
          ...state,
          draft: {...state.draft, subheading: action.newSubheading},
        };
      }
      case ActionType.UPDATE_DRAFT_CONTENT: {
        return {
          ...state,
          draft: {...state.draft, content: action.newContent},
        };
      }
      case ActionType.UPDATE_DRAFT_CATEGORY: {
        return {
          ...state,
          draft: {...state.draft, category: action.newCategory},
        };
      }
      case ActionType.CLEAR_DRAFT: {
        return initialState;
      }
      case ActionType.SET_PUBLISHING: {
        return { ...state, publishingDraft: action.publishing };
      }
      default:
        return state;
    }
  })();

  if (newState !== state) {
    setTimeout(() => {
      localStorage.setItem('postDraft', JSON.stringify(newState));
    });
  }

  return newState;
}