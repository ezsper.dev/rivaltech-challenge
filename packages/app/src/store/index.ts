import { createStore, combineReducers, applyMiddleware, compose, Store as StoreType } from 'redux';
import {
  useStore as useStoreHook,
  useSelector as useSelectorHook,
  useDispatch as useDispatchHook,
} from 'react-redux';
import {
  createStateSyncMiddleware,
  initStateWithPrevTab,
} from 'redux-state-sync';
import * as AuthStore from './auth';
import * as PostDraftStore from './postDraft';
import * as ExploreFilterStore from './exploreFilter';

export { AuthStore, PostDraftStore, ExploreFilterStore };

declare module 'react-redux' {
  interface DefaultRootState extends RootState {}
}

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__?: () => any;
  }
}

const config = {
  whitelist: [
    AuthStore.ActionType.SET_TOKEN,
    AuthStore.ActionType.REMOVE_TOKEN,
    PostDraftStore.ActionType.UPDATE_DRAFT_CATEGORY,
    PostDraftStore.ActionType.UPDATE_DRAFT_CONTENT,
    PostDraftStore.ActionType.UPDATE_DRAFT_SUBHEADING,
    PostDraftStore.ActionType.UPDATE_DRAFT_TITLE,
    PostDraftStore.ActionType.CLEAR_DRAFT,
  ],
};

export const store = createStore(
  combineReducers({
    auth: AuthStore.reducer,
    postDraft: PostDraftStore.reducer,
    exploreFilter: ExploreFilterStore.reducer,
  }),
  compose(
    // If you are using the devToolsExtension, you can add it here also
    applyMiddleware(
      createStateSyncMiddleware(config),
    ),
    (typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined')
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : (f: any) => f,
  ),
);

initStateWithPrevTab(store);

export type RootStore = typeof store;
export type RootState = RootStore extends StoreType<infer State> ? State : never;
export type RootAction = RootStore extends StoreType<any, infer Action> ? Action : never;
export type Dispatch = RootStore['dispatch'];
export const useStore = useStoreHook as () => typeof store;
export const useSelector = useSelectorHook as {
  <TSelected = unknown>(
    selector: (state: RootState) => TSelected,
    equalityFn?: (left: TSelected, right: TSelected) => boolean
  ): TSelected;
};
export const useDispatch = useDispatchHook as () => Dispatch;