import * as ActionType from './ActionType';
import { createAction } from '../../utils/createAction';
export { ActionType };

export function setCategoryFilter(categoryId: string, checked: boolean) {
  return createAction(ActionType.SET_CATEGORY_FILTER, { categoryId, checked });
}

export interface SetCategoryFilterAction extends ReturnType<typeof setCategoryFilter> {}

export function changeSearchTerm(searchTerm: string) {
  return createAction(ActionType.CHANGE_SEARCH_TERM, { searchTerm });
}


export interface ChangeSearchTermAction extends ReturnType<typeof changeSearchTerm> {}

export type Action = SetCategoryFilterAction | ChangeSearchTermAction;
