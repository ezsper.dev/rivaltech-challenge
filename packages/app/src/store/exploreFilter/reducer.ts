import { ActionType, Action } from './actions';
import without from 'lodash/without';
import union from 'lodash/union';

export type ExploreFilterState = {
  categories: string[];
  searchTerm: string;
}

const initialState: ExploreFilterState = {
  categories: [],
  searchTerm: '',
};

export function reducer(state = initialState, action: Action): ExploreFilterState {
  switch (action.type) {
    case ActionType.CHANGE_SEARCH_TERM:
      return { ...state, searchTerm: action.searchTerm };
    case ActionType.SET_CATEGORY_FILTER:
      return {
        ...state,
        categories: action.checked
          ? union(state.categories, [action.categoryId])
          : without(state.categories, action.categoryId),
      };
    default:
      return state;
  }
}