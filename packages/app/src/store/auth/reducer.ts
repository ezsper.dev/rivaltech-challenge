import Cookie from 'js-cookie';
import { Action, ActionType } from './actions';

export interface AuthState {
  token?: string;
}

export const initialState: AuthState = {
  token: Cookie.get('auth'),
};

export const reducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.SET_TOKEN:
      Cookie.set('auth', action.token);
      return { token: action.token };
    case ActionType.REMOVE_TOKEN:
      Cookie.remove('auth');
      return {};
  }
  return state;
};