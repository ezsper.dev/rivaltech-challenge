import { createAction } from '../../utils/createAction';
import * as ActionType from './ActionType';

export { ActionType };

export function setToken(token: string) {
  return createAction(ActionType.SET_TOKEN, { token });
}

export interface SetTokenAction extends ReturnType<typeof setToken> {}

export function removeToken() {
  return createAction(ActionType.REMOVE_TOKEN);
}

export interface RemoveTokenAction extends ReturnType<typeof removeToken> {}

export type Action =
  SetTokenAction
  | RemoveTokenAction;