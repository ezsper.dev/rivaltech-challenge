export function createAction<ActionType extends string>(type: ActionType): { type: ActionType };
export function createAction<ActionType extends string, Payload extends {}>(type: ActionType, payload: Payload): { type: ActionType } & Payload;
export function createAction<ActionType extends string, Payload extends {}>(type: ActionType, payload?: Payload) {
  return { type, ...payload };
}