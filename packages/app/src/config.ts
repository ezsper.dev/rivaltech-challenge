const apiUri = 'http://localhost:7080';

export const config = {
  graphqlUri: `${apiUri}/`,
  githubAuthUri: `${apiUri}/auth/github/`,
};