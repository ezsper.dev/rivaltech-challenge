import { useMutation } from '@apollo/react-hooks';
import { gql, RemoveSongsFromPlaylistMutation, RemoveSongsFromPlaylistMutationVariables } from '..';

const REMOVE_SONGS_FROM_PLAYLIST_MUTATION = gql `
  mutation RemoveSongsFromPlaylist ($input: RemoveSongsFromPlaylistInput!) {
    removeSongsFromPlaylist(input: $input) {
      songPlaylistId
    }
  }
`;

export const useRemoveSongsFromPlaylistMutation = () =>
  useMutation<RemoveSongsFromPlaylistMutation, RemoveSongsFromPlaylistMutationVariables>(REMOVE_SONGS_FROM_PLAYLIST_MUTATION);