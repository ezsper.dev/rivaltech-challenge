import { useMutation  } from '@apollo/react-hooks';
import { gql, RatePostMutation, RatePostMutationVariables } from '..';

const RATE_POST_MUTATION = gql `
    mutation RatePost ($input: PostRateInput!) {
        rate(input: $input) {
            ratingScore
        }
    }
`;

export const useRatePostMutation = () => useMutation<RatePostMutation, RatePostMutationVariables>(RATE_POST_MUTATION);