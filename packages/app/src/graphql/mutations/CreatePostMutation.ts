import { useMutation  } from '@apollo/react-hooks';
import { gql, CreatePostMutation, CreatePostMutationVariables } from '..';

const CREATE_POST_MUTATION = gql `
    mutation CreatePost ($input: PostInput!) {
        createPost(input: $input) {
            post {
                _id,
                name
            }
        }
    }
`;

export const useCreatePostMutation = () => useMutation<CreatePostMutation, CreatePostMutationVariables>(CREATE_POST_MUTATION);