import { useMutation } from '@apollo/react-hooks';
import { gql, CreateSongPlaylistMutation, CreateSongPlaylistMutationVariables } from '..';

const CREATE_SONG_PLAYLIST_MUTATION = gql `
  mutation CreateSongPlaylist ($input: SongPlaylistInput!) {
    createSongPlaylist(input: $input) {
      songPlaylistId
    }
  }
`;

export const useCreateSongPlaylistMutation = () =>
  useMutation<CreateSongPlaylistMutation, CreateSongPlaylistMutationVariables>(CREATE_SONG_PLAYLIST_MUTATION);