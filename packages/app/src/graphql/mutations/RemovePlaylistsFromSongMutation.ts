import { useMutation } from '@apollo/react-hooks';
import { gql, RemovePlaylistsFromSongMutation, RemovePlaylistsFromSongMutationVariables } from '..';

const REMOVE_PLAYLISTS_FROM_SONG_MUTATION = gql `
  mutation RemovePlaylistsFromSong ($input: RemovePlaylistsFromSongInput!) {
    removePlaylistsFromSong(input: $input) {
      songId
    }
  }
`;

export const useRemovePlaylistsFromSongMutation = () =>
  useMutation<RemovePlaylistsFromSongMutation, RemovePlaylistsFromSongMutationVariables>(REMOVE_PLAYLISTS_FROM_SONG_MUTATION);