import { useMutation  } from '@apollo/react-hooks';
import { gql, CreateCommentMutation, CreateCommentMutationVariables } from '..';

const CREATE_COMMENT_MUTATION = gql `
    mutation CreateComment ($postId: ID!, $input: CommentInput!) {
        createComment(postId: $postId, input: $input) {
            comment {
                id,
                content,
                author {
                    avatarSrc
                    displayName
                    initials
                }
            }
        }
    }
`;

export const useCreateCommentMutation = () => useMutation<CreateCommentMutation, CreateCommentMutationVariables>(CREATE_COMMENT_MUTATION);