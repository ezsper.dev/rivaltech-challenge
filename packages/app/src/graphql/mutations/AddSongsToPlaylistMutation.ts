import { useMutation } from '@apollo/react-hooks';
import { gql, AddSongsToPlaylistMutation, AddSongsToPlaylistMutationVariables } from '..';

const ADD_SONG_TO_PLAYLIST_MUTATION = gql `
  mutation AddSongsToPlaylist ($input: AddSongsToPlaylistInput!) {
    addSongsToPlaylist(input: $input) {
      songPlaylistId
    }
  }
`;

export const useAddSongsToPlaylistMutation = () =>
  useMutation<AddSongsToPlaylistMutation, AddSongsToPlaylistMutationVariables>(ADD_SONG_TO_PLAYLIST_MUTATION);