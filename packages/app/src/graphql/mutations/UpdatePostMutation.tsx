import { useMutation } from '@apollo/react-hooks';
import { gql, UpdatePostMutation, UpdatePostMutationVariables } from '..';

const UPDATE_POST_MUTATION = gql `
    mutation UpdatePost ($id: ID!, $input: PostInput!) {
        updatePost(id: $id, input: $input) {
            post {
                id,
            }
        }
    }
`;

export const useUpdatePostMutation = () => useMutation<UpdatePostMutation, UpdatePostMutationVariables>(UPDATE_POST_MUTATION);