import { useMutation } from '@apollo/react-hooks';
import { gql, AddPlaylistsToSongMutation, AddPlaylistsToSongMutationVariables } from '..';

const ADD_PLAYLISTS_TO_SONG_MUTATION = gql `
  mutation AddPlaylistsToSong ($input: AddPlaylistsToSongInput!) {
    addPlaylistsToSong(input: $input) {
      songId
    }
  }
`;

export const useAddPlaylistsToSongMutation = () =>
  useMutation<AddPlaylistsToSongMutation, AddPlaylistsToSongMutationVariables>(ADD_PLAYLISTS_TO_SONG_MUTATION);