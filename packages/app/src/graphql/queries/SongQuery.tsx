import { useQuery } from '@apollo/react-hooks';
import { gql, SongQuery, SongQueryVariables } from '..';

const SONG_QUERY = gql `
  query Song(
    $id: ID!,
  ) {
    song(
      id: $id,
    ) {
      id
      title
      artist {
        id
        title
      }
      album {
        id
        title
      }
    }
  }
`;

export const useSongQuery = (variables?: SongQueryVariables) => useQuery<SongQuery, SongQueryVariables>(SONG_QUERY, {
  fetchPolicy: 'cache-and-network',
  variables: variables,
});