import { useLazyQuery, useQuery  } from '@apollo/react-hooks';
import { gql, AuthQuery, AuthQueryVariables } from '..';

const AUTH_QUERY = gql `
    query Auth {
        me {
            type,
            viewer {
                id,
                displayName,
                initials,
                avatarSrc,
            }
        }
    }
`;

export const useLazyAuthQuery = () => useLazyQuery<AuthQuery, AuthQueryVariables>(AUTH_QUERY, {
  fetchPolicy: 'cache-and-network',
});

export const useAuthQuery = () => useQuery<AuthQuery, AuthQueryVariables>(AUTH_QUERY, {
  fetchPolicy: 'cache-and-network',
});