import { useQuery, useLazyQuery, LazyQueryHookOptions, QueryHookOptions  } from '@apollo/react-hooks';
import { gql, Data } from '..';

const EXPLORE_POSTS_QUERY = gql `
    query ExplorePosts (
        $before: ID,
        $after: ID,
        $first: Int,
        $last: Int,
        $categories: [ID!]
        $searchTerm: String
    ) {
       posts(
           before: $before,
           after: $after,
           first: $first,
           last: $last,
           categories: $categories,
           searchTerm: $searchTerm,
       ) {
           edges {
               cursor,
               node {
                   id,
               }
           }
       }
    }
`;

export type ExplorePostsQuery = Data.ExplorePostsQuery;
export type ExplorePostsQueryVariables = Data.ExplorePostsQueryVariables;

export const useExplorePostQuery = (variables: ExplorePostsQueryVariables = {}, options?: Omit<QueryHookOptions<ExplorePostsQuery, ExplorePostsQueryVariables>, 'variables'>) =>
  useQuery<ExplorePostsQuery, ExplorePostsQueryVariables>(EXPLORE_POSTS_QUERY, {
    variables,
    fetchPolicy: 'cache-and-network',
    ...options,
  });

export const useLazyExplorePostQuery = (variables: ExplorePostsQueryVariables = {}, options?: Omit<LazyQueryHookOptions<ExplorePostsQuery, ExplorePostsQueryVariables>, 'variables'>) =>
  useLazyQuery<ExplorePostsQuery, ExplorePostsQueryVariables>(EXPLORE_POSTS_QUERY, {
    variables,
    fetchPolicy: 'cache-and-network',
    ...options,
  });
