import { useQuery, useLazyQuery, LazyQueryHookOptions, QueryHookOptions  } from '@apollo/react-hooks';
import { gql, Data } from '..';

const COMMENT_LIST_QUERY = gql `
    query CommentList (
        $postId: ID!,
        $before: ID,
        $after: ID,
        $first: Int,
        $last: Int,
    ) {
        comments(
            postId: $postId,
            before: $before,
            after: $after,
            first: $first,
            last: $last,
        ) {
            edges {
                cursor,
                node {
                    id,
                    content
                    publishedAt
                    author {
                        avatarSrc
                        displayName
                        initials
                    }
                }
            }
        }
    }
`;

export type CommentListQuery = Data.CommentListQuery;
export type CommentListQueryVariables = Data.CommentListQueryVariables;

export const useCommentListQuery = (variables: CommentListQueryVariables, options?: Omit<QueryHookOptions<CommentListQuery, CommentListQueryVariables>, 'variables'>) =>
  useQuery<CommentListQuery, CommentListQueryVariables>(COMMENT_LIST_QUERY, {
    variables,
    fetchPolicy: 'cache-and-network',
    ...options,
  });

export const useCommentListLazyQuery = (variables: CommentListQueryVariables, options?: Omit<LazyQueryHookOptions<CommentListQuery, CommentListQueryVariables>, 'variables'>) =>
  useLazyQuery<CommentListQuery, CommentListQueryVariables>(COMMENT_LIST_QUERY, {
    variables,
    fetchPolicy: 'cache-and-network',
    ...options,
  });
