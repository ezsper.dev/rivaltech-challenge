import { useQuery, useLazyQuery, QueryHookOptions, LazyQueryHookOptions } from '@apollo/react-hooks';
import { gql, MySongPlaylistsQuery, MySongPlaylistsQueryVariables } from '..';

const SONG_PLAYLISTS_QUERY = gql `
  query MySongPlaylists(
    $before: ID,
    $after: ID,
    $first: Int,
    $last: Int,
  ) {
    mySongPlaylists(
      before: $before,
      after: $after,
      first: $first,
      last: $last,
    ) {
      nodes {
        _id,
        id,
        title
      }
    }
  }
`;

export const useMySongPlaylistsQuery = (options?: QueryHookOptions<MySongPlaylistsQuery, MySongPlaylistsQueryVariables>) =>
  useQuery<MySongPlaylistsQuery, MySongPlaylistsQueryVariables>(SONG_PLAYLISTS_QUERY, {
    fetchPolicy: 'cache-and-network',
    ...options,
  });

export const useLazyMySongPlaylistsQuery = (options: LazyQueryHookOptions<MySongPlaylistsQuery, MySongPlaylistsQueryVariables>) =>
  useLazyQuery<MySongPlaylistsQuery, MySongPlaylistsQueryVariables>(SONG_PLAYLISTS_QUERY, {
    fetchPolicy: 'cache-and-network',
    ...options,
  });