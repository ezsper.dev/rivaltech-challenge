import { useQuery } from '@apollo/react-hooks';
import { gql, PostCategoriesQuery, PostCategoriesQueryVariables } from '..';

const POST_CATEGORIES_QUERY = gql `
    query PostCategories {
        postCategories {
            nodes {
                id,
                title
            }
        }
    }
`;

export const usePostCategoriesQuery = () => useQuery<PostCategoriesQuery, PostCategoriesQueryVariables>(POST_CATEGORIES_QUERY, {
    fetchPolicy: 'cache-and-network',
});