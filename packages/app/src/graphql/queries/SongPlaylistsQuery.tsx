import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import { gql, SongPlaylistsQuery, SongPlaylistsQueryVariables } from '..';

const SONG_PLAYLISTS_QUERY = gql `
  query SongPlaylists(
    $before: ID,
    $after: ID,
    $first: Int,
    $last: Int,
    $authorId: ID
  ) {
    songPlaylists(
      before: $before,
      after: $after,
      first: $first,
      last: $last,
      authorId: $authorId
    ) {
      nodes {
        id,
        title
      }
    }
  }
`;

export const useSongPlaylistsQuery = (variables?: SongPlaylistsQueryVariables) => useQuery<SongPlaylistsQuery, SongPlaylistsQueryVariables>(SONG_PLAYLISTS_QUERY, {
  fetchPolicy: 'cache-and-network',
  variables: variables,
});

export const useLazySongPlaylistsQuery = (variables?: SongPlaylistsQueryVariables) => useLazyQuery<SongPlaylistsQuery, SongPlaylistsQueryVariables>(SONG_PLAYLISTS_QUERY, {
  fetchPolicy: 'cache-and-network',
  variables: variables,
});