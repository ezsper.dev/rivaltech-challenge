import { useQuery, useLazyQuery, QueryHookOptions, LazyQueryHookOptions } from '@apollo/react-hooks';
import { gql, SongPlaylistByShortIdQuery, SongPlaylistByShortIdQueryVariables } from '..';

const SONG_PLAYLIST_BY_SHORT_ID_QUERY = gql `
  query SongPlaylistByShortId(
    $_id: String!
  ) {
    songPlaylistByShortId(
      _id: $_id
    ) {
      id
      title
      privacy
      items {
        createdAt
        sortOrder
        song {
          id
          title
          album {
            id
            title
          }
          artist {
            id
            title
          }
        }
      }
    }
  }
`;

export const useSongPlaylistByShortIdQuery = (options?: QueryHookOptions<SongPlaylistByShortIdQuery, SongPlaylistByShortIdQueryVariables>) => useQuery(SONG_PLAYLIST_BY_SHORT_ID_QUERY, {
  fetchPolicy: 'cache-and-network',
  ...options,
});

export const useLazySongPlaylistByShortIdQuery = (options?: LazyQueryHookOptions<SongPlaylistByShortIdQuery, SongPlaylistByShortIdQueryVariables>) => useLazyQuery(SONG_PLAYLIST_BY_SHORT_ID_QUERY, {
  fetchPolicy: 'cache-and-network',
  ...options,
});