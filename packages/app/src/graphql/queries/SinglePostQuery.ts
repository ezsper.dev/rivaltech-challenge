import { useQuery, useLazyQuery, LazyQueryHookOptions, QueryHookOptions  } from '@apollo/react-hooks';
import { gql, SinglePostQuery, SinglePostQueryVariables } from '..';

const SINGLE_POST_QUERY = gql `
    query SinglePost($_id: String!) {
        post: postByShortId(_id: $_id) {
            id,
            _id,
            title,
            name,
            subheading,
            posterSrc,
            publishedAt,
            ratingScore,
            viewerRatingScore,
            counters {
                comments
                raters
            }
            author {
                id
                initials
                avatarSrc
                displayName
            }
            content,
            excerpt,
            category {
                id,
                title
            }       
        }
    }
`;

export const useSinglePostQuery = (variables: SinglePostQueryVariables, options?: Omit<QueryHookOptions<SinglePostQuery, SinglePostQueryVariables>, 'variables'>) =>
  useQuery<SinglePostQuery, SinglePostQueryVariables>(SINGLE_POST_QUERY, {
    variables,
    fetchPolicy: 'cache-and-network',
    ...options,
  });

export const useSinglePostLazyQuery = (variables?: SinglePostQueryVariables, options?: Omit<LazyQueryHookOptions<SinglePostQuery, SinglePostQueryVariables>, 'variables'>) =>
  useLazyQuery<SinglePostQuery, SinglePostQueryVariables>(SINGLE_POST_QUERY, {
    variables,
    fetchPolicy: 'cache-and-network',
    ...options,
  });
