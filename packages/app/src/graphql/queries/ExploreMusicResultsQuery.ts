import { useQuery, useLazyQuery, LazyQueryHookOptions, QueryHookOptions  } from '@apollo/react-hooks';
import { gql, Data } from '..';

const EXPLORE_MUSIC_QUERY = gql `
  query ExploreMusicResults (
    $before: ID,
    $after: ID,
    $first: Int,
    $last: Int,
    $searchTerm: String
  ) {
    songs(
      before: $before,
      after: $after,
      first: $first,
      last: $last,
      searchTerm: $searchTerm,
    ) {
      edges {
        cursor,
        node {
          id,
          title,
          album {
            id
            title
          }
          artist {
            id
            title
          }
        }
      }
    }
  }
`;

export type ExploreMusicResultsQuery = Data.ExploreMusicResultsQuery;
export type ExploreMusicResultsQueryVariables = Data.ExploreMusicResultsQueryVariables;

export const useExploreMusicResultsQuery = (variables: ExploreMusicResultsQueryVariables = {}, options?: Omit<QueryHookOptions<ExploreMusicResultsQuery, ExploreMusicResultsQueryVariables>, 'variables'>) =>
  useQuery<ExploreMusicResultsQuery, ExploreMusicResultsQueryVariables>(EXPLORE_MUSIC_QUERY, {
    variables,
    fetchPolicy: 'cache-and-network',
    ...options,
  });

export const useLazyExploreMusicResultsQuery = (variables: ExploreMusicResultsQueryVariables = {}, options?: Omit<LazyQueryHookOptions<ExploreMusicResultsQuery, ExploreMusicResultsQueryVariables>, 'variables'>) =>
  useLazyQuery<ExploreMusicResultsQuery, ExploreMusicResultsQueryVariables>(EXPLORE_MUSIC_QUERY, {
    variables,
    fetchPolicy: 'cache-and-network',
    ...options,
  });
