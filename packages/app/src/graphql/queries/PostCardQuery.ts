import { useQuery, useLazyQuery, LazyQueryHookOptions, QueryHookOptions  } from '@apollo/react-hooks';
import { gql, PostCardQuery, PostCardQueryVariables } from '..';

const POST_CARD_QUERY = gql `
    query PostCard ($id: ID!) {
        post(id: $id) {
            id,
            _id,
            title,
            name,
            subheading,
            content,
            excerpt(resolve: true),
            publishedAt,
            thumbnailSrc,
            ratingScore,
            counters {
                comments
            }
            author {
                displayName
                initials
                avatarSrc
            }
            category {
                id,
                title
            }
        }
    }
`;

export const usePostCardQuery = (variables: PostCardQueryVariables, options?: Omit<QueryHookOptions<PostCardQuery, PostCardQueryVariables>, 'variables'>) =>
  useQuery<PostCardQuery, PostCardQueryVariables>(POST_CARD_QUERY, {
    variables,
    fetchPolicy: 'no-cache',
    ...options,
  });

export const usePostCardLazyQuery = (variables?: PostCardQueryVariables, options?: Omit<LazyQueryHookOptions<PostCardQuery, PostCardQueryVariables>, 'variables'>) =>
  useLazyQuery<PostCardQuery, PostCardQueryVariables>(POST_CARD_QUERY, {
    variables,
    fetchPolicy: 'no-cache',
    ...options,
  });
