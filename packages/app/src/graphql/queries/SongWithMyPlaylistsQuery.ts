import { useQuery, useLazyQuery, QueryHookOptions, LazyQueryHookOptions } from '@apollo/react-hooks';
import { gql, SongWithMyPlaylistsQuery, SongWithMyPlaylistsQueryVariables } from '..';

const SONG_WITH_MY_PLAYLISTS_QUERY = gql `
  query SongWithMyPlaylists(
    $id: ID!,
  ) {
    song(
      id: $id,
    ) {
      myPlaylists {
        nodes {
          id
        }
      }
    }
  }
`;

export const useSongWithMyPlaylistsQuery = (options?: QueryHookOptions<SongWithMyPlaylistsQueryVariables, SongWithMyPlaylistsQuery>) => useQuery(SONG_WITH_MY_PLAYLISTS_QUERY, {
  fetchPolicy: 'cache-and-network',
  ...options,
});

export const useLazySongWithMyPlaylistsQuery = (options?: LazyQueryHookOptions<SongWithMyPlaylistsQuery, SongWithMyPlaylistsQueryVariables>) => useLazyQuery(SONG_WITH_MY_PLAYLISTS_QUERY, {
  fetchPolicy: 'cache-and-network',
  ...options,
});