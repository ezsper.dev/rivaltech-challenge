import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { setContext } from 'apollo-link-context';
import { HttpLink } from 'apollo-link-http';
import { store } from '../store';
import { config } from '../config';

const httpLink = new HttpLink({
  uri: config.graphqlUri,
});

const authLink = setContext((_, { headers }) => {
  const { token } = store.getState().auth;
  return {
    headers: {
      ...headers,
      ...(token && {
        authorization: token,
      })
    }
  }
});

const cacheLink = new InMemoryCache();

export const client = new ApolloClient({
  cache: cacheLink,
  link: authLink.concat(httpLink),
});
