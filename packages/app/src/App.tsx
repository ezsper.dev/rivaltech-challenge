import React from 'react';
import { AppContext } from './components/context';
import { AppRoutes } from './components/AppRoutes';
import './App.css';

function App() {
  return (
    <div className="App">
      <AppContext>
        <AppRoutes />
      </AppContext>
    </div>
  );
}

export default App;
