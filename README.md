## React App

The intent was to create a working application on desktop that
would give a simple, yet powerful, experience to the user using
best practices on development. Hope you enjoy it :).

Required Features
-----------------------------

Start by building out these features:

1. [x] Load the library of songs and show it in the browser.
2. [x] Add songs from the library to a playlist.
3. [x] Name and save a playlist.
4. [x] List saved playlists.
5. [x] Load saved playlists.

If you complete the above and still have time, try one of these, in no
particular order:

* [x] Drag 'n Drop organization. (but it's currently not saving though)
* [x] Search or filtering for songs.
* [x] Sorting songs by properties like album, artist or length. (currently online on playlist)

You do **not** have to do any of the following:

* [ ]  Play sounds! 🎵 (wanted to do that, but did not had the time :()
* [x]  Deal with authentication. 🔐 (with Github OAuth)
* [ ] Work across browsers. Pick your favourite and get it working there. 🏄

### Installation

Install packages

```bash
npm install
```

Execute npm script
```bash
npm run start
```

The App will be available at

http://localhost:3000/

The GraphQL Server will be available at

http://localhost:7080/

##### Storybook (Extra)

Execute the following command to get access to storybook view
for design components

```bash
npm run components
```


### Key aspects of the project

* It uses Redux for offline user experience and local state
* It uses Apollo GraphQL for realtime user experience, reliability and global state
* JSS styles
* Monorepo with Lerna
* Rich text editor
* Progressive Web App using React Suspense API
* Back-end server with reusable schema definitions across the packages
* OAuth 2 login with github
* JWT for serverless authentication
And more...

### Things that were out in this initial push

* **Add to playlist** - Currently it just display a mocked action, plan to properly implement in the next days

* **Create a playlist** - Although I already implemented the methods in backend, the current page is a mock, plan to properly implement in the next days

* **More specific Unit tests** - Major unit tests are solely based on snapshots.

* **Metrics** - Did not had the time to create fully capable metrics
based in real data from the app, plan to do in the next days, I didn't
want to simply put a Pie chart in there with any relation whatsoever.

* **Post removal** - Did not had the time to create a button for post
removal, although it's implemented in the API, for now you can only edit the post.

* **More Redux** - I used redux in key points for the application state
and used GraphQL for the rest of the data as it was more suitable.

* **Responsiveness** - Did not had the time to review most of the components
on mobile, it should be pretty simple to fix any issue or to simply optimise,
as I said my focus in this first push was to have a based working application.

* **CrossBrowser** - Did not had the time to verify browser support, webpack
with polyfill.io covers most of the problems.


### Project structure

#### `/packages/api`

Holds the back-end application with GraphQL and Apollo, using a 
simple SQLite database (development only).


#### `/packages/app`

Holds the front-end application fully connected with the back-end


#### `/packages/design`

Holds reusable components based on UX to be implemented on the App project


